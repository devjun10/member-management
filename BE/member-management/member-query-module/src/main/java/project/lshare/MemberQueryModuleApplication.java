package project.lshare;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import project.lshare.membercoremodule.MemberCoreModuleApplication;

@SpringBootApplication
@Import(MemberCoreModuleApplication.class)
public class MemberQueryModuleApplication {

    public static void main(String[] args) {
        SpringApplication.run(MemberQueryModuleApplication.class, args);
    }

}
