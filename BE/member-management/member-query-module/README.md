## ⛺️ 회원관리 질의(Query) 모듈

회원관리 질의에 관련된 모듈입니다.

<br/><br/><br/><br/>

## 👪 패키지 간 의존관계

질의 모듈은 Core 모듈에만 의존합니다.

| Core Module | Command Module | Query Module | OAuth2 Module |
|:-----------:|:--------------:|:------------:|:-------------:|
|      O      |       X        |      -       |       X       |
