## ⛺️ 회원 인증 모듈

인증/인가는 Command/Query로 나누어진 양쪽 모두에서 사용하기 위한 별도의 인증 모듈입니다.

<br/><br/><br/><br/>

## 👪 패키지 간 의존관계

인증 모듈은 Core 모듈에만 의존합니다.

| Core Module | Command Module | Query Module | OAuth2 Module |
|:-----------:|:--------------:|:------------:|:-------------:|
|      X      |       O        |      X       |       -       |
