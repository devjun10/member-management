package project.lshare.oauth2.test.acceptance.login.failure;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.test.context.TestPropertySource;
import project.lshare.oauth2.configuration.annotation.TestScenario;
import project.lshare.oauth2.test.acceptance.AcceptanceTestBase;
import project.lshare.oauth2.test.helper.stub.OAuth2LoginTimeoutStub;

import java.io.IOException;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.Matchers.equalTo;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.COMMAND;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.FAILURE;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.LOGIN_CLASS;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.LOGIN_METHOD;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.LOGOUT_UNAUTHORIZED_RESPONSE_SNIPPET;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.TIMEOUT_FAILURE;
import static project.lshare.oauth2.test.helper.fixture.TokenFixture.getValidOAuth2Code;

@TestScenario(
        target = "사용자가",
        situation = "로그인/회원가입을 시도하는데 OAuth2 서버와의 연결이 지연된다.",
        expectedResult = "502 응답을 받는다."
)
@TestPropertySource(properties = {
        "oauth2.access-token=http://localhost:${wiremock.server.port}/api/oauth2/google/authorization",
        "oauth2.userprofile=http://localhost:${wiremock.server.port}/api/oauth2/google/userprofile"
})
@AutoConfigureWireMock(port = 0)
@DisplayName("OAuth2 로그인/회원가입 실패(502) 인수 테스트")
class OAuth2LoginTimeoutErrorAcceptanceTest extends AcceptanceTestBase {

    @BeforeAll
    static void setStub() throws IOException {
        OAuth2LoginTimeoutStub.setUP();
    }

    @Test
    @DisplayName("OAuth2 서버로 부터 응답이 지연되면 502 응답을 반환한다.")
    void when_oauth2_server_response_delayed_then_statuscode_should_be_502() {
        given(this.specification)
                .filters(createDocument(LOGIN_CLASS, LOGIN_METHOD, COMMAND, FAILURE, TIMEOUT_FAILURE, LOGOUT_UNAUTHORIZED_RESPONSE_SNIPPET))

                .when()
                .contentType(JSON)
                .queryParam("code", getValidOAuth2Code())
                .get("/api/oauth2/login")

                .then()
                .statusCode(equalTo(502))
                .log().all();
    }
}
