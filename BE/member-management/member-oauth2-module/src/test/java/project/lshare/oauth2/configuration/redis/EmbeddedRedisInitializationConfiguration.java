package project.lshare.oauth2.configuration.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

import static project.lshare.oauth2.test.helper.fixture.TokenFixture.getValidRefreshToken;

@Component
public class EmbeddedRedisInitializationConfiguration {

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    public void init() {
        initRedisTemplate();
        initStringRedisTemplate();
    }

    private void initRedisTemplate() {
        Set<String> redisKeys = redisTemplate.keys("*");

        if (redisKeys == null) {
            redisKeys = new HashSet<>();
        }

        for (String key : redisKeys) {
            redisTemplate.delete(key);
        }
        saveFixedRefreshToken();
    }

    private void initStringRedisTemplate() {
        Set<String> redisKeys = stringRedisTemplate.keys("#*");

        if (redisKeys == null) {
            redisKeys = new HashSet<>();
        }

        for (String key : redisKeys) {
            stringRedisTemplate.delete(key);
        }
    }

    private void saveFixedRefreshToken() {
        redisTemplate.opsForValue().set("#{refreshToken}/{memberId}/" + 1L, getValidRefreshToken());
    }
}
