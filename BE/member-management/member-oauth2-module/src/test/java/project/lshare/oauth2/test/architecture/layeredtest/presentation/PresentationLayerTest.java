package project.lshare.oauth2.test.architecture.layeredtest.presentation;

import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;
import com.tngtech.archunit.library.Architectures;
import org.junit.jupiter.api.DisplayName;
import project.lshare.membercoremodule.common.annotation.helper.Tag;
import project.lshare.oauth2.MemberOauth2ModuleApplication;
import project.lshare.oauth2.common.annotation.business.ApplicationLayer;
import project.lshare.oauth2.common.annotation.business.PresentationLayer;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;
import static com.tngtech.archunit.library.Architectures.layeredArchitecture;
import static project.lshare.oauth2.test.architecture.layeredtest.presentation.PresentationLayerTestConstant.ARGUMENT_RESOLVER_TEST_LAYER;
import static project.lshare.oauth2.test.architecture.layeredtest.presentation.PresentationLayerTestConstant.OAUTH2_PRESENTATION_LAYER;
import static project.lshare.oauth2.test.architecture.layeredtest.presentation.PresentationLayerTestConstant.OAUTH2_PRESENTATION_LAYER_PATH;
import static project.lshare.oauth2.test.architecture.layeredtest.presentation.PresentationLayerTestConstant.TEST_LAYER_PATH;

@DisplayName("Presentation 계층 테스트")
@AnalyzeClasses(packages = "project.lshare.member-oauth2-module", packagesOf = MemberOauth2ModuleApplication.class)
class PresentationLayerTest {

    @ArchTest
    @Tag(description = "Presentation 계층은 Application 계층에만 접근해야 한다.")
    ArchRule presentation_layer_should_access_application_layer = classes().that().areAnnotatedWith(PresentationLayer.class)
            .should().accessClassesThat().areAnnotatedWith(ApplicationLayer.class);

    /**
     * ArgumentResolver 테스트로 인해 예외 케이스 발생.
     * Presentation 계층에 테스트에서 접근.
     * ArgumentResolver 패키지 접근 제어자 public으로 변경.
     */
    @ArchTest
    private static final Architectures.LayeredArchitecture presentation_layer_should_not_be_accessed_by_other_layer = layeredArchitecture()
            .withOptionalLayers(false)
            .layer(OAUTH2_PRESENTATION_LAYER).definedBy(OAUTH2_PRESENTATION_LAYER_PATH)

            // Test로 인한 접근
            .optionalLayer(ARGUMENT_RESOLVER_TEST_LAYER).definedBy(TEST_LAYER_PATH)

            .whereLayer(OAUTH2_PRESENTATION_LAYER).mayOnlyBeAccessedByLayers(ARGUMENT_RESOLVER_TEST_LAYER);
}
