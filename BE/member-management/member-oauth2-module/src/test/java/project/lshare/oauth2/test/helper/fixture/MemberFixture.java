package project.lshare.oauth2.test.helper.fixture;

import project.lshare.membercoremodule.common.field.Deleted;
import project.lshare.membercoremodule.domainmodel.member.District;
import project.lshare.membercoremodule.domainmodel.member.Member;
import project.lshare.membercoremodule.domainmodel.member.Role;
import project.lshare.membercoremodule.persistencemodel.member.MemberJpaEntity;
import project.lshare.membercoremodule.common.annotation.helper.Tag;

@Tag(description = "회원 테스트 헬퍼 클래스")
public final class MemberFixture {

    private MemberFixture() {
        throw new AssertionError("올바른 방법으로 생성자를 호출해주세요.");
    }

    public static Member createMemberDomainWithId(Long memberId) {
        return new Member(
                memberId,
                "devjun10",
                "https://e1.pngegg.com/pngimages/817/696/png-clipart-lionel-messi.png",
                "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36",
                "192.168.10.23",
                Role.NORMAL,
                District.SEOUL,
                Deleted.FALSE
        );
    }

    public static MemberJpaEntity createMemberWithoutId() {
        return new MemberJpaEntity(
                "devjun10",
                "https://e1.pngegg.com/pngimages/817/696/png-clipart-lionel-messi.png"
        );
    }

    public static MemberJpaEntity createMemberWithId() {
        return new MemberJpaEntity(
                null,
                "devjun10",
                "https://e1.pngegg.com/pngimages/817/696/png-clipart-lionel-messi.png",
                "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36",
                "192.168.10.23",
                Role.NORMAL,
                District.SEOUL,
                Deleted.FALSE
        );
    }

    public static MemberJpaEntity createMemberWithId(Long memberId) {
        return new MemberJpaEntity(
                memberId,
                "devjun10",
                "https://e1.pngegg.com/pngimages/817/696/png-clipart-lionel-messi.png",
                "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36",
                "192.168.10.23",
                Role.NORMAL,
                District.SEOUL,
                Deleted.FALSE
        );
    }
}
