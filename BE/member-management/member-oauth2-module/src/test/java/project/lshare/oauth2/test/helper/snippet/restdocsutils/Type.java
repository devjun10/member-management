package project.lshare.oauth2.test.helper.snippet.restdocsutils;

public enum Type {
    STRING("String");

    private final String name;

    Type(String name) {
        this.name = name;
    }

    public String getName(String name) {
        return Type.valueOf(name).toString();
    }
}
