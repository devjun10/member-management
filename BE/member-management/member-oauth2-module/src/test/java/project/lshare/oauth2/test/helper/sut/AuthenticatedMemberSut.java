package project.lshare.oauth2.test.helper.sut;

import project.lshare.membercoremodule.common.annotation.helper.Tag;
import project.lshare.membercoremodule.domainmodel.authentication.valueobject.AuthenticatedMember;

import static org.junit.Assert.assertNotNull;

@Tag(description = "인증된 사용자 검증 헬퍼 클래스(Sut)")
public class AuthenticatedMemberSut {

    private final AuthenticatedMember authenticatedMember;

    public AuthenticatedMemberSut(AuthenticatedMember authenticatedMember) {
        this.authenticatedMember = authenticatedMember;
    }

    public AuthenticatedMemberSut shouldExist() {
        assertNotNull(authenticatedMember);
        return this;
    }

    public AuthenticatedMemberSut withId() {
        assertNotNull(authenticatedMember.getMemberId());
        return this;
    }

    public AuthenticatedMemberSut withRole() {
        assertNotNull(authenticatedMember.getRole());
        return this;
    }
}
