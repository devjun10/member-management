package project.lshare.oauth2.test.integrationtest.oauth2;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.test.context.TestPropertySource;
import project.lshare.membercoremodule.persistencemodel.authentication.AuthenticationJpaEntity;
import project.lshare.membercoremodule.persistencemodel.member.MemberJpaEntity;
import project.lshare.oauth2.oauth.application.port.in.OAuth2LoginCommand;
import project.lshare.oauth2.test.helper.stub.OAuth2LoginStub;
import project.lshare.oauth2.test.helper.persistence.PersistenceHelper;
import project.lshare.oauth2.test.integrationtest.IntegrationTestBase;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static project.lshare.oauth2.test.helper.fixture.MemberFixture.createMemberWithoutId;
import static project.lshare.oauth2.test.helper.fixture.TokenFixture.getValidOAuth2Code;

@AutoConfigureWireMock(port = 0)
@TestPropertySource(properties = {
        "oauth2.access-token=http://localhost:${wiremock.server.port}/api/oauth2/google/authorization",
        "oauth2.userprofile=http://localhost:${wiremock.server.port}/api/oauth2/google/userprofile"
})
@DisplayName("OAuth2 회원가입 통합 테스트")
class GoogleOAuth2LoginServiceIntegrationTest extends IntegrationTestBase {

    @Autowired
    private OAuth2LoginCommand auth2LoginCommand;

    @Autowired
    private PersistenceHelper persistenceHelper;

    @BeforeAll
    static void setUP() throws IOException {
        OAuth2LoginStub.setUP();
    }

    @Test
    @DisplayName("이미 가입된 회원이라면 프로필만 업데이트 된다.")
    void when_already_signuped_member_then_just_the_profile_should_be_updated() {
        // given
        String expectedImageUrl = "https://lh3.googleusercontent.com/a/default-user=s96-c";
        MemberJpaEntity member = persistenceHelper.persist(createMemberWithoutId());
        AuthenticationJpaEntity authentication = persistenceHelper.persist(new AuthenticationJpaEntity(member, "devjun1023@gmail.com", "hellow.png"));

        // when
        auth2LoginCommand.login(getValidOAuth2Code());

        // then
        AuthenticationJpaEntity findAuthentication = persistenceHelper.findAuthenticationById(authentication.getAuthenticationId());
        assertEquals(expectedImageUrl, findAuthentication.getProfileImageUrl());
    }
}
