package project.lshare.oauth2.test.acceptance.logout.success;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.lshare.oauth2.configuration.annotation.InsertData;
import project.lshare.oauth2.configuration.annotation.LoginAcceptanceTest;
import project.lshare.oauth2.configuration.annotation.TestScenario;
import project.lshare.oauth2.test.acceptance.AcceptanceTestBase;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.Matchers.equalTo;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.COMMAND;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.FAILURE_FORBIDDEN_ACCESS;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.FAILURE_UNAUTHORIZED;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.LOGOUT;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.SINGLE_DEVICE_LOG_OUT_CLASS;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.SUCCESS;
import static project.lshare.oauth2.test.helper.fixture.TokenFixture.getInvalidAccessToken;
import static project.lshare.oauth2.test.helper.fixture.TokenFixture.getValidAccessToken;
import static project.lshare.oauth2.test.helper.fixture.TokenFixture.getValidRefreshToken;

@TestScenario(
        target = "사용자가",
        situation = "로그아웃을 시도한다.",
        expectedResult = "로그아웃이 된다.",
        failureExpectedResult = {
                "올바른 토큰이 존재하지 않는 경우 401 응답을 반환한다.",
                "AccessToken / RefreshToken이 존재하지 않는 경우 403 응답을 반환한다."
        }
)
@InsertData
@LoginAcceptanceTest
@DisplayName("OAuth2 단일 기기 로그아웃 인수 테스트")
class LogoutSingleDeviceAcceptanceTest extends AcceptanceTestBase {

    @Test
    @DisplayName("현재 로그인 된 사용자가 단일 기기에 대해 로그아웃을 시도할 경우 200응답이 반환된다.")
    void when_currently_logined_member_try_logout_of_single_device_then_statuscode_should_be_200() {
        given(this.specification)
                .filters(createDocument(
                        SINGLE_DEVICE_LOG_OUT_CLASS, LOGOUT, COMMAND, SUCCESS)
                )

                .when()
                .contentType(JSON)
                .header("Authorization", getValidAccessToken())
                .cookie("RefreshToken", getValidRefreshToken())
                .post("/api/logout/single-device")

                .then()
                .statusCode(equalTo(200))
                .cookie("RefreshToken")
                .log().all();
    }

    @Test
    @DisplayName("현재 로그인 된 사용자가 단일 기기에 대해 로그아웃을 시도할 경우 200응답이 반환된다.")
    void given_forged_accesstoken_when_try_logout_then_statuscode_should_be_401() {
        given(this.specification)
                .filters(createDocument(SINGLE_DEVICE_LOG_OUT_CLASS, LOGOUT, COMMAND, FAILURE_UNAUTHORIZED))

                .when()
                .contentType(JSON)
                .post("/api/logout/single-device")

                .then()
                .statusCode(equalTo(401))
                .cookie("RefreshToken")
                .log().all();
    }

    @Test
    @DisplayName("현재 로그인 된 사용자가 단일 기기에 대해 로그아웃을 시도할 경우 200응답이 반환된다.")
    void given_invalid_accesstoken_member_when_try_logout_then_statuscode_should_be_403() {
        given(this.specification)
                .filters(createDocument(SINGLE_DEVICE_LOG_OUT_CLASS, LOGOUT, COMMAND, FAILURE_FORBIDDEN_ACCESS))

                .when()
                .contentType(JSON)
                .header("Authorization", getInvalidAccessToken())
                .cookie("RefreshToken", getValidRefreshToken())
                .post("/api/logout/single-device")

                .then()
                .statusCode(equalTo(403))
                .cookie("RefreshToken")
                .log().all();
    }
}
