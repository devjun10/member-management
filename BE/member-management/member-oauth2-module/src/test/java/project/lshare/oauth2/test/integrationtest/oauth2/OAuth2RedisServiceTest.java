package project.lshare.oauth2.test.integrationtest.oauth2;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import project.lshare.oauth2.oauth.application.port.out.OAuth2RedisService;
import project.lshare.oauth2.test.integrationtest.IntegrationTestBase;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertAll;
import static project.lshare.oauth2.test.helper.fixture.TokenFixture.getOtherValidRefreshToken;
import static project.lshare.oauth2.test.helper.fixture.TokenFixture.getValidRefreshToken;

@DisplayName("레디스(토큰) 통합 테스트")
class OAuth2RedisServiceTest extends IntegrationTestBase {

    @Autowired
    private OAuth2RedisService redisService;

    @Test
    @DisplayName("회원 아이디와 RefreshToken을 넣으면 값이 저장된다.")
    void when_save_refreshtoken_then_value_should_be_not_null() {
        redisService.saveRefreshToken(1L, getValidRefreshToken());

        assertNotNull(redisService.getRefreshToken(1L, getValidRefreshToken()));
    }

    @Test
    @DisplayName("일치하는 RefreshToken이 없다면 Null 값이 반환된다.")
    void when_matching_refreshtoken_not_in_the_redis_then_expected_value_should_be_null() {
        assertNull(redisService.getRefreshToken(Long.MAX_VALUE, getValidRefreshToken()));
    }

    @Test
    @DisplayName("모든 기기에서 로그아웃 하면 저장된 모든 RefreshToken이 삭제된다.")
    void when_logout_all_devices_then_all_refreshtoken_should_be_deleted() {
        redisService.saveRefreshToken(1L, getValidRefreshToken());
        redisService.saveRefreshToken(1L, getOtherValidRefreshToken());
        redisService.logoutAllDevices(1L);

        assertAll(
                () -> assertNull(redisService.getRefreshToken(1L, getValidRefreshToken())),
                () -> assertNull(redisService.getRefreshToken(1L, getOtherValidRefreshToken()))
        );
    }
}
