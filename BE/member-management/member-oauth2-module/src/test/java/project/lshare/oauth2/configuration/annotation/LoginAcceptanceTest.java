package project.lshare.oauth2.configuration.annotation;

import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.test.context.TestPropertySource;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@AutoConfigureWireMock(port = 0)
@TestPropertySource(properties = {
        "oauth2.access-token=http://localhost:${wiremock.server.port}/api/oauth2/google/authorization",
        "oauth2.userprofile=http://localhost:${wiremock.server.port}/api/oauth2/google/userprofile"
})
@Inherited
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface LoginAcceptanceTest {
}
