package project.lshare.oauth2.test.acceptance.login.success;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.lshare.oauth2.configuration.annotation.LoginAcceptanceTest;
import project.lshare.oauth2.configuration.annotation.TestScenario;
import project.lshare.oauth2.test.acceptance.AcceptanceTestBase;
import project.lshare.oauth2.test.helper.stub.OAuth2LoginStub;

import java.io.IOException;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.COMMAND;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.LOGIN_CLASS;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.LOGIN_METHOD;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.LOGIN_RRESPONSE_SNIPPET;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.SUCCESS;
import static project.lshare.oauth2.test.helper.fixture.TokenFixture.getValidOAuth2Code;

@TestScenario(
        target = "사용자가",
        situation = "로그인/회원가입을 시도한다.",
        expectedResult = "회원가입이 된다."
)
@LoginAcceptanceTest
@DisplayName("OAuth2 로그인 인수 테스트")
class OAuth2LoginAcceptanceTest extends AcceptanceTestBase {

    @BeforeAll
    static void setStub() throws IOException {
        OAuth2LoginStub.setUP();
    }

    @Test
    @DisplayName("올바른 code가 들어오면 Google로 부터 인증/인가를 받아 회원가입을 진행한 후 200 응답을 반환한다.")
    void given_validcode_when_oauth2_login_then_statuscode_should_be_200() {
        given(this.specification)
                .filters(createDocument(LOGIN_CLASS, LOGIN_METHOD, COMMAND, SUCCESS,
                        getRequestParameterSnippet("code", "OAuth2 Code", false, "String"),
                        LOGIN_RRESPONSE_SNIPPET))

                .when()
                .contentType(JSON)
                .queryParam("code", getValidOAuth2Code())
                .get("/api/oauth2/login")

                .then()
                .statusCode(equalTo(200))
                .body(notNullValue())
                .cookie("RefreshToken")
                .log().all();
    }
}
