package project.lshare.oauth2.test.integrationtest.oauth2;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import project.lshare.membercoremodule.persistencemodel.member.MemberJpaEntity;
import project.lshare.oauth2.oauth.application.service.IpUpdateService;
import project.lshare.oauth2.test.integrationtest.IntegrationTestBase;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static project.lshare.oauth2.test.helper.fixture.MemberFixture.createMemberWithoutId;

@DisplayName("회원 아이피 갱신 통합 테스트")
class LastLoginIpAddressUpdateIntegrationTest extends IntegrationTestBase {

    @Autowired
    private IpUpdateService ipUpdateService;

    @Test
    @DisplayName("최근 로그인 한 IP 주소가 식별되면 IP 주소가 갱신된다.")
    void when_login_with_new_ipaddress_then_lastlogin_ipaddress_should_be_updated() {
        // given
        String expected = "50.12.40.50";
        MemberJpaEntity memberJpaEntity = persistenceHelper.persist(createMemberWithoutId());

        // when
        ipUpdateService.updateLastLoginIpAddress(memberJpaEntity.getMemberId(), "50.12.40.50");

        // then
        MemberJpaEntity member = entityManager.find(MemberJpaEntity.class, memberJpaEntity.getMemberId());
        assertEquals(expected, member.getLastLoginIpaddress());
    }
}
