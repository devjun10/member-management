package project.lshare.oauth2.test.architecture.packagetest.utils;

import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;
import org.junit.jupiter.api.DisplayName;
import project.lshare.membercoremodule.common.annotation.helper.Tag;
import project.lshare.membercoremodule.common.annotation.helper.UtilityClass;
import project.lshare.oauth2.MemberOauth2ModuleApplication;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;

@DisplayName("유틸리티 클래스 패키지 테스트")
@AnalyzeClasses(packages = "project.lshare.member-oauth2-module", packagesOf = MemberOauth2ModuleApplication.class)
class UtilClassesPackageTest {

    @ArchTest
    @Tag(description = "유틸리티 클래스는 utils 패키지 내부에 있어야 한다.")
    static final ArchRule util_classes_should_reside_in_utils_package =
            classes().that().areAnnotatedWith(UtilityClass.class)
                    .should()
                    .resideInAPackage("..utils..")
                    .as("UtilityClass should reside in a package '..utils..'");

}
