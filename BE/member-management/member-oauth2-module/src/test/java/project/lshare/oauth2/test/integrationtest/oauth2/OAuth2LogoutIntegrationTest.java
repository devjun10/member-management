package project.lshare.oauth2.test.integrationtest.oauth2;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import project.lshare.membercoremodule.domainmodel.member.Role;
import project.lshare.oauth2.oauth.application.port.out.OAuth2RedisService;
import project.lshare.membercoremodule.domainmodel.authentication.valueobject.AuthenticatedMember;
import project.lshare.oauth2.oauth.application.port.in.OAuth2LogoutCommand;
import project.lshare.oauth2.test.integrationtest.IntegrationTestBase;

import static org.junit.jupiter.api.Assertions.assertNull;
import static project.lshare.oauth2.test.helper.fixture.TokenFixture.getValidRefreshToken;

@DisplayName("로그아웃 통합 테스트")
class OAuth2LogoutIntegrationTest extends IntegrationTestBase {

    @Autowired
    private OAuth2LogoutCommand oauth2LogoutCommand;

    @Autowired
    private OAuth2RedisService oauth2RedisService;

    @Test
    @DisplayName("로그아웃을 하면 해당 기기에 대한 RefreshToken이 삭제된다.")
    void when_logout_then_refreshtoken_of_device_should_be_deleted() {
        String refreshToken = getValidRefreshToken();
        oauth2RedisService.saveRefreshToken(1L, refreshToken);
        oauth2LogoutCommand.logout(new AuthenticatedMember(1L, Role.NORMAL), refreshToken);

        assertNull(oauth2RedisService.getRefreshToken(1L, getValidRefreshToken()));
    }

    @Test
    @DisplayName("모든 기기에서 로그아웃 하면 저장된 모든 RefreshToken이 삭제된다.")
    void when_logout_all_devices_then_all_refreshtokens_should_be_deleted() {
        oauth2RedisService.saveRefreshToken(1L, getValidRefreshToken());
        oauth2LogoutCommand.logoutAllDevices(new AuthenticatedMember(1L, Role.NORMAL));

        assertNull(oauth2RedisService.getRefreshToken(1L, getValidRefreshToken()));
    }
}
