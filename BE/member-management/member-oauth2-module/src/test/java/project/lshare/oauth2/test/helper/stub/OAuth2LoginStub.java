package project.lshare.oauth2.test.helper.stub;

import com.github.tomakehurst.wiremock.client.WireMock;
import project.lshare.membercoremodule.common.annotation.helper.Tag;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;

import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.equalToJson;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static java.nio.charset.Charset.defaultCharset;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.util.StreamUtils.copyToString;
import static project.lshare.oauth2.test.helper.fixture.TokenFixture.getOAuth2AccessToken;

@Tag(description = "OAuth2 로그인 외부 통신 대역(Stub)")
public final class OAuth2LoginStub {

    public static void setUP() throws IOException {
        stubFor(post(urlEqualTo("/api/oauth2/google/authorization"))
                .withRequestBody(equalToJson(getResource("__files/google-oauth2-requestbody.json")))
                .willReturn(WireMock.aResponse()
                        .withHeader(CONTENT_TYPE, APPLICATION_JSON_VALUE)
                        .withStatus(200)
                        .withBody(getMockResponseByPath("__files/authorization-response.json"))));

        stubFor(get(urlEqualTo("/api/oauth2/google/userprofile"))
                .withHeader(AUTHORIZATION, equalTo(getAccessToken()))
                .willReturn(WireMock.aResponse()
                        .withHeader(CONTENT_TYPE, APPLICATION_JSON_VALUE)
                        .withStatus(200)
                        .withBody(getMockResponseByPath("__files/userprofile-response.json"))));
    }

    private static String getResource(String path) {
        return new BufferedReader(new InputStreamReader(getMockResourceAsStream(path), StandardCharsets.UTF_8))
                .lines()
                .collect(Collectors.joining("\n"));
    }

    private static String getMockResponseByPath(String path) throws IOException {
        return copyToString(getMockResourceAsStream(path), defaultCharset());
    }

    private static InputStream getMockResourceAsStream(String path) {
        return OAuth2LoginStub.class.getClassLoader().getResourceAsStream(path);
    }

    private static String getAccessToken() {
        return getOAuth2AccessToken();
    }
}
