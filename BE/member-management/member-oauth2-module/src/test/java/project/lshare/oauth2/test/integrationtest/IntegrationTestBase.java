package project.lshare.oauth2.test.integrationtest;

import org.junit.jupiter.api.AfterEach;
import org.springframework.beans.factory.annotation.Autowired;
import project.lshare.oauth2.configuration.annotation.IntegrationTest;
import project.lshare.oauth2.configuration.rdb.RDBInitializationConfiguration;
import project.lshare.oauth2.configuration.redis.EmbeddedRedisInitializationConfiguration;
import project.lshare.oauth2.test.helper.persistence.PersistenceHelper;

import javax.persistence.EntityManager;

@IntegrationTest
public abstract class IntegrationTestBase {

    @Autowired
    private RDBInitializationConfiguration rdbInitialization;

    @Autowired
    private EmbeddedRedisInitializationConfiguration redisInitialization;

    @Autowired
    protected PersistenceHelper persistenceHelper;

    @Autowired
    protected EntityManager entityManager;

    @AfterEach
    void afterEach() {
        rdbInitialization.truncateAllEntity();
        redisInitialization.init();
    }

}
