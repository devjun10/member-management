package project.lshare.oauth2.test.integrationtest.jwt;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.mock.web.MockHttpServletRequest;
import project.lshare.oauth2.oauth.adapter.in.web.jwt.JwtTokenProvider;
import project.lshare.oauth2.test.integrationtest.IntegrationTestBase;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static project.lshare.oauth2.test.helper.fixture.TokenFixture.getExpiredToken;
import static project.lshare.oauth2.test.helper.fixture.TokenFixture.getInvalidAccessToken;
import static project.lshare.oauth2.test.helper.fixture.TokenFixture.getValidAccessToken;
import static project.lshare.oauth2.test.helper.fixture.TokenFixture.getValidParsedAccessToken;

@DisplayName("TokenProvider 테스트")
class JwtTokenProviderTest extends IntegrationTestBase {

    @Autowired
    private JwtTokenProvider tokenProvider;

    private MockHttpServletRequest request;

    @BeforeEach
    public void setUP() {
        request = new MockHttpServletRequest();
    }

    @Test
    @DisplayName("올바른 토큰을 파싱하면 회원 아이디가 반환된다.")
    void when_valid_token_parsed_then_should_get_memberid() {
        // given
        Long expected = 1L;
        String accessToken = tokenProvider.createAccessToken(1L);

        // when
        Long memberId = tokenProvider.getMemberId(accessToken);

        // then
        assertEquals(expected, memberId);
    }

    @Test
    @DisplayName("올바른 토큰을 요청 헤더에 가지고 있으면 회원 아이디가 반환된다.")
    void when_requestheader_has_valid_token_in_header_then_should_get_memberid() {
        // given
        Long expected = 1L;
        request.addHeader("Authorization", getValidAccessToken());
        String accessToken = tokenProvider.getAccessToken(request);

        // when
        Long memberId = tokenProvider.getMemberId(accessToken);

        // then
        assertEquals(expected, memberId);
    }

    @Test
    @DisplayName("올바르지 않은 토큰을 파싱하면 Null 값이 반환된다.")
    void when_invalid_token_parsed_then_expected_value_should_be_null() {
        // given
        Long expected = null;
        request.addHeader("Authorization", getInvalidAccessToken());

        // when
        String invalidAccessToken = tokenProvider.getAccessToken(request);

        // then
        assertEquals(expected, invalidAccessToken);
    }

    @Test
    @DisplayName("잘못된 접두어를 가진 토큰을 파싱하면 Null 값이 반환된다.")
    void when_invalid_prefix_jwt_token_parsed_then_expected_value_should_be_null() {
        // given
        Long expected = null;
        request.addHeader("Authorization", "Dearer Token");

        // when
        String invalidAccessToken = tokenProvider.getAccessToken(request);

        // then
        assertEquals(expected, invalidAccessToken);
    }

    @Test
    @DisplayName("기한이 만료된 토큰을 파싱하면 Null 값이 반환된다.")
    void when_expired_token_parsed_then_expected_value_should_be_null() {
        // given
        Long expected = null;
        request.addHeader("Authorization", getExpiredToken());

        // when
        String invalidAccessToken = tokenProvider.getAccessToken(request);

        // then
        assertEquals(expected, invalidAccessToken);
    }

    @Test
    @DisplayName("유효한 토큰을 파싱하면 토큰 유효기간이 0보다 크다.")
    void when_valid_token_parsed_then_remainingtime_should_be_greater_than_zero() {
        // given
        String validToken = getValidParsedAccessToken();

        // when
        long remainingTime = tokenProvider.getRemainingTime(validToken);

        // then
        assertTrue(remainingTime >= 0);
    }

    @Test
    @DisplayName("유효하지 않은 토큰을 파싱하면 유효기간이 0이다.")
    void when_invalid_token_parsed_then_remainingtime_should_be_zero() {
        // given
        String inValidToken = getInvalidAccessToken();

        // when
        long remainingTime = tokenProvider.getRemainingTime(inValidToken);

        // then
        assertTrue(remainingTime == 0);
    }
}
