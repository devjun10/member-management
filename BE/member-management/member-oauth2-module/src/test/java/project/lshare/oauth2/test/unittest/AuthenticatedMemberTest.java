package project.lshare.oauth2.test.unittest;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.lshare.membercoremodule.domainmodel.authentication.valueobject.AuthenticatedMember;
import project.lshare.membercoremodule.domainmodel.member.Role;
import project.lshare.oauth2.test.helper.sut.AuthenticatedMemberSut;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertAll;
import static project.lshare.oauth2.test.helper.fixture.MemberFixture.createMemberDomainWithId;

@DisplayName("인증된 사용자 단위 테스트")
class AuthenticatedMemberTest {

    static class Nseted {

        @Test
        @DisplayName("올바른 값이 생성자에 들어오면 객체가 생성된다.")
        void when_valid_parameter_then_object_should_be_created() {
            // given, when
            AuthenticatedMember authenticatedMember = new AuthenticatedMember(createMemberDomainWithId(1L));
            AuthenticatedMemberSut sut = new AuthenticatedMemberSut(authenticatedMember);

            // then
            sut.shouldExist()
                    .withId()
                    .withRole();
        }

        @Test
        @DisplayName("Null값이 들어오면 IllegalArgumentException이 발생한다.")
        void when_null_parameter_then_object_should_throw_exception() {
            assertThatThrownBy(() -> new AuthenticatedMember(null))
                    .isInstanceOf(IllegalArgumentException.class);
        }

        @Test
        @DisplayName("역할 값이 Null이면 IllegalArgumentException이 발생한다.")
        void when_role_parameter_null_then_object_should_throw_exception() {
            assertThatThrownBy(() -> new AuthenticatedMember(1L, null))
                    .isInstanceOf(IllegalArgumentException.class);
        }

        @Test
        @DisplayName("회원 아이디 값이 Null이면 IllegalArgumentException이 발생한다.")
        void when_memberid_parameter_null_then_object_should_throw_exception() {
            assertThatThrownBy(() -> new AuthenticatedMember(null, Role.NORMAL))
                    .isInstanceOf(IllegalArgumentException.class);
        }
    }

    @Test
    @DisplayName("Equals 메서드를 재정의 하면 값으로 객체를 비교한다.")
    void when_override_equals_then_object_should_be_compared_by_value() {
        AuthenticatedMember authenticatedMember = new AuthenticatedMember(1L, Role.NORMAL);
        AuthenticatedMember otherAuthenticatedMember = new AuthenticatedMember(1L, Role.NORMAL);

        assertAll(
                () -> assertTrue(authenticatedMember.equals(otherAuthenticatedMember)),
                () -> assertTrue(authenticatedMember.equals(authenticatedMember)),
                () -> assertFalse(authenticatedMember.equals(1L))
        );
    }

    @Test
    @DisplayName("hashcode를 재정의 하면 hashcode 값으로 객체를 비교한다.")
    void when_override_hashcode_then_object_should_be_compared_by_hashcode() {
        AuthenticatedMember authenticatedMember = new AuthenticatedMember(createMemberDomainWithId(1L));
        AuthenticatedMember otherAuthenticatedMember = new AuthenticatedMember(1L, Role.NORMAL);

        assertTrue(authenticatedMember.hashCode() == otherAuthenticatedMember.hashCode());
    }

    @Test
    @DisplayName("toString을 재정의 하면 우리가 지정한 형식으로 메시지가 출력된다.")
    void when_override_tostring_then_message_should_be_the_format_what_we_set() {
        String expected = "MemberId: 1, Role: NORMAL";
        AuthenticatedMember otherAuthenticatedMember = new AuthenticatedMember(1L, Role.NORMAL);

        assertEquals(expected, otherAuthenticatedMember.toString());
    }
}
