package project.lshare.oauth2.test.acceptance.logout.success;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.test.context.TestPropertySource;
import project.lshare.oauth2.configuration.annotation.InsertData;
import project.lshare.oauth2.configuration.annotation.TestScenario;
import project.lshare.oauth2.test.acceptance.AcceptanceTestBase;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.Matchers.equalTo;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.ALL_DEVICE_LOG_OUT_CLASS;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.COMMAND;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.LOGOUT;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.SUCCESS;
import static project.lshare.oauth2.test.helper.fixture.TokenFixture.getValidAccessToken;

@TestScenario(
        target = "사용자가",
        situation = "모든 기기에서 로그아웃을 시도한다.",
        expectedResult = "모든 기기가 로그아웃 된다.",
        failureExpectedResult = {
                "올바른 토큰이 존재하지 않는 경우 401 응답을 반환한다.",
                "AccessToken / RefreshToken이 존재하지 않는 경우 403 응답을 반환한다."
        }
)
@InsertData
@AutoConfigureWireMock(port = 0)
@TestPropertySource(properties = {
        "oauth2.access-token=http://localhost:${wiremock.server.port}/api/oauth2/google/authorization",
        "oauth2.userprofile=http://localhost:${wiremock.server.port}/api/oauth2/google/userprofile"
})
@DisplayName("모든 기기 로그아웃 인수 테스트")
class LogoutAllDeviceAcceptanceTest extends AcceptanceTestBase {

    @Test
    @DisplayName("현재 로그인 된 사용자가 단일 기기에 대해 로그아웃을 시도할 경우 200응답이 반환된다.")
    void when_currently_logined_member_try_logout_of_all_devices_then_statuscode_should_be_200() {
        given(this.specification)
                .filters(createDocument(ALL_DEVICE_LOG_OUT_CLASS, LOGOUT, COMMAND, SUCCESS))
                .when()
                .contentType(JSON)
                .header("Authorization", getValidAccessToken())
                .post("/api/logout/all-devices")

                .then()
                .statusCode(equalTo(200))
                .cookie("RefreshToken")
                .log().all();
    }
}
