package project.lshare.oauth2.test.integrationtest.filter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import project.lshare.oauth2.oauth.application.port.out.OAuth2RedisService;
import project.lshare.oauth2.oauth.adapter.in.web.filter.OAuth2LoginTokenValidationFilter;
import project.lshare.oauth2.test.integrationtest.IntegrationTestBase;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static project.lshare.oauth2.test.helper.fixture.TokenFixture.getValidAccessToken;
import static project.lshare.oauth2.test.helper.fixture.TokenFixture.getValidParsedAccessToken;

@Import(OAuth2LoginTokenValidationFilter.class)
@DisplayName("블랙 리스트에 등록된 토큰 테스트")
class BlackListTest extends IntegrationTestBase {

    @Autowired
    private OAuth2LoginTokenValidationFilter filter;

    @Autowired
    private OAuth2RedisService redisService;

    private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    private MockFilterChain filterChain;

    @BeforeEach
    void setUP() {
        this.request = new MockHttpServletRequest();
        this.response = new MockHttpServletResponse();
        this.filterChain = new MockFilterChain();
    }

    @Test
    @DisplayName("블랙 리스트에 등록된 AccessToken이면 403 응답을 반환한다.")
    void when_accesstoken_registered_in_blacklist_then_statuscode_should_be_403() throws Exception {
        // given
        redisService.registerBlackList(getValidParsedAccessToken());
        request.addHeader("Authorization", getValidAccessToken());

        // when
        filter.doFilter(request, response, filterChain);

        // then
        assertEquals(403, response.getStatus());
    }
}
