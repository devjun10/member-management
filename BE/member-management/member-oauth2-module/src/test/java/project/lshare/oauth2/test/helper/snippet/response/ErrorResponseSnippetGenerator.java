package project.lshare.oauth2.test.helper.snippet.response;

import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.restdocs.payload.ResponseFieldsSnippet;

import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;

public interface ErrorResponseSnippetGenerator {

    ResponseFieldsSnippet ERROR_RESPONSE_SNIPPET = responseFields(
            fieldWithPath("time").type(JsonFieldType.STRING).description("발생 시간"),
            fieldWithPath("code").type(JsonFieldType.NUMBER).description("코드"),
            fieldWithPath("message").type(JsonFieldType.STRING).description("메시지"),
            fieldWithPath("status").type(JsonFieldType.STRING).description("상태")
    );

}
