package project.lshare.oauth2.test.acceptance.login.failure;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.test.context.TestPropertySource;
import project.lshare.oauth2.configuration.annotation.TestScenario;
import project.lshare.oauth2.test.acceptance.AcceptanceTestBase;
import project.lshare.oauth2.test.helper.stub.OAuth2LoginApiCallbackErrorStub;

import java.io.IOException;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.Matchers.equalTo;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.API_CALLBACK_FAILURE;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.COMMAND;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.FAILURE;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.LOGIN_CLASS;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.LOGIN_METHOD;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.LOGOUT_UNAUTHORIZED_RESPONSE_SNIPPET;
import static project.lshare.oauth2.test.helper.fixture.TokenFixture.getInvalidOAuth2Code;

@TestScenario(
        target = "사용자가",
        situation = "로그인/회원가입을 하며 code를 전송한다.",
        expectedResult = "400 응답을 받는다."
)
@AutoConfigureWireMock(port = 0)
@TestPropertySource(properties = {"oauth2.access-token=http://localhost:${wiremock.server.port}/api/oauth2/google/authorization/bad-gateway"})
@DisplayName("OAuth2 로그인/회원가입 실패(400) 인수 테스트")
class OAuth2LoginApiCallErrorAcceptanceTest extends AcceptanceTestBase {

    @BeforeAll
    static void setStub() throws IOException {
        OAuth2LoginApiCallbackErrorStub.setUP();
    }

    @Test
    @DisplayName("code가 올바르지 않다면 400 응답이 반환된다.")
    void when_code_is_invalid_then_statuscode_should_be_400() {
        given(this.specification)
                .filters(createDocument(LOGIN_CLASS, LOGIN_METHOD, COMMAND, FAILURE, API_CALLBACK_FAILURE, LOGOUT_UNAUTHORIZED_RESPONSE_SNIPPET))

                .when()
                .contentType(JSON)
                .queryParam("code", getInvalidOAuth2Code())
                .get("/api/oauth2/login")

                .then()
                .statusCode(equalTo(400))
                .log().all();
    }
}
