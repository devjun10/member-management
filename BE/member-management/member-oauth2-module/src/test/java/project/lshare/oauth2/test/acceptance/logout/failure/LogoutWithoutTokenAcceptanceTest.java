package project.lshare.oauth2.test.acceptance.logout.failure;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.lshare.oauth2.configuration.annotation.InsertData;
import project.lshare.oauth2.configuration.annotation.LoginAcceptanceTest;
import project.lshare.oauth2.configuration.annotation.TestScenario;
import project.lshare.oauth2.test.acceptance.AcceptanceTestBase;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.Matchers.equalTo;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.ALL_DEVICE_LOG_OUT_CLASS;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.COMMAND;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.FAILURE;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.FAILURE_UNAUTHORIZED;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.LOGOUT;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.LOGOUT_UNAUTHORIZED_RESPONSE_SNIPPET;

@TestScenario(
        target = "사용자가",
        situation = "AccessToken, RefreshToken 없이 모든 기기 로그아웃을 시도한다.",
        expectedResult = "401 응답이 반환된다."
)
@InsertData
@LoginAcceptanceTest
@DisplayName("모든 기기 로그아웃 실패(401) 인수 테스트")
class LogoutWithoutTokenAcceptanceTest extends AcceptanceTestBase {

    @Test
    @DisplayName("AccessToken, RefreshToken이 존재하지 않는 상태로 로그인을 시도하면 401 응답이 반환된다.")
    void when_try_logout_of_all_devices_without_token_then_statuscode_shoduld_be_401() {
        given(this.specification)
                .filters(createDocument(ALL_DEVICE_LOG_OUT_CLASS, LOGOUT, COMMAND, FAILURE, FAILURE_UNAUTHORIZED, LOGOUT_UNAUTHORIZED_RESPONSE_SNIPPET))

                .when()
                .contentType(JSON)
                .post("/api/logout/all-devices")

                .then()
                .statusCode(equalTo(401))
                .cookie("RefreshToken")
                .log().all();
    }
}
