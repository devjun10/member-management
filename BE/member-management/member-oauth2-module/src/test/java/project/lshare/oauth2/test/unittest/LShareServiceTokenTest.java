package project.lshare.oauth2.test.unittest;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.lshare.oauth2.oauth.application.service.LShareServiceToken;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertAll;
import static project.lshare.oauth2.test.helper.fixture.TokenFixture.getValidAccessToken;
import static project.lshare.oauth2.test.helper.fixture.TokenFixture.getValidRefreshToken;

@DisplayName("서비스 토큰 단위 테스트")
class LShareServiceTokenTest {

    static class Nested {

        @Test
        @DisplayName("AccessToken과 RefreshToken이 정상적으로 들어오면 객체가 생성된다.")
        void when_accesstoken_and_refreshtoken_is_valid_then_object_should_be_created() {
            assertNotNull(new LShareServiceToken(getValidAccessToken(), getValidRefreshToken()));
        }

        @Test
        @DisplayName("AccessToken 값이 Null이면 IllegalArgumentException이 발생한다.")
        void when_accesstoken_is_null_then_object_should_throw_exception() {
            assertThatThrownBy(() -> new LShareServiceToken(null, getValidRefreshToken()))
                    .isInstanceOf(IllegalArgumentException.class);
        }

        @Test
        @DisplayName("RefreshToken 값이 Null이면 IllegalArgumentException이 발생한다.")
        void when_refreshtoken_is_null_then_object_should_throw_exception() {
            assertThatThrownBy(() -> new LShareServiceToken(getValidAccessToken(), null))
                    .isInstanceOf(IllegalArgumentException.class);
        }
    }

    @Test
    @DisplayName("Equals 메서드를 재정의 하면 값으로 객체를 비교한다.")
    void when_override_equals_then_object_should_be_compared_by_value() {
        // given
        LShareServiceToken serviceToken = new LShareServiceToken(
                getValidAccessToken(),
                getValidRefreshToken()
        );

        LShareServiceToken otherServiceToken = new LShareServiceToken(
                getValidRefreshToken(),
                getValidRefreshToken()
        );

        // when, then
        assertAll(
                () -> assertTrue(serviceToken.equals(serviceToken)),
                () -> assertFalse(serviceToken.equals(otherServiceToken)),
                () -> assertFalse(serviceToken.equals(1L))
        );
    }

    @Test
    @DisplayName("hashcode를 재정의 하면 hashcode 값으로 객체를 비교한다.")
    void when_override_hashcode_then_object_should_be_compared_by_hashcode() {
        // given
        LShareServiceToken serviceToken = new LShareServiceToken(
                getValidAccessToken(),
                getValidRefreshToken()
        );

        LShareServiceToken otherServiceToken = new LShareServiceToken(
                getValidAccessToken(),
                getValidRefreshToken()
        );

        // when, then
        assertTrue(serviceToken.hashCode() == otherServiceToken.hashCode());
    }


    @Test
    @DisplayName("toString을 재정의 하면 우리가 지정한 형식으로 메시지가 출력된다.")
    void when_override_tostring_then_message_should_be_the_format_what_we_set() {
        // given
        String accessToken = getValidAccessToken();
        String refreshToken = getValidRefreshToken();
        String expected = "AccessToken: " + accessToken + ", RefreshToken: " + refreshToken;

        // when, then
        assertEquals(expected, new LShareServiceToken(accessToken, refreshToken).toString());
    }
}
