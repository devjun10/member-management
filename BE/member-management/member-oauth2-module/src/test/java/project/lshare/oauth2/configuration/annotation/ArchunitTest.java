package project.lshare.oauth2.configuration.annotation;

import com.tngtech.archunit.junit.AnalyzeClasses;
import org.junit.jupiter.api.TestClassOrder;
import project.lshare.oauth2.MemberOauth2ModuleApplication;
import project.lshare.oauth2.configuration.testorder.TestOrder;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Inherited
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@TestClassOrder(value = TestOrder.class)
@AnalyzeClasses(packages = "project.lshare.member-oauth2-module", packagesOf = MemberOauth2ModuleApplication.class)
public @interface ArchunitTest {
}
