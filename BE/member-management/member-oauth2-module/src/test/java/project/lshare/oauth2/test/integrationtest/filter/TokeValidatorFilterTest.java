package project.lshare.oauth2.test.integrationtest.filter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.core.MethodParameter;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.method.support.ModelAndViewContainer;
import project.lshare.membercoremodule.domainmodel.authentication.valueobject.AuthenticatedMember;
import project.lshare.membercoremodule.domainmodel.member.Role;
import project.lshare.membercoremodule.persistencemodel.member.MemberJpaEntity;
import project.lshare.oauth2.oauth.adapter.in.web.argumentresolver.AuthenticationCheckArgumentResolver;
import project.lshare.oauth2.oauth.adapter.in.web.filter.TokenValidationFilter;
import project.lshare.oauth2.oauth.application.port.out.OAuth2RedisService;
import project.lshare.oauth2.test.helper.persistence.PersistenceHelper;
import project.lshare.oauth2.test.integrationtest.IntegrationTestBase;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static project.lshare.oauth2.test.helper.fixture.CookieFixture.createInvalidRefreshTokenCooki;
import static project.lshare.oauth2.test.helper.fixture.CookieFixture.createValidRefreshTokenCooki;
import static project.lshare.oauth2.test.helper.fixture.MemberFixture.createMemberWithoutId;
import static project.lshare.oauth2.test.helper.fixture.TokenFixture.getInvalidAccessToken;
import static project.lshare.oauth2.test.helper.fixture.TokenFixture.getValidAccessToken;
import static project.lshare.oauth2.test.helper.fixture.TokenFixture.getValidParsedAccessToken;
import static project.lshare.oauth2.test.helper.fixture.TokenFixture.getValidRefreshToken;

@Import(TokenValidationFilter.class)
@DisplayName("토큰 검증 필터 테스트")
class TokeValidatorFilterTest extends IntegrationTestBase {

    @Autowired
    private TokenValidationFilter filter;

    @Autowired
    private OAuth2RedisService redisService;

    @Autowired
    private PersistenceHelper persistenceHelper;

    @Autowired
    private AuthenticationCheckArgumentResolver argumentResolver;

    @MockBean
    private MethodParameter parameter;

    @MockBean
    private ModelAndViewContainer mavContainer;

    @MockBean
    private NativeWebRequest webRequest;

    @MockBean
    private WebDataBinderFactory binderFactory;

    private MockHttpServletRequest request;
    private MockHttpServletResponse response;
    private MockFilterChain filterChain;
    private MemberJpaEntity member;

    @BeforeEach
    void setUP() {
        this.request = new MockHttpServletRequest();
        this.response = new MockHttpServletResponse();
        this.filterChain = new MockFilterChain();
        this.webRequest = new ServletWebRequest(request, response);
        this.member = persistenceHelper.persist(createMemberWithoutId());
    }

    @Test
    @DisplayName("유효한 AccessToken이 헤더에 존재하면 302 응답이 반환된다.")
    void when_request_has_valid_accesstoken_in_header_then_statuscode_should_be_302() throws Exception {
        // given
        Long expectedId = 1L;
        Role expectedRole = Role.NORMAL;
        request.addHeader("Authorization", getValidAccessToken());

        // when
        filter.doFilter(request, response, filterChain);

        Object argument = argumentResolver.resolveArgument(parameter, mavContainer, webRequest, binderFactory);
        AuthenticatedMember authenticatedMember = (AuthenticatedMember) argument;

        assertAll(
                () -> assertEquals(expectedId, authenticatedMember.getMemberId()),
                () -> assertEquals(expectedRole, authenticatedMember.getRole())
        );
    }

    @Test
    @DisplayName("유효한 AccessToken이 헤더에 존재하면 302 응답이 반환된다.")
    void when_request_has_3valid_accesstoken_in_header_then_statuscode_should_be_302() throws Exception {
        // given
        redisService.registerBlackList(getValidParsedAccessToken());
        request.addHeader("Authorization", getValidAccessToken());

        // when
        filter.doFilter(request, response, filterChain);

        assertEquals(403, response.getStatus());
    }

    @Test
    @DisplayName("AccessToken이 유효하지 않고 RefreshToken이 Null 이라면 401 응답이 반환된다.")
    void when_request_has_invalid_accesstoken_and_refreshtoken_is_null_then_statuscode_should_be_401() throws Exception {
        // given
        request.addHeader("Authorization", getInvalidAccessToken());

        // when
        filter.doFilter(request, response, filterChain);

        // then
        assertEquals(401, response.getStatus());
    }

    @Test
    @DisplayName("AccessToken이 유효하지 않고 RefreshToken이 유효하다면 302 응답이 반환된다.")
    void when_request_has_invalid_accesstoken_and_valid_refreshtoken_then_statuscode_should_be_302() throws Exception {
        // given
        Long expectedId = 1L;
        Role expectedRole = Role.NORMAL;
        redisService.saveRefreshToken(member.getMemberId(), getValidRefreshToken());
        request.addHeader("Authorization", getInvalidAccessToken());
        request.setCookies(createValidRefreshTokenCooki());

        // when
        filter.doFilter(request, response, filterChain);

        Object argument = argumentResolver.resolveArgument(parameter, mavContainer, webRequest, binderFactory);
        AuthenticatedMember authenticatedMember = (AuthenticatedMember) argument;

        assertAll(
                () -> assertEquals(expectedId, authenticatedMember.getMemberId()),
                () -> assertEquals(expectedRole, authenticatedMember.getRole())
        );
    }

    @Test
    @DisplayName("AccessToken이 유효하지 않고 쿠키에 담긴 RefreshToken이 레디스에 저장된 RefreshToken과 다르다면 403 응답이 반환된다.")
    void when_request_has_invalid_accesstoken_and_refreshtoken_in_cookie_is_different_from_redis_saved_refreshtoken_then_statuscode_should_be_403() throws Exception {
        // given
        redisService.saveRefreshToken(member.getMemberId(), getValidAccessToken());
        request.addHeader("Authorization", getInvalidAccessToken());
        request.setCookies(createValidRefreshTokenCooki());

        // when
        filter.doFilter(request, response, filterChain);

        // then
        assertEquals(403, response.getStatus());
    }

    @Test
    @DisplayName("AccessToken이 유효하지 않고 RefreshToken도 유효하지 않다면 302 응답이 반환되지 않는다.")
    void when_request_has_invalid_accesstoken_and_invalid_refreshtoken_then_statuscode_should_not_be_302() throws Exception {
        // given
        request.addHeader("Authorization", getInvalidAccessToken());
        request.setCookies(createInvalidRefreshTokenCooki());

        // when
        filter.doFilter(request, response, filterChain);

        // then
        assertEquals(401, response.getStatus());
    }
}
