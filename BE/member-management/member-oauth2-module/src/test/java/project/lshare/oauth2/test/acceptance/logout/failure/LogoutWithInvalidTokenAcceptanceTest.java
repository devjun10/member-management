package project.lshare.oauth2.test.acceptance.logout.failure;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.lshare.oauth2.configuration.annotation.InsertData;
import project.lshare.oauth2.configuration.annotation.LoginAcceptanceTest;
import project.lshare.oauth2.configuration.annotation.TestScenario;
import project.lshare.oauth2.test.acceptance.AcceptanceTestBase;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.Matchers.equalTo;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.ALL_DEVICE_LOG_OUT_CLASS;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.COMMAND;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.FAILURE;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.FAILURE_FORBIDDEN_ACCESS;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.LOGOUT;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.LOGOUT_FORBIDDEN_RESPONSE_SNIPPET;
import static project.lshare.oauth2.test.helper.fixture.TokenFixture.getInvalidAccessToken;
import static project.lshare.oauth2.test.helper.fixture.TokenFixture.getValidRefreshToken;

@TestScenario(
        target = "사용자가",
        situation = "올바르지 않은 AccessToken, RefreshToken을 가지고 모든 기기 로그아웃을 시도한다.",
        expectedResult = "403 응답이 반환된다."
)
@InsertData
@LoginAcceptanceTest
@DisplayName("모든 기기 로그아웃 실패(403) 인수 테스트")
class LogoutWithInvalidTokenAcceptanceTest extends AcceptanceTestBase {

    @Test
    @DisplayName("올바르지 않은 토큰을 가지고 로그아웃을 시도하면 403 응답이 반환된다.")
    void when_try_logout_of_all_devices_with_invalid_token_then_statuscode_shoduld_be_403() {
        given(this.specification)
                .filters(createDocument(ALL_DEVICE_LOG_OUT_CLASS, LOGOUT, COMMAND, FAILURE, FAILURE_FORBIDDEN_ACCESS, LOGOUT_FORBIDDEN_RESPONSE_SNIPPET))

                .when()
                .contentType(JSON)
                .header("Authorization", getInvalidAccessToken())
                .cookie("RefreshToken", getValidRefreshToken())
                .post("/api/logout/all-devices")

                .then()
                .statusCode(equalTo(403))
                .cookie("RefreshToken")
                .log().all();
    }
}
