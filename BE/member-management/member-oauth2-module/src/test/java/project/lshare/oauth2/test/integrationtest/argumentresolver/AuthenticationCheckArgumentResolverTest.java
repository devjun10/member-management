package project.lshare.oauth2.test.integrationtest.argumentresolver;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.MethodParameter;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.method.support.ModelAndViewContainer;
import project.lshare.membercoremodule.common.mapper.BusinessException;
import project.lshare.membercoremodule.domainmodel.member.Role;
import project.lshare.oauth2.oauth.adapter.in.web.GoogleLogoutController;
import project.lshare.oauth2.oauth.adapter.in.web.argumentresolver.AuthenticationCheckArgumentResolver;
import project.lshare.membercoremodule.domainmodel.authentication.valueobject.AuthenticatedMember;
import project.lshare.oauth2.test.helper.persistence.PersistenceHelper;
import project.lshare.oauth2.test.integrationtest.IntegrationTestBase;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertAll;
import static project.lshare.oauth2.test.helper.fixture.MemberFixture.createMemberWithoutId;

@DisplayName("사용자 인증 파라미터 변환 테스트")
public class AuthenticationCheckArgumentResolverTest extends IntegrationTestBase {

    @Autowired
    private AuthenticationCheckArgumentResolver argumentResolver;

    @MockBean
    private MethodParameter parameter;

    @MockBean
    private ModelAndViewContainer mavContainer;

    @MockBean
    private NativeWebRequest webRequest;

    @MockBean
    private WebDataBinderFactory binderFactory;

    @Autowired
    private PersistenceHelper persistenceHelper;

    private MockHttpServletRequest request;
    private MockHttpServletResponse response;

    @BeforeEach
    void setUP() {
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
        webRequest = new ServletWebRequest(request, response);
    }

    @Test
    @DisplayName("요청(WebRequest)이 올바른 회원 아이디를 가지고 있지 않다면 회원을 조회할 수 없다.")
    void when_request_has_not_memberid_then_argumentresolver_cannot_find_member() {
        assertThatThrownBy(() -> argumentResolver.resolveArgument(parameter, mavContainer, webRequest, binderFactory))
                .isInstanceOf(BusinessException.class)
                .hasMessage("회원을 찾을 수 없습니다.");
    }

    @Test
    @DisplayName("메서드 인자에 존재하는 타입의 정보를 알 수 있다.")
    void when_method_has_parameter_then_argumentresolver_can_identify_method_parameter_type() throws NoSuchMethodException {
        // given
        boolean expected = true;
        Method method = GoogleLogoutController.class.getMethod("logout", AuthenticatedMember.class, HttpServletRequest.class);
        MethodParameter methodParameter = new MethodParameter(method, 0);

        // when, then
        assertEquals(expected, argumentResolver.supportsParameter(methodParameter));
    }

    @Test
    @DisplayName("요청(WebRequest)이 올바른 회원 아이디를 가지고 있다면 회원을 조회할 수 있다.")
    void when_request_has_memberid_then_argumentresolver_can_find_member() {
        // given
        Long expectedId = 1L;
        Role expectedRole = Role.NORMAL;
        request.setAttribute("memberId", 1L);
        persistenceHelper.persist(createMemberWithoutId());

        // when
        Object argument = argumentResolver.resolveArgument(parameter, mavContainer, webRequest, binderFactory);

        // then
        AuthenticatedMember authenticatedMember = (AuthenticatedMember) argument;
        assertAll(
                () -> assertEquals(expectedId, authenticatedMember.getMemberId()),
                () -> assertEquals(expectedRole, authenticatedMember.getRole())
        );
    }
}
