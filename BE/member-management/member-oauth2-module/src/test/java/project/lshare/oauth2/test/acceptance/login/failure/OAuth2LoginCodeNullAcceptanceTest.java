package project.lshare.oauth2.test.acceptance.login.failure;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.lshare.oauth2.configuration.annotation.TestScenario;
import project.lshare.oauth2.test.acceptance.AcceptanceTestBase;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.Matchers.equalTo;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.BAD_REQUEST;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.COMMAND;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.FAILURE;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.LOGIN_CLASS;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.LOGIN_METHOD;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.LOGOUT_UNAUTHORIZED_RESPONSE_SNIPPET;

@TestScenario(
        target = "사용자가",
        situation = "로그인/회원가입을 시도하며 code를 전송하지 않는다.",
        expectedResult = "400 응답을 받는다."
)
@DisplayName("OAuth2 로그인/회원가입 실패(400) 인수 테스")
class OAuth2LoginCodeNullAcceptanceTest extends AcceptanceTestBase {

    @Test
    @DisplayName("code가 존재하지 않으면 400 응답을 반환한다.")
    void when_code_is_null_then_statuscode_should_be_400() {
        given(this.specification)
                .filters(createDocument(LOGIN_CLASS, LOGIN_METHOD, COMMAND, FAILURE, BAD_REQUEST, LOGOUT_UNAUTHORIZED_RESPONSE_SNIPPET))

                .when()
                .contentType(JSON)
                .get("/api/oauth2/login")

                .then()
                .statusCode(equalTo(400))
                .log().all();
    }
}
