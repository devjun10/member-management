package project.lshare.oauth2.test.helper.fixture;

import javax.servlet.http.Cookie;

import static project.lshare.oauth2.test.helper.fixture.TokenFixture.getInValidRefreshToken;
import static project.lshare.oauth2.test.helper.fixture.TokenFixture.getValidRefreshToken;

public class CookieFixture {

    private CookieFixture() {
        throw new AssertionError("올바른 방법으로 생성자를 호출해주세요.");
    }

    public static Cookie createValidRefreshTokenCooki() {
        return new Cookie("RefreshToken", getValidRefreshToken());
    }

    public static Cookie createInvalidRefreshTokenCooki() {
        return new Cookie("RefreshToken", getInValidRefreshToken());
    }
}
