package project.lshare.oauth2.test.architecture.layeredtest.presentation;

public interface PresentationLayerTestConstant {

    String OAUTH2_PRESENTATION_LAYER = "PresentationLayer";
    String OAUTH2_PRESENTATION_LAYER_PATH = "project.lshare.oauth2.oauth.adapter.in.web";

    String ARGUMENT_RESOLVER_TEST_LAYER = "AuthenticationCheckArgumentResolverTest";
    String TEST_LAYER_PATH = "project.lshare.oauth2.test.integrationtest.argumentresolver";

}
