package project.lshare.oauth2.test.helper.snippet.restdocsutils;

import java.util.Objects;

public class Constraint {

    private final String constraint;

    public Constraint(String constraint) {
        this.constraint = constraint;
    }

    public String getConstraint() {
        return constraint;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Constraint that)) return false;
        return getConstraint().equals(that.getConstraint());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getConstraint());
    }

    @Override
    public String toString() {
        return constraint;
    }
}
