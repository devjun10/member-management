package project.lshare.oauth2.test.architecture.packagetest.response;

import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;
import org.junit.jupiter.api.DisplayName;
import project.lshare.membercoremodule.common.annotation.helper.Tag;
import project.lshare.oauth2.MemberOauth2ModuleApplication;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;

@DisplayName("응답 클래스 패키지 테스트")
@AnalyzeClasses(packages = "project.lshare.member-oauth2-module", packagesOf = MemberOauth2ModuleApplication.class)
class ResponseClassPackageTest {

    @ArchTest
    @Tag(description = "응답 클래스는 response 패키지 내부에 있어야 한다.")
    static final ArchRule response_class_should_reside_in_the_responsepackage =
            classes().that().haveSimpleNameContaining("Response")
                    .should()
                    .resideInAPackage("..response..")
                    .as("ResponseClass should reside in a package '..response..'");
}
