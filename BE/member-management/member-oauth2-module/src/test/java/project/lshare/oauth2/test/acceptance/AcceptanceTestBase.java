package project.lshare.oauth2.test.acceptance;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.config.ObjectMapperConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.constraints.ConstraintDescriptions;
import org.springframework.restdocs.operation.preprocess.OperationPreprocessor;
import org.springframework.restdocs.operation.preprocess.OperationRequestPreprocessor;
import org.springframework.restdocs.operation.preprocess.OperationResponsePreprocessor;
import org.springframework.restdocs.operation.preprocess.Preprocessors;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.restdocs.payload.ResponseFieldsSnippet;
import org.springframework.restdocs.request.RequestParametersSnippet;
import org.springframework.restdocs.restassured3.RestAssuredOperationPreprocessorsConfigurer;
import org.springframework.restdocs.restassured3.RestDocumentationFilter;
import org.springframework.restdocs.snippet.Attributes;
import org.springframework.util.StringUtils;
import project.lshare.oauth2.configuration.annotation.AcceptanceTest;
import project.lshare.oauth2.configuration.rdb.RDBInitializationConfiguration;
import project.lshare.oauth2.configuration.redis.EmbeddedRedisInitializationConfiguration;

import java.util.List;
import java.util.Objects;

import static org.hibernate.criterion.Restrictions.isNotNull;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.modifyUris;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.requestParameters;
import static org.springframework.restdocs.restassured3.RestAssuredRestDocumentation.document;
import static org.springframework.restdocs.restassured3.RestAssuredRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.snippet.Attributes.key;

@AcceptanceTest
public abstract class AcceptanceTestBase {

    @Autowired
    private RDBInitializationConfiguration databaseInitialization;

    @Autowired
    private EmbeddedRedisInitializationConfiguration redisInitialization;

    @LocalServerPort
    protected int port;

    @Autowired
    protected ObjectMapper objectMapper;

    protected RequestSpecification specification;

    private static ConstraintDescriptions requestConstraints;

    public AcceptanceTestBase() {
        initRestAssureConfiguration();
    }

    @BeforeEach
    void beforeEach(RestDocumentationContextProvider restDocumentation) {
        redisInitialization.init();
        RestAssured.port = port;

        OperationPreprocessor operationPreprocessor = modifyUris()
                .host("www.lshare-study.com")
                .removePort();

        RestAssuredOperationPreprocessorsConfigurer restDocumentationFilter = documentationConfiguration(restDocumentation)
                .operationPreprocessors()
                .withRequestDefaults(operationPreprocessor, prettyPrint())
                .withResponseDefaults(prettyPrint());

        this.specification = new RequestSpecBuilder()
                .setPort(port)
                .addFilter(restDocumentationFilter)
                .build();
    }

    @AfterEach
    void afterEach() {
        databaseInitialization.afterPropertiesSet();
        databaseInitialization.truncateAllEntity();
    }

    private void initRestAssureConfiguration() {
        objectMapper = new ObjectMapper()
                .disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        // 모든 정보 출력
        objectMapper.setSerializationInclusion(JsonInclude.Include.ALWAYS);

        // https://stackoverflow.com/questions/58794871/restassured-does-not-respect-objectmapper-configuration-in-quarkus
        RestAssured.config = new RestAssuredConfig().objectMapperConfig(
                new ObjectMapperConfig().jackson2ObjectMapperFactory((clazz, charset) -> objectMapper)
        );
    }

    protected RestDocumentationFilter createDocument(
            String className,
            String methodName,
            String cqsPattern,
            String successOrNot,
            RequestParametersSnippet requestParametersSnippet
    ) {
        return document(
                createDirectory(className, methodName, cqsPattern, successOrNot),
                getDocumentRequest(),
                requestParametersSnippet
        );
    }

    protected RestDocumentationFilter createDocument(
            String className,
            String methodName,
            String cqsPattern,
            String successOrNot,
            ResponseFieldsSnippet responseFieldsSnippet
    ) {
        return document(
                createDirectory(className, methodName, cqsPattern, successOrNot),
                getDocumentRequest(),
                responseFieldsSnippet
        );
    }

    protected RestDocumentationFilter createDocument(
            String className,
            String methodName,
            String cqsPattern,
            String successOrNot,
            String failureCase,
            RequestParametersSnippet requestParametersSnippet
    ) {
        return document(
                createDirectory(className, methodName, cqsPattern, successOrNot, failureCase),
                getDocumentRequest(),
                requestParametersSnippet
        );
    }

    protected RestDocumentationFilter createDocument(
            String className,
            String methodName,
            String cqsPattern,
            String successOrNot,
            RequestParametersSnippet requestParametersSnippet,
            ResponseFieldsSnippet responseFieldsSnippet
    ) {
        return document(
                createDirectory(className, methodName, cqsPattern, successOrNot),
                getDocumentRequest(),
                requestParametersSnippet,
                responseFieldsSnippet
        );
    }

    protected RestDocumentationFilter createDocument(
            String className,
            String methodName,
            String cqsPattern,
            String successOrNot
    ) {
        return document(
                createDirectory(className, methodName, cqsPattern, successOrNot),
                getDocumentRequest()
        );
    }

    protected RestDocumentationFilter createDocument(
            String className,
            String methodName,
            String cqsPattern,
            String successOrNot,
            String failureCase,
            ResponseFieldsSnippet responseFieldsSnippet
    ) {
        return document(
                createDirectory(className, methodName, cqsPattern, successOrNot, failureCase),
                getDocumentRequest(),
                responseFieldsSnippet
        );
    }

    protected static RequestParametersSnippet getRequestParameterSnippet(
            String parameterName,
            String description,
            boolean optional,
            String type
    ) {
        if (optional) {
            return requestParameters(
                    parameterWithName(parameterName)
                            .description(description)
                            .optional()
                            .attributes(getType(type))
                            .attributes(getValidationAttributes(
                                    isNotNull(parameterName))
                            )
            );
        }
        return requestParameters(
                parameterWithName(parameterName)
                        .description(description)
                        .attributes(getType(type))
                        .attributes(getValidationAttributes(
                                isNotNull(parameterName))
                        )
        );
    }

    protected static Attributes.Attribute getType(Object validation) {
        return key("type").value(validation);
    }

    protected static Attributes.Attribute getValidationAttribute(Object validation) {
        return key("constraints").value(validation);
    }

    protected static Attributes.Attribute getValidationAttributes(Object... validation) {
        StringBuilder validations = new StringBuilder();
        for (Object object : validation) {
            validations.append("- ").append(object).append("\n\n");
        }
        return key("constraints").value(validations.toString());
    }

    protected String createDirectory(String... directories) {
        StringBuffer stringBuffer = new StringBuffer();

        for (String directory : directories) {
            stringBuffer.append(directory).append("/");
        }

        return stringBuffer.toString();
    }

    public static FieldDescriptor extractFieldDescriptor(String path,
                                                         JsonFieldType jsonFieldType,
                                                         String description,
                                                         Class<?> clazz) {
        if (Objects.isNull(clazz)) {
            return fieldWithPath(path)
                    .type(jsonFieldType)
                    .description(description)
                    .attributes(key("constraints").value(""));
        }

        requestConstraints = new ConstraintDescriptions(clazz);
        List<String> constraints = requestConstraints.descriptionsForProperty(path);

        if (Objects.isNull(constraints) || constraints.isEmpty()) {
            return fieldWithPath(path)
                    .type(jsonFieldType)
                    .description(description)
                    .attributes(key("constraints").value(""));
        }
        return fieldWithPath(path)
                .type(jsonFieldType)
                .description(description)
                .attributes(key("constraints").value(
                                "- " + StringUtils.collectionToDelimitedString(
                                        requestConstraints.descriptionsForProperty(path), "\n\n- ")
                        )
                );
    }

    protected static OperationRequestPreprocessor getDocumentRequest() {
        return Preprocessors.preprocessRequest(prettyPrint());
    }

    protected static OperationResponsePreprocessor getDocumentResponse() {
        return Preprocessors.preprocessResponse(Preprocessors.prettyPrint());
    }

    private static RestAssuredOperationPreprocessorsConfigurer documentationFilter(RestDocumentationContextProvider restDocumentation,
                                                                                   OperationPreprocessor requestOperationProcessor) {
        return documentationConfiguration(restDocumentation)
                .operationPreprocessors()
                .withRequestDefaults(requestOperationProcessor, prettyPrint())
                .withResponseDefaults(prettyPrint());
    }
}
