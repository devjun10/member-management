package project.lshare.oauth2.configuration.annotation;

import org.junit.jupiter.api.TestClassOrder;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import project.lshare.oauth2.configuration.testorder.TestOrder;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Inherited
@SpringBootTest
@ActiveProfiles("test")
@Target(ElementType.TYPE)
@TestClassOrder(value = TestOrder.class)
@Retention(RetentionPolicy.RUNTIME)
public @interface IntegrationTest {
}
