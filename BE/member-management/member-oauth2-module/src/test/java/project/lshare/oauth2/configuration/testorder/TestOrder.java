package project.lshare.oauth2.configuration.testorder;

import org.junit.jupiter.api.ClassDescriptor;
import org.junit.jupiter.api.ClassOrderer;
import org.junit.jupiter.api.ClassOrdererContext;
import project.lshare.oauth2.configuration.annotation.ArchunitTest;
import project.lshare.oauth2.test.acceptance.AcceptanceTestBase;
import project.lshare.oauth2.test.integrationtest.IntegrationTestBase;

import java.util.Comparator;

public class TestOrder implements ClassOrderer {

    @Override
    public void orderClasses(ClassOrdererContext classOrdererContext) {
        classOrdererContext.getClassDescriptors().sort(Comparator.comparingInt(TestOrder::getOrder));
    }

    private static int getOrder(ClassDescriptor classDescriptor) {
        if (classDescriptor.getTestClass().getSuperclass().equals(IntegrationTestBase.class)) {
            return 1;
        } else if (classDescriptor.getTestClass().getSuperclass().equals(AcceptanceTestBase.class)) {
            return 2;
        } else if (classDescriptor.getTestClass().isAnnotationPresent(ArchunitTest.class)) {
            return 3;
        } else {
            return 4;
        }
    }
}
