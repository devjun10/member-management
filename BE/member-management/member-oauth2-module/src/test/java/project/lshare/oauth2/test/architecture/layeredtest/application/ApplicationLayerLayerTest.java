package project.lshare.oauth2.test.architecture.layeredtest.application;

import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;
import org.junit.jupiter.api.DisplayName;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import project.lshare.membercoremodule.common.annotation.helper.PersistenceAdapter;
import project.lshare.membercoremodule.common.annotation.helper.Tag;
import project.lshare.oauth2.MemberOauth2ModuleApplication;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;

@DisplayName("Application 계층 테스트")
@AnalyzeClasses(packages = "project.lshare.member-oauth2-module", packagesOf = MemberOauth2ModuleApplication.class)
class ApplicationLayerLayerTest {

    @ArchTest
    @Tag(description = "Application 계층은 Persistnece 계층, 외부 API를 호출하는 계층 또는 개발자가 정의한 Custom한 레이어만 호출한다.")
    ArchRule application_layer_should_be_accessed_by_persistence_layer_or_custom_component_layer = classes().that().areAnnotatedWith(Service.class)
            .should().accessClassesThat().areAnnotatedWith(PersistenceAdapter.class)
            .orShould().accessClassesThat().areAnnotatedWith(Component.class);

}
