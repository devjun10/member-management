package project.lshare.oauth2.test.architecture.cycletest;

import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;
import org.junit.jupiter.api.DisplayName;
import project.lshare.membercoremodule.common.annotation.helper.Tag;
import project.lshare.oauth2.configuration.annotation.ArchunitTest;

import static com.tngtech.archunit.library.dependencies.SlicesRuleDefinition.slices;

@ArchunitTest
@DisplayName("어노테이션 패키지 테스트")
public class DependencyCycleTest {

    @ArchTest
    @Tag(description = "모든 클래스는 순환 의존성이 없어야 한다.")
    static final ArchRule all_classes_should_be_free_of_cyclic_dependence =
            slices().matching("..project.lshare.(*)..")
                    .should()
                    .beFreeOfCycles();

}
