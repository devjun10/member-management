package project.lshare.oauth2.test.helper.snippet.restdocsutils;

import java.util.Objects;

public class Parameter {

    private final String parameter;

    public Parameter(String parameter) {
        validateParameter(parameter);
        this.parameter = parameter;
    }

    private void validateParameter(String parameter) {
        if (parameter == null || parameter.isBlank()) {
            throw new IllegalArgumentException("파라미터를 입력해주세요.");
        }
    }

    public String getParameter() {
        return parameter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Parameter parameter1)) return false;
        return getParameter().equals(parameter1.getParameter());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getParameter());
    }

    @Override
    public String toString() {
        return parameter;
    }
}
