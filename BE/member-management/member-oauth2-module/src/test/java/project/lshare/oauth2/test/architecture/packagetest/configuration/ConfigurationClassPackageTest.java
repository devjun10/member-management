package project.lshare.oauth2.test.architecture.packagetest.configuration;

import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;
import org.junit.jupiter.api.DisplayName;
import project.lshare.membercoremodule.common.annotation.helper.Tag;
import project.lshare.oauth2.MemberOauth2ModuleApplication;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;

@DisplayName("설정 클래스 패키지 테스트")
@AnalyzeClasses(packages = "project.lshare.member-oauth2-module", packagesOf = MemberOauth2ModuleApplication.class)
class ConfigurationClassPackageTest {

    @ArchTest
    @Tag(description = "설정 클래스는 configuration 패키지 내부에 있어야 한다.")
    static final ArchRule configuration_class_should_reside_in_the_configurationpackage =
            classes().that().haveSimpleNameContaining("Configuration")
                    .should()
                    .resideInAPackage("..configuration..")
                    .as("Configuration should reside in a package '..configuration..'");
}
