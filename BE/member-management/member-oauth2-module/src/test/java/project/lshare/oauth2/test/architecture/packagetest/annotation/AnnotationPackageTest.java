package project.lshare.oauth2.test.architecture.packagetest.annotation;

import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;
import org.junit.jupiter.api.DisplayName;
import project.lshare.membercoremodule.common.annotation.helper.Tag;
import project.lshare.oauth2.MemberOauth2ModuleApplication;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;

@DisplayName("어노테이션 패키지 테스트")
@AnalyzeClasses(packages = "project.lshare.member-oauth2-module", packagesOf = MemberOauth2ModuleApplication.class)
class AnnotationPackageTest {

    @ArchTest
    @Tag(description = "커스텀 어노테이션은 annotation 패키지 내부에 존재해야 한다.")
    static final ArchRule annotation_should_reside_in_the_annotationpackage =
            classes().that().resideInAPackage("..annotation..")
                    .should()
                    .resideInAPackage("..annotation..")
                    .as("Custom Annotation should reside in a package '..annotation..'");

}
