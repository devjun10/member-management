package project.lshare.oauth2.test.acceptance;

import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.restdocs.payload.ResponseFieldsSnippet;
import project.lshare.membercoremodule.common.annotation.helper.Tag;

import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static project.lshare.oauth2.test.helper.snippet.response.ErrorResponseSnippetGenerator.ERROR_RESPONSE_SNIPPET;

@Tag(description = "RestDocs Snippet Generator")
public interface OAuth2SnippetGenerator {

    /**
     * {class-name}
     */
    String LOGIN_PAGE_CLASS = "oauth2-login-page-acceptancetest";
    String LOGIN_CLASS = "oauth2-login-acceptancetest";

    /**
     * {method-name}
     */
    String LOGIN_PAGE = "login-page";
    String LOGIN_METHOD = "login";

    /**
     * {command/query}
     */
    String COMMAND = "command";
    String QUERY = "query";

    /**
     * {success/failure}
     */
    String SUCCESS = "success";
    String FAILURE = "failure";

    /**
     * {failure-case}
     */
    String BAD_REQUEST = "bad-request";
    String API_CALLBACK_FAILURE = "api-callback";
    String TIMEOUT_FAILURE = "timeout";
    String FAILURE_FORBIDDEN_ACCESS = "forbidden";
    String FAILURE_UNAUTHORIZED = "unauthorized";
    String LOGOUT = "logout";
    String LOGOUT_ALL_DEVICES = "logout-all-devices";
    String SINGLE_DEVICE_LOG_OUT_CLASS = "oauth2-logout-single-device-acceptancetest";
    String ALL_DEVICE_LOG_OUT_CLASS = "oauth2-logout-all-devices-acceptancetest";

    /**
     * 로그인 API 응답 문서 snippet
     */
    ResponseFieldsSnippet LOGIN_PAGE_URL_RESPONSE_SNIPPET =
            responseFields(
                    fieldWithPath("loginPageUrl").type(JsonFieldType.STRING).description("로그인/회원가입 페이지 URL")
            );

    ResponseFieldsSnippet LOGIN_RRESPONSE_SNIPPET =
            responseFields(
                    fieldWithPath("accessToken").type(JsonFieldType.STRING).description("액세스 토큰")
            );


    /**
     * 로그인 API 응답 실패 문서 snippet
     */
    ResponseFieldsSnippet LOGOUT_FORBIDDEN_RESPONSE_SNIPPET = ERROR_RESPONSE_SNIPPET;
    ResponseFieldsSnippet LOGOUT_UNAUTHORIZED_RESPONSE_SNIPPET = ERROR_RESPONSE_SNIPPET;

}
