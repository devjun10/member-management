package project.lshare.oauth2.test.helper.fixture;

import project.lshare.membercoremodule.common.annotation.helper.Tag;

@Tag(description = "토큰 테스트 헬퍼 클래스")
public class TokenFixture {

    private TokenFixture() {
        throw new AssertionError("올바른 방법으로 생성자를 호출해주세요.");
    }

    /**
     * 반 영구 AccessToken
     */
    public static String getValidAccessToken() {
        return "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxIiwiYXV0aCI6MSwiZXhwIjoxNjc4NTM1MDUyfQ.Tgbleo5ir_IxYTRDTcjzP00ClHEs6d07mppiG6liyR11GLALFuri_-K7pz-6DdP9XXutwiKhVKKl0E63lEaYeQ";
    }

    public static String getValidParsedAccessToken() {
        return "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxIiwiYXV0aCI6MSwiZXhwIjoxNjc4NTM1MDUyfQ.Tgbleo5ir_IxYTRDTcjzP00ClHEs6d07mppiG6liyR11GLALFuri_-K7pz-6DdP9XXutwiKhVKKl0E63lEaYeQ";
    }

    public static String getInvalidAccessToken() {
        return "ThisIsInvalidAccessToken";
    }

    /**
     * 반 영구 RefreshToken
     */
    public static String getValidRefreshToken() {
        return "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxIiwiYXV0aCI6MSwiZXhwIjoxNjc4NTM1MDUyfQ.Tgbleo5ir_IxYTRDTcjzP00ClHEs6d07mppiG6liyR11GLALFuri_-K7pz-6DdP9XXutwiKhVKKl0E63lEaYeQ";
    }

    public static String getOtherValidRefreshToken() {
        return "eyJhbGciOiJIUzUxMiJ9.eyjiodfTJIfsdjfoasfjwo.fJIEFKDFSJDOIFJSOFJ-K7pz-6DdP9XXutwiKhVKKl0E63lEaYeQ";
    }

    public static String getInValidRefreshToken() {
        return "ThisIsInvalidRefreshToken";
    }

    public static String getExpiredToken() {
        return "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxIiwiYXV0aCI6MSwiZXhwIjoxNjc4NTM1MDUyfQ.Tgbleo5ir_IxYTRDTcjzP00ClHEs6d07mppiG6liyR11GLALFuri_-K7pz-6DdP9XXutwiKhVKKl0E63lEaYeQ";
    }

    /**
     * OAuth2 Code
     */
    public static String getValidOAuth2Code() {
        return "4/0AWtgzh4pg5gI-3d9YwquSfAWQvg3gmsPJyySR842HBbyWzCyf-WbTos-CvQ8OnZHyGI-pA";
    }

    public static String getInvalidOAuth2Code() {
        return "invalid-code";
    }

    /**
     * OAuth2 AccessToken
     */
    public static String getOAuth2AccessToken() {
        return "Bearer ya29.a0AVvZVsoKnRfLlkPIdHwuisrT8dACGUTt0XE8MliX-1iwP1VOkCDNYb0ihylp00y9dAfflwaKkRZDucMZA3Bm8RyN24pdcBCvnic7s2RcUdzzrlKA7tuJqCj_lSnifQu7qyHyqKPzpyjMMj48WYtJTarib3SwaCgYKAVsSARISFQGbdwaIwDZOYI9MtxKop1kvw3PSHQ0163";
    }
}
