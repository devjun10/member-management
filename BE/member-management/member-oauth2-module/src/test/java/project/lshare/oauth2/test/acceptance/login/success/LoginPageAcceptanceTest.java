package project.lshare.oauth2.test.acceptance.login.success;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.lshare.oauth2.configuration.annotation.LoginAcceptanceTest;
import project.lshare.oauth2.configuration.annotation.TestScenario;
import project.lshare.oauth2.test.acceptance.AcceptanceTestBase;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.LOGIN_PAGE;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.LOGIN_PAGE_CLASS;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.LOGIN_PAGE_URL_RESPONSE_SNIPPET;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.QUERY;
import static project.lshare.oauth2.test.acceptance.OAuth2SnippetGenerator.SUCCESS;

@TestScenario(
        target = "사용자가",
        situation = "로그인/회원가입을 하기 위해 URL을 요청한다.",
        expectedResult = "로그인/회원가입을 하는 URL 주소가 반환된다.")
@LoginAcceptanceTest
@DisplayName("로그인 페이지 요청 인수 테스트")
class LoginPageAcceptanceTest extends AcceptanceTestBase {

    @Test
    @DisplayName("로그인 페이지 URL을 요청하면 서버에 저장된 값을 반환한다.")
    void when_login_page_url_requested_then_redirecturl_should_be_returned() {
        given(this.specification)
                .filters(createDocument(LOGIN_PAGE_CLASS, LOGIN_PAGE, QUERY, SUCCESS, LOGIN_PAGE_URL_RESPONSE_SNIPPET))

                .when()
                .contentType(JSON)
                .get("/api/oauth2/login-page")

                .then()
                .statusCode(equalTo(200))
                .body(notNullValue())
                .log().all();
    }
}
