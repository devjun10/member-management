package project.lshare.oauth2.oauth.adapter.in.web.argumentresolver;

import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;
import project.lshare.membercoremodule.common.mapper.BusinessException;
import project.lshare.membercoremodule.domainmodel.authentication.valueobject.AuthenticatedMember;
import project.lshare.membercoremodule.domainmodel.member.Member;
import project.lshare.membercoremodule.persistencemodel.member.MemberPersistenceAdapter;
import project.lshare.oauth2.common.annotation.business.Authenticated;

import javax.servlet.http.HttpServletRequest;

import static project.lshare.membercoremodule.domainmodel.member.MemberTypeException.MEMBER_NOT_FOUND_EXCEPTION;

@Component
public class AuthenticationCheckArgumentResolver implements HandlerMethodArgumentResolver {

    private final MemberPersistenceAdapter memberPersistenceAdapter;

    public AuthenticationCheckArgumentResolver(MemberPersistenceAdapter memberPersistenceAdapter) {
        this.memberPersistenceAdapter = memberPersistenceAdapter;
    }

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.hasParameterAnnotation(Authenticated.class);
    }

    @Override
    public Object resolveArgument(
            MethodParameter parameter,
            ModelAndViewContainer mavContainer,
            NativeWebRequest webRequest,
            WebDataBinderFactory binderFactory
    ) {
        HttpServletRequest request = (HttpServletRequest) webRequest.getNativeRequest();
        Long memberId = getMemberId(request);
        Member member = memberPersistenceAdapter.findMemberById(memberId);
        return new AuthenticatedMember(member);
    }

    private Long getMemberId(HttpServletRequest request) {
        Object memberId = request.getAttribute("memberId");
        if (memberId == null) {
            throw BusinessException.of(MEMBER_NOT_FOUND_EXCEPTION);
        }
        return (Long) memberId;
    }
}
