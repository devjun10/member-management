package project.lshare.oauth2.oauth.adapter.in.web.client.utils;

import org.springframework.stereotype.Component;

import javax.servlet.ServletRequest;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

@Component
public class CookieUtils {

    public String getRefreshToken(ServletRequest servletRequest) {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        Cookie[] cookies = httpServletRequest.getCookies();

        if (cookies == null) {
            return null;
        }

        for (Cookie cookie : cookies) {
            if (cookie != null && cookie.getName().equals("RefreshToken")) {
                return cookie.getValue();
            }
        }
        return null;
    }

    public Cookie createCookie(String refreshToken) {
        Cookie cookie = new Cookie("RefreshToken", refreshToken);
        cookie.setMaxAge(60 * 60 * 24 * 14);
        cookie.setPath("/");
        cookie.setDomain("localhost"); // temp
        cookie.setHttpOnly(true);
        cookie.setSecure(true);
        return cookie;
    }

    public Cookie createExpireCookie() {
        Cookie cookie = new Cookie("RefreshToken", "");
        cookie.setMaxAge(0);
        cookie.setPath("/");
        cookie.setDomain("localhost"); // temp
        cookie.setHttpOnly(true);
        cookie.setSecure(true);
        return cookie;
    }
}
