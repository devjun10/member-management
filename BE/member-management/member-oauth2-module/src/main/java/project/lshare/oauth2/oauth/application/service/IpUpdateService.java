package project.lshare.oauth2.oauth.application.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import project.lshare.membercoremodule.domainmodel.member.Member;
import project.lshare.membercoremodule.domainmodel.member.valueobject.LastLoginIpAddress;
import project.lshare.membercoremodule.persistencemodel.member.MemberPersistenceAdapter;
import project.lshare.oauth2.oauth.application.port.in.IpUpdateCommand;

@Service
public class IpUpdateService implements IpUpdateCommand {

    private final MemberPersistenceAdapter memberPersistenceAdapter;

    public IpUpdateService(MemberPersistenceAdapter memberPersistenceAdapter) {
        this.memberPersistenceAdapter = memberPersistenceAdapter;
    }

    @Transactional
    public void updateLastLoginIpAddress(
            Long memberId,
            String ipAddress
    ) {
        Member member = memberPersistenceAdapter.findMemberById(memberId);
        member.updateLastLoginIpAddress(new LastLoginIpAddress(ipAddress));
        memberPersistenceAdapter.update(member);
    }
}
