package project.lshare.oauth2.oauth.application.port.out;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.concurrent.TimeUnit;

@Component
public class OAuth2RedisService implements OAuth2RedisCommand, OAuth2RedisQuery {

    private final StringRedisTemplate stringRedisTemplate;
    private final RedisTemplate<String, Object> redisTemplate;

    public OAuth2RedisService(
            StringRedisTemplate stringRedisTemplate,
            RedisTemplate<String, Object> redisTemplate
    ) {
        this.stringRedisTemplate = stringRedisTemplate;
        this.redisTemplate = redisTemplate;
    }

    @Override
    public void saveRefreshToken(
            Long memberId,
            String refreshToken
    ) {
        getSetOperations().add(getRefreshTokenKey(memberId), refreshToken.trim());
    }

    @Override
    public String getRefreshToken(Long memberId, String refreshToken) {
        Set<String> refreshTokens = getSetOperations().members(getRefreshTokenKey(memberId));
        if (exist(refreshTokens)) {
            return refreshTokens.stream()
                    .filter(token -> token.equals(refreshToken))
                    .findAny()
                    .orElseGet(() -> null);
        }
        return null;
    }

    private SetOperations<String, String> getSetOperations() {
        return stringRedisTemplate.opsForSet();
    }

    private boolean exist(Set<String> refreshTokens) {
        return refreshTokens != null && !refreshTokens.isEmpty();
    }

    @Override
    public void registerBlackList(String accessToken) {
        redisTemplate.opsForValue().set(accessToken, accessToken, 3, TimeUnit.DAYS);
    }

    @Override
    public boolean isBlackListToken(String accessToken) {
        Object blackListToken = redisTemplate.opsForValue().get(accessToken);
        return blackListToken != null;
    }

    @Override
    public void logout(Long memberId, String refreshToken) {
        getSetOperations().remove(getRefreshTokenKey(memberId), refreshToken);
    }

    @Override
    public void logoutAllDevices(Long memberId) {
        stringRedisTemplate.delete(getRefreshTokenKey(memberId));
    }

    /**
     * 레디스 키 이슈.
     * String.format("refreshToken-memberId-%s", memberId) 과
     * 같이 설정하면 레디스에서 토큰을 읽어오지 못함.
     */
    private String getRefreshTokenKey(Long memberId) {
        return memberId.toString();
    }
}
