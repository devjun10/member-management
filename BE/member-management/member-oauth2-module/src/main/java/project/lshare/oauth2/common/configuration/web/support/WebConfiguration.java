package project.lshare.oauth2.common.configuration.web.support;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import project.lshare.membercoremodule.persistencemodel.member.MemberPersistenceAdapter;
import project.lshare.oauth2.oauth.adapter.in.web.argumentresolver.AuthenticationCheckArgumentResolver;

import java.util.List;

@Configuration
public class WebConfiguration implements WebMvcConfigurer {

    private final MemberPersistenceAdapter memberPersistenceAdapter;

    public WebConfiguration(MemberPersistenceAdapter memberPersistenceAdapter) {
        this.memberPersistenceAdapter = memberPersistenceAdapter;
    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
        resolvers.add(new AuthenticationCheckArgumentResolver(memberPersistenceAdapter));
    }
}
