package project.lshare.oauth2.oauth.adapter.in.web.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import project.lshare.oauth2.common.exception.common.response.ErrorResponse;
import project.lshare.oauth2.oauth.adapter.in.web.client.utils.CookieUtils;
import project.lshare.oauth2.oauth.adapter.in.web.client.utils.IpAddressUtils;
import project.lshare.oauth2.oauth.adapter.in.web.jwt.JwtTokenProvider;
import project.lshare.oauth2.oauth.adapter.in.web.response.LoginResponse;
import project.lshare.oauth2.oauth.application.port.out.OAuth2RedisService;
import project.lshare.oauth2.oauth.application.service.IpUpdateService;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;

public class OAuth2LoginTokenValidationFilter implements Filter {

    private final IpUpdateService ipUpdateService;
    private final OAuth2RedisService redisService;
    private final ObjectMapper objectMapper;
    private final JwtTokenProvider tokenProvider;
    private final IpAddressUtils ipAddressUtils;
    private final CookieUtils cookieUtils;

    public OAuth2LoginTokenValidationFilter(
            IpUpdateService ipUpdateService,
            OAuth2RedisService redisService,
            ObjectMapper objectMapper,
            JwtTokenProvider tokenProvider,
            IpAddressUtils ipAddressUtils,
            CookieUtils cookieUtils
    ) {
        this.ipUpdateService = ipUpdateService;
        this.redisService = redisService;
        this.objectMapper = objectMapper;
        this.tokenProvider = tokenProvider;
        this.ipAddressUtils = ipAddressUtils;
        this.cookieUtils = cookieUtils;
    }

    @Override
    public void doFilter(
            ServletRequest request,
            ServletResponse response,
            FilterChain chain
    ) throws IOException, ServletException {
        String accessToken = tokenProvider.getAccessToken((HttpServletRequest) request);
        if (isValid(accessToken)) {
            if (isBlackListToken(accessToken)) {
                forbiddenAccess((HttpServletResponse) response);
                return;
            }
            String refreshToken = cookieUtils.getRefreshToken(request);
            if (!isValid(refreshToken)) {
                forbiddenAccess((HttpServletResponse) response);
                return;
            }
            updateLastLoginIpAddress((HttpServletRequest) request, accessToken);
            redirectSilently(accessToken, response);
            return;
        }

        String refreshToken = cookieUtils.getRefreshToken(request);
        if (isValid(refreshToken)) {
            Long memberId = tokenProvider.getMemberId(refreshToken);
            String redisRefreshToken = redisService.getRefreshToken(memberId, refreshToken);
            if (!refreshToken.equals(redisRefreshToken)) {
                forbiddenAccess((HttpServletResponse) response);
                return;
            }
            updateLastLoginIpAddress((HttpServletRequest) request, refreshToken);
            redirectSilently(refreshToken, response);
            return;
        }
        chain.doFilter(request, response);
    }

    private boolean isValid(String accessToken) {
        return tokenProvider.validate(accessToken);
    }

    private boolean isBlackListToken(String accessToken) {
        return redisService.isBlackListToken(accessToken);
    }

    private void updateLastLoginIpAddress(
            HttpServletRequest request,
            String token
    ) {
        Long memberId = tokenProvider.getMemberId(token);
        String ipAddress = ipAddressUtils.getClientIpAddress(request);
        ipUpdateService.updateLastLoginIpAddress(memberId, ipAddress);
    }

    private void forbiddenAccess(HttpServletResponse response) throws IOException {
        ErrorResponse errorResponse = ErrorResponse.of(FORBIDDEN, "Access forbidden because of invalid-token.");
        response.setContentType(APPLICATION_JSON_VALUE);
        response.setStatus(FORBIDDEN.value());
        response.addCookie(cookieUtils.createExpireCookie());
        response.getWriter().write(objectMapper.writeValueAsString(errorResponse));
    }

    private void redirectSilently(
            String token,
            ServletResponse response
    ) throws IOException {
        Long memberId = tokenProvider.getMemberId(token);
        String accessToken = tokenProvider.createAccessToken(memberId);
        String refreshToken = tokenProvider.createRefreshToken(memberId);

        HttpServletResponse httpServletResponse = (HttpServletResponse) response;
        ((HttpServletResponse) response).setStatus(302);
        Cookie cookie = cookieUtils.createCookie(refreshToken);
        ((HttpServletResponse) response).addCookie(cookie);
        httpServletResponse.getWriter().write(objectMapper.writeValueAsString(new LoginResponse(accessToken)));
    }
}
