package project.lshare.oauth2.common.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import project.lshare.membercoremodule.common.mapper.BusinessException;
import project.lshare.oauth2.common.exception.common.response.ErrorResponse;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@RestControllerAdvice
public class ExceptionHandler {

    @org.springframework.web.bind.annotation.ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity<ErrorResponse> catBadRequest(MissingServletRequestParameterException exception) {
        ErrorResponse response = ErrorResponse.of(BAD_REQUEST, exception.getMessage());
        return ResponseEntity.status(response.getStatus())
                .body(response);
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<ErrorResponse> catBadRequest(IllegalArgumentException exception) {
        ErrorResponse response = ErrorResponse.of(BAD_REQUEST, exception.getMessage());
        return ResponseEntity.status(response.getStatus())
                .body(response);
    }

    @org.springframework.web.bind.annotation.ExceptionHandler(BusinessException.class)
    public ResponseEntity<ErrorResponse> catBusinessException(BusinessException exception) {
        ErrorResponse response = ErrorResponse.of(exception);
        return ResponseEntity.status(response.getStatus())
                .body(response);
    }
}
