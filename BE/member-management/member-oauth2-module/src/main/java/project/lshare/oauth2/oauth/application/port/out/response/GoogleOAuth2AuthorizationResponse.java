package project.lshare.oauth2.oauth.application.port.out.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class GoogleOAuth2AuthorizationResponse {

    @JsonProperty(value = "access_token")
    private String accessToken;

    @JsonProperty(value = "expires_in")
    private long expiresIn;

    private String scope;

    @JsonProperty(value = "token_type")
    private String tokenType;

    @JsonProperty(value = "id_token")
    private String idToken;

    private GoogleOAuth2AuthorizationResponse() {
    }

    public GoogleOAuth2AuthorizationResponse(
            String accessToken,
            long expiresIn,
            String scope,
            String tokenType,
            String idToken
    ) {
        this.accessToken = accessToken;
        this.expiresIn = expiresIn;
        this.scope = scope;
        this.tokenType = tokenType;
        this.idToken = idToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public long getExpiresIn() {
        return expiresIn;
    }

    public String getScope() {
        return scope;
    }

    public String getTokenType() {
        return tokenType;
    }

    public String getIdToken() {
        return idToken;
    }

    @Override
    public String toString() {
        return String.format("AccessToken: %s, Expired: %s, Scope: %s, Token Type: %s", accessToken, expiresIn, scope, tokenType);
    }
}
