package project.lshare.oauth2.common.configuration.web.support;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import project.lshare.oauth2.oauth.application.service.IpUpdateService;
import project.lshare.oauth2.oauth.application.port.out.OAuth2RedisService;
import project.lshare.oauth2.oauth.adapter.in.web.client.utils.CookieUtils;
import project.lshare.oauth2.oauth.adapter.in.web.client.utils.IpAddressUtils;
import project.lshare.oauth2.oauth.adapter.in.web.jwt.JwtTokenProvider;
import project.lshare.oauth2.oauth.adapter.in.web.filter.OAuth2LoginTokenValidationFilter;
import project.lshare.oauth2.oauth.adapter.in.web.filter.TokenValidationFilter;

@Configuration
public class ServletConfiguration {

    private final IpUpdateService ipUpdateService;
    private final OAuth2RedisService redisService;
    private final ObjectMapper objectMapper;
    private final JwtTokenProvider tokenProvider;
    private final IpAddressUtils ipAddressUtils;
    private final CookieUtils cookieUtils;

    public ServletConfiguration(
            IpUpdateService ipUpdateService,
            OAuth2RedisService redisService,
            ObjectMapper objectMapper,
            JwtTokenProvider tokenProvider,
            IpAddressUtils ipAddressUtils,
            CookieUtils cookieUtils
    ) {
        this.ipUpdateService = ipUpdateService;
        this.redisService = redisService;
        this.objectMapper = objectMapper;
        this.tokenProvider = tokenProvider;
        this.ipAddressUtils = ipAddressUtils;
        this.cookieUtils = cookieUtils;
    }

    @Bean
    public FilterRegistrationBean<OAuth2LoginTokenValidationFilter> oauth2LoginTokenValidationFilterFilterRegistrationBean() {
        FilterRegistrationBean<OAuth2LoginTokenValidationFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(getOAuth2LoginTokenValidationFilter());
        registrationBean.addUrlPatterns("/api/oauth2/login/*");
        return registrationBean;
    }

    @Bean
    public FilterRegistrationBean<TokenValidationFilter> tokenValidationFilterFilterRegistrationBean() {
        FilterRegistrationBean<TokenValidationFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(getTokenValidationFilter());
        registrationBean.addUrlPatterns("/api/logout/*");
        return registrationBean;
    }

    private OAuth2LoginTokenValidationFilter getOAuth2LoginTokenValidationFilter() {
        return new OAuth2LoginTokenValidationFilter(ipUpdateService, redisService, objectMapper, tokenProvider, ipAddressUtils, cookieUtils);
    }

    private TokenValidationFilter getTokenValidationFilter() {
        return new TokenValidationFilter(redisService, objectMapper, tokenProvider, cookieUtils);
    }
}
