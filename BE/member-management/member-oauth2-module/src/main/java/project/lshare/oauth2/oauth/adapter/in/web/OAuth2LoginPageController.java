package project.lshare.oauth2.oauth.adapter.in.web;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import project.lshare.oauth2.common.annotation.business.PresentationLayer;
import project.lshare.oauth2.oauth.adapter.in.web.response.LoginPageUrlResponse;
import project.lshare.oauth2.oauth.application.port.in.LoginPageQuery;

@PresentationLayer
@RequestMapping("/api/oauth2")
public class OAuth2LoginPageController {

    private final LoginPageQuery loginPageQuery;

    public OAuth2LoginPageController(LoginPageQuery loginPageQuery) {
        this.loginPageQuery = loginPageQuery;
    }

    @GetMapping(path = "/login-page")
    public ResponseEntity<LoginPageUrlResponse> getLoginPageUrl() {
        LoginPageUrlResponse response = LoginPageUrlResponse.of(loginPageQuery.getPage());
        return ResponseEntity.ok(response);
    }
}
