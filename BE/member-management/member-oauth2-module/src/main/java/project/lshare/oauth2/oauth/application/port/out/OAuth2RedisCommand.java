package project.lshare.oauth2.oauth.application.port.out;

public interface OAuth2RedisCommand {

    void saveRefreshToken(Long memberId, String refreshToken);

    void registerBlackList(String accessToken);

    void logout(Long memberId, String refreshToken);

    void logoutAllDevices(Long memberId);

}
