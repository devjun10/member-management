package project.lshare.oauth2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import project.lshare.membercoremodule.MemberCoreModuleApplication;

@SpringBootApplication
@Import(MemberCoreModuleApplication.class)
public class MemberOauth2ModuleApplication {

    public static void main(String[] args) {
        SpringApplication.run(MemberOauth2ModuleApplication.class, args);
    }

}
