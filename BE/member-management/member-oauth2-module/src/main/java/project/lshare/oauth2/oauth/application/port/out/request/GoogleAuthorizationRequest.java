package project.lshare.oauth2.oauth.application.port.out.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.security.oauth2.client.registration.ClientRegistration;

public class GoogleAuthorizationRequest {

    @JsonProperty("code")
    private final String code;

    @JsonProperty("client_id")
    private final String clientId;

    @JsonProperty("client_secret")
    private final String clientSecret;

    @JsonProperty("redirect_uri")
    private final String redirectUri;

    @JsonProperty("grant_type")
    private final String grantType;

    public GoogleAuthorizationRequest(
            String code,
            ClientRegistration clientRegistration
    ) {
        this.code = code;
        this.clientId = clientRegistration.getClientId();
        this.clientSecret = clientRegistration.getClientSecret();
        this.redirectUri = clientRegistration.getRedirectUri();
        this.grantType = clientRegistration.getAuthorizationGrantType().getValue();
    }

    public String getCode() {
        return code;
    }

    public String getClientId() {
        return clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public String getRedirectUri() {
        return redirectUri;
    }

    public String getGrantType() {
        return grantType;
    }

    /**
     * ClientId, Cient-Secret은 노출해서 안되기 때문에
     * Code 정도만 올바르게 들어오는지 검증
     * */
    @Override
    public String toString() {
        return String.format("Code: %s", code);
    }
}
