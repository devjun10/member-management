package project.lshare.oauth2.oauth.application.port.in;

import project.lshare.membercoremodule.domainmodel.authentication.valueobject.AuthenticatedMember;
import project.lshare.oauth2.common.annotation.business.ApplicationLayer;

@ApplicationLayer
public interface OAuth2LogoutCommand {

    void logout(AuthenticatedMember authenticatedMember, String refreshToken);

    void logoutAllDevices(AuthenticatedMember authenticatedMember);

}
