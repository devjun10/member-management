package project.lshare.oauth2.oauth.application.port.in;

import project.lshare.oauth2.common.annotation.business.ApplicationLayer;
import project.lshare.oauth2.oauth.application.service.LShareServiceToken;

@ApplicationLayer
public interface OAuth2LoginCommand {

    LShareServiceToken login(String code);

}
