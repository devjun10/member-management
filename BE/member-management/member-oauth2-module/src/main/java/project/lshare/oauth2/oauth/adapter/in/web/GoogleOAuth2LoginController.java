package project.lshare.oauth2.oauth.adapter.in.web;

import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import project.lshare.oauth2.common.annotation.business.PresentationLayer;
import project.lshare.oauth2.oauth.adapter.in.web.response.LoginResponse;
import project.lshare.oauth2.oauth.application.port.in.OAuth2LoginCommand;
import project.lshare.oauth2.oauth.application.service.LShareServiceToken;

import static org.springframework.http.HttpHeaders.SET_COOKIE;
import static org.springframework.http.HttpStatus.OK;

@PresentationLayer
@RequestMapping("/api/oauth2/login")
public class GoogleOAuth2LoginController {

    private final OAuth2LoginCommand auth2LoginCommand;

    public GoogleOAuth2LoginController(OAuth2LoginCommand auth2LoginCommand) {
        this.auth2LoginCommand = auth2LoginCommand;
    }

    @GetMapping
    public ResponseEntity<LoginResponse> login(@RequestParam("code") String code) {
        LShareServiceToken token = auth2LoginCommand.login(code);
        String cookie = extracted(token.getRefreshToken()).toString();
        return ResponseEntity.status(OK)
                .header(SET_COOKIE, cookie)
                .body(new LoginResponse(token.getAccessToken()));
    }

    private ResponseCookie extracted(String refreshToken) {
        return ResponseCookie.from("RefreshToken", refreshToken)
                .sameSite("Lax")
                .secure(true)
                .httpOnly(true)
//                .domain("/")
//                .path("/")
                .build();
    }
}
