package project.lshare.oauth2.oauth.application.port.out.response;

public class GoogleMemberProfileResponse {

    private String id;
    private String email;
    private boolean verified_email;
    private String picture;

    private GoogleMemberProfileResponse() {
    }

    public GoogleMemberProfileResponse(
            String id,
            boolean verified_email,
            String email,
            String picture
    ) {
        this.id = id;
        this.email = email;
        this.verified_email = verified_email;
        this.picture = picture;
    }

    public String getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public boolean getVerified_email() {
        return verified_email;
    }

    public String getPicture() {
        return picture;
    }

    @Override
    public String toString() {
        return String.format("OAuthId: %s, name: %s, email:%s", id, verified_email, email);
    }
}
