package project.lshare.oauth2.oauth.application.port.out;

import org.springframework.security.oauth2.client.registration.ClientRegistration;

public interface OAuth2Client<T> {

    T callBack(String code, ClientRegistration clientRegistration);

}
