package project.lshare.oauth2.oauth.application.service;

import org.springframework.stereotype.Service;
import project.lshare.oauth2.oauth.adapter.in.web.jwt.JwtTokenProvider;
import project.lshare.membercoremodule.domainmodel.authentication.valueobject.AuthenticatedMember;
import project.lshare.oauth2.oauth.application.port.in.OAuth2LogoutCommand;
import project.lshare.oauth2.oauth.application.port.out.OAuth2RedisService;

@Service
public class GoogleOAuth2LogoutService implements OAuth2LogoutCommand {

    private final OAuth2RedisService redisService;
    private final JwtTokenProvider tokenProvider;

    public GoogleOAuth2LogoutService(
            OAuth2RedisService redisService,
            JwtTokenProvider tokenProvider
    ) {
        this.redisService = redisService;
        this.tokenProvider = tokenProvider;
    }

    public void logout(
            AuthenticatedMember authenticatedMember,
            String refreshToken
    ) {
        redisService.logout(authenticatedMember.getMemberId(), refreshToken);
    }

    public void logoutAllDevices(AuthenticatedMember authenticatedMember) {
        redisService.logoutAllDevices(authenticatedMember.getMemberId());
    }
}
