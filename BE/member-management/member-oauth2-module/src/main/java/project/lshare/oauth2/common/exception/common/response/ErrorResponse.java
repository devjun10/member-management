package project.lshare.oauth2.common.exception.common.response;

import org.springframework.http.HttpStatus;
import project.lshare.membercoremodule.common.mapper.BusinessException;

import java.time.LocalDateTime;

public class ErrorResponse {

    private final LocalDateTime time;
    private final int code;
    private final String message;
    private final HttpStatus status;

    private ErrorResponse(BusinessException businessException) {
        this.time = LocalDateTime.now();
        this.code = businessException.getCode();
        this.message = businessException.getMessage();
        this.status = businessException.getHttpStatus();
    }

    private ErrorResponse(
            HttpStatus status,
            String message
    ) {
        this.time = LocalDateTime.now();
        this.code = status.value();
        this.message = message;
        this.status = status;
    }

    public static ErrorResponse of(BusinessException businessException) {
        return new ErrorResponse(businessException);
    }

    public static ErrorResponse of(
            HttpStatus status,
            String message
    ) {
        return new ErrorResponse(status, message);
    }

    public LocalDateTime getTime() {
        return time;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public HttpStatus getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return String.format("Time: %s, Code: %s, Message: %s, Status: %s", time, code, message, status);
    }
}
