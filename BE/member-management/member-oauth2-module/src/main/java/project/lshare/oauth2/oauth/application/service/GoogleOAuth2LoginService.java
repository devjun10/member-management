package project.lshare.oauth2.oauth.application.service;

import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import project.lshare.membercoremodule.persistencemodel.authentication.AuthenticationJpaEntity;
import project.lshare.membercoremodule.persistencemodel.authentication.SpringDataAuthenticationRepository;
import project.lshare.membercoremodule.persistencemodel.member.MemberJpaEntity;
import project.lshare.membercoremodule.persistencemodel.member.SpringDataMemberRepository;
import project.lshare.oauth2.oauth.adapter.in.web.jwt.JwtTokenProvider;
import project.lshare.oauth2.oauth.application.port.in.OAuth2LoginCommand;
import project.lshare.oauth2.oauth.application.port.out.OAuth2Client;
import project.lshare.oauth2.oauth.application.port.out.OAuth2RedisService;
import project.lshare.oauth2.oauth.application.port.out.response.GoogleMemberProfileResponse;

import java.util.Optional;

@Service
public class GoogleOAuth2LoginService implements OAuth2LoginCommand {

    private final OAuth2Client<GoogleMemberProfileResponse> oauth2Client;
    private final SpringDataMemberRepository memberRepository;
    private final SpringDataAuthenticationRepository authenticationRepository;
    private final JwtTokenProvider tokenProvider;
    private final ClientRegistrationRepository clientRegistrationRepository;
    private final OAuth2RedisService redisService;

    public GoogleOAuth2LoginService(
            OAuth2Client oauth2Client,
            SpringDataMemberRepository memberRepository,
            SpringDataAuthenticationRepository authenticationRepository,
            JwtTokenProvider tokenProvider,
            ClientRegistrationRepository clientRegistrationRepository,
            OAuth2RedisService redisService
    ) {
        this.oauth2Client = oauth2Client;
        this.memberRepository = memberRepository;
        this.authenticationRepository = authenticationRepository;
        this.tokenProvider = tokenProvider;
        this.clientRegistrationRepository = clientRegistrationRepository;
        this.redisService = redisService;
    }

    @Override
    @Transactional
    public LShareServiceToken login(String code) {
        ClientRegistration clientRegistration = clientRegistrationRepository.findByRegistrationId("google");
        GoogleMemberProfileResponse response = oauth2Client.callBack(code, clientRegistration);
        MemberJpaEntity memberJpaEntity = upsertMember(response);
        return createToken(memberJpaEntity.getMemberId());
    }

    private MemberJpaEntity upsertMember(GoogleMemberProfileResponse response) {
        Optional<AuthenticationJpaEntity> authenticationJpaEntity = authenticationRepository.findAuthenticationJpaEntitiesByEmail(response.getEmail());
        if (authenticationJpaEntity.isPresent()) {
            AuthenticationJpaEntity authentication = authenticationJpaEntity.get();
            authentication.updateProfile(response.getPicture());
            return authentication.getMember();
        }
        MemberJpaEntity memberJpaEntity = new MemberJpaEntity(response.getEmail(), response.getPicture());
        memberRepository.save(memberJpaEntity);
        AuthenticationJpaEntity authenticationJpaEntity1 = new AuthenticationJpaEntity(memberJpaEntity, response.getEmail(), response.getVerified_email(), response.getPicture());
        authenticationRepository.save(authenticationJpaEntity1);
        return memberJpaEntity;
    }

    private void saveRefreshToken(
            Long memberId,
            String refreshToken
    ) {
        redisService.saveRefreshToken(memberId, refreshToken);
    }

    private LShareServiceToken createToken(Long memberId) {
        String accessToken = tokenProvider.createAccessToken(memberId);
        String refreshToken = tokenProvider.createRefreshToken(memberId);
        saveRefreshToken(memberId, refreshToken);
        return new LShareServiceToken(accessToken, refreshToken);
    }
}
