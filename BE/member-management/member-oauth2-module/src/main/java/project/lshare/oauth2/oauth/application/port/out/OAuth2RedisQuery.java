package project.lshare.oauth2.oauth.application.port.out;

public interface OAuth2RedisQuery {

    String getRefreshToken(Long memberId, String refreshToken);

    boolean isBlackListToken(String accessToken);

}
