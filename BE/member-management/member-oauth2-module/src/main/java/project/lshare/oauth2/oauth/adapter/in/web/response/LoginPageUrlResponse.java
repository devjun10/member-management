package project.lshare.oauth2.oauth.adapter.in.web.response;

public class LoginPageUrlResponse {

    private final String loginPageUrl;

    public LoginPageUrlResponse(String loginPageUrl) {
        this.loginPageUrl = loginPageUrl;
    }

    public static LoginPageUrlResponse of(String loginPageUrl) {
        return new LoginPageUrlResponse(loginPageUrl);
    }

    public String getLoginPageUrl() {
        return loginPageUrl;
    }
}
