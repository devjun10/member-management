package project.lshare.oauth2.oauth.adapter.in.web;

import org.springframework.http.ResponseCookie;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import project.lshare.membercoremodule.domainmodel.authentication.valueobject.AuthenticatedMember;
import project.lshare.oauth2.common.annotation.business.Authenticated;
import project.lshare.oauth2.common.annotation.business.PresentationLayer;
import project.lshare.oauth2.oauth.adapter.in.web.client.utils.CookieUtils;
import project.lshare.oauth2.oauth.application.port.in.OAuth2LogoutCommand;

import javax.servlet.http.HttpServletRequest;

import static org.springframework.http.HttpHeaders.SET_COOKIE;

@PresentationLayer
@RequestMapping("/api/logout")
public class GoogleLogoutController {

    private final OAuth2LogoutCommand auth2LogoutCommand;
    private final CookieUtils cookieUtils;

    public GoogleLogoutController(
            OAuth2LogoutCommand auth2LogoutCommand,
            CookieUtils cookieUtils
    ) {
        this.auth2LogoutCommand = auth2LogoutCommand;
        this.cookieUtils = cookieUtils;
    }

    @PostMapping(path = "/single-device")
    public ResponseEntity<Void> logout(
            @Authenticated AuthenticatedMember authenticatedMember,
            HttpServletRequest httpServletRequest
    ) {
        String refreshToken = cookieUtils.getRefreshToken(httpServletRequest);
        auth2LogoutCommand.logout(authenticatedMember, refreshToken);
        return ResponseEntity.ok()
                .header(SET_COOKIE, createExpiredRefreshTokenCooki())
                .build();
    }

    @PostMapping(path = "/all-devices")
    public ResponseEntity<Void> logoutAllDevices(@Authenticated AuthenticatedMember authenticatedMember) {
        auth2LogoutCommand.logoutAllDevices(authenticatedMember);
        return ResponseEntity.ok()
                .header(SET_COOKIE, createExpiredRefreshTokenCooki())
                .build();
    }

    private String createExpiredRefreshTokenCooki() {
        return ResponseCookie.from("RefreshToken", "")
                .maxAge(0)
                .httpOnly(true)
                .secure(true)
                .build()
                .toString();
    }
}
