package project.lshare.oauth2.oauth.application.port.in;

import project.lshare.oauth2.common.annotation.business.ApplicationLayer;

@ApplicationLayer
public interface LoginPageQuery {

    String getPage();
}
