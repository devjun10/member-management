package project.lshare.oauth2.oauth.adapter.in.web.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import project.lshare.oauth2.common.exception.common.response.ErrorResponse;
import project.lshare.oauth2.oauth.adapter.in.web.client.utils.CookieUtils;
import project.lshare.oauth2.oauth.adapter.in.web.jwt.JwtTokenProvider;
import project.lshare.oauth2.oauth.application.port.out.OAuth2RedisService;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;
import static org.springframework.util.MimeTypeUtils.APPLICATION_JSON_VALUE;

public class TokenValidationFilter implements Filter {

    private final OAuth2RedisService redisService;
    private final ObjectMapper objectMapper;
    private final JwtTokenProvider tokenProvider;
    private final CookieUtils cookieUtils;

    public TokenValidationFilter(
            OAuth2RedisService redisService,
            ObjectMapper objectMapper,
            JwtTokenProvider tokenProvider,
            CookieUtils cookieUtils
    ) {
        this.redisService = redisService;
        this.objectMapper = objectMapper;
        this.tokenProvider = tokenProvider;
        this.cookieUtils = cookieUtils;
    }

    @Override
    public void doFilter(
            ServletRequest request,
            ServletResponse response,
            FilterChain chain
    ) throws IOException, ServletException {
        String accessToken = tokenProvider.getAccessToken((HttpServletRequest) request);
        if (isValid(accessToken)) {
            if (isBlackListToken(accessToken)) {
                forbiddenAccess((HttpServletResponse) response);
                return;
            }
            setMemberId(request, accessToken);
            chain.doFilter(request, response);
            return;
        }

        String refreshToken = cookieUtils.getRefreshToken(request);
        if (isValid(refreshToken)) {
            Long memberId = tokenProvider.getMemberId(refreshToken);
            String redisRefreshToken = redisService.getRefreshToken(memberId, refreshToken);
            if (!refreshToken.equals(redisRefreshToken)) {
                forbiddenAccess((HttpServletResponse) response);
                return;
            }
            setMemberId(request, refreshToken);
            chain.doFilter(request, response);
            return;
        }
        accessDenied((HttpServletResponse) response);
    }

    private void setMemberId(ServletRequest request, String token) {
        Long memberId = tokenProvider.getMemberId(token);
        request.setAttribute("memberId", memberId);
    }

    private boolean isValid(String accessToken) {
        return tokenProvider.validate(accessToken);
    }

    private boolean isBlackListToken(String accessToken) {
        return redisService.isBlackListToken(accessToken);
    }

    private void forbiddenAccess(HttpServletResponse response) throws IOException {
        ErrorResponse errorResponse = ErrorResponse.of(FORBIDDEN, "Access forbidden because of invalid-token.");
        response.setContentType(APPLICATION_JSON_VALUE);
        response.setStatus(FORBIDDEN.value());
        response.addCookie(cookieUtils.createExpireCookie());
        response.getWriter().write(objectMapper.writeValueAsString(errorResponse));
    }

    private void accessDenied(HttpServletResponse response) throws IOException {
        ErrorResponse errorResponse = ErrorResponse.of(UNAUTHORIZED, "Access denied because of unauthorized.");
        response.setContentType(APPLICATION_JSON_VALUE);
        response.setStatus(UNAUTHORIZED.value());
        response.addCookie(cookieUtils.createExpireCookie());
        response.getWriter().write(objectMapper.writeValueAsString(errorResponse));
    }
}
