package project.lshare.oauth2.oauth.adapter.in.web.jwt;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.UnsupportedJwtException;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.security.Key;
import java.util.Date;
import java.util.regex.PatternSyntaxException;

@Slf4j
@Order(1)
@Component
public class JwtTokenProvider {

    private final String secretKey;
    private final long accessTokenTimeValidTime;
    private final long refreshTokenValidTime;
    protected final Key key;

    public JwtTokenProvider(
            @Value("${jwt.secret-key}") String secretKey,
            @Value("${jwt.access-token-valid-time}") long accessTokenTimeValidTime,
            @Value("${jwt.refresh-token-valid-time}") long refreshTokenValidTime
    ) {
        this.secretKey = secretKey;
        this.accessTokenTimeValidTime = accessTokenTimeValidTime;
        this.refreshTokenValidTime = refreshTokenValidTime;
        byte[] keyBytes = Decoders.BASE64.decode(secretKey);
        this.key = Keys.hmacShaKeyFor(keyBytes);
    }

    public String createAccessToken(Long memberId) {
        long now = (new Date()).getTime();
        return Jwts.builder()
                .signWith(key, SignatureAlgorithm.HS512)
                .setSubject(memberId.toString())
                .setIssuer("lshare")
                .setIssuedAt(new Date())
                .setExpiration(new Date(now + this.accessTokenTimeValidTime))
                .compact();
    }

    public String createRefreshToken(Long memberId) {
        long now = (new Date()).getTime();
        return Jwts.builder()
                .signWith(key, SignatureAlgorithm.HS512)
                .setSubject(memberId.toString())
                .setIssuer("lshare")
                .setIssuedAt(new Date())
                .setExpiration(new Date(now + this.refreshTokenValidTime))
                .compact();
    }

    public Long getMemberId(String token) {
        String memberId = Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody()
                .getSubject();
        return Long.parseLong(memberId);
    }

    // https://stackoverflow.com/questions/72359540/valid-jwt-token-with-wrong-base64-encoding
    // https://stackoverflow.com/questions/65782480/illegal-base64url-character-when-getting-claims-decode-from-token-java-jwt
    public String getAccessToken(HttpServletRequest request) {
        String bearerToken = request.getHeader("Authorization");
        try {
            if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
                return bearerToken.split(" ")[1].trim();
            }
        } catch (PatternSyntaxException e) {
            log.error("올바르지 않은 구조의 토큰입니다.");
            return null;
        }
        return null;
    }

    public boolean validate(String token) {
        if (token == null) {
            return false;
        }
        try {
            Jwts.parserBuilder()
                    .setSigningKey(key)
                    .build()
                    .parseClaimsJws(token)
                    .getBody();
            return true;
        } catch (io.jsonwebtoken.security.SecurityException | MalformedJwtException e) {
            log.error("서명이 잘못되었습니다.");
        } catch (ExpiredJwtException e) {
            log.error("토큰이 만료되었습니다.");
        } catch (UnsupportedJwtException e) {
            log.error("지원하지 않는 토큰입니다.");
        } catch (IllegalArgumentException e) {
            log.error("잘못된 형식의 JWT 토큰입니다.");
        } catch (Exception e) {
            log.error("올바르지 않은 토큰입니다.");
        }
        return false;
    }

    public long getRemainingTime(String accessToken) {
        if (validate(accessToken)) {
            Date validTokenDate = Jwts.parserBuilder().setSigningKey(key)
                    .build().parseClaimsJws(accessToken)
                    .getBody().getExpiration();
            long currentTime = new Date().getTime();
            return validTokenDate.getTime() - currentTime;
        }
        return 0;
    }
}
