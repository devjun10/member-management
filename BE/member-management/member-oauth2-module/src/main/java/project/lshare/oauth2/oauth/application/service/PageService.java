package project.lshare.oauth2.oauth.application.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import project.lshare.oauth2.oauth.application.port.in.LoginPageQuery;

@Service
public class PageService implements LoginPageQuery {

    private final PageUrlComponent pageUrlComponent;

    public PageService(PageUrlComponent pageUrlComponent) {
        this.pageUrlComponent = pageUrlComponent;
    }

    @Transactional(readOnly = true)
    public String getPage() {
        return pageUrlComponent.getPage();
    }
}
