package project.lshare.oauth2.oauth.application.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PageUrlComponent {

    @Value("${oauth2.login-page}")
    public String page;

    public String getPage() {
        return page;
    }
}
