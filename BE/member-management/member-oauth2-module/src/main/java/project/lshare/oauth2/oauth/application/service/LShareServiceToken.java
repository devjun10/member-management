package project.lshare.oauth2.oauth.application.service;

import project.lshare.membercoremodule.common.error.ErrorField;

import java.util.Objects;

public class LShareServiceToken {

    private final String accessToken;
    private final String refreshToken;

    public LShareServiceToken(
            String accessToken,
            String refreshToken
    ) {
        validate(accessToken, refreshToken);
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
    }

    private void validate(String accessToken, String refreshToken) {
        if (accessToken == null) {
            throw new IllegalArgumentException("AccessToken 값이 존재하지 않습니다.", ErrorField.of("AccessToken", accessToken));
        }
        if (refreshToken == null) {
            throw new IllegalArgumentException("RefreshToken 값이 존재하지 않습니다.", ErrorField.of("RefreshToken", refreshToken));
        }
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LShareServiceToken that)) return false;
        return getAccessToken().equals(that.getAccessToken()) && getRefreshToken().equals(that.getRefreshToken());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAccessToken(), getRefreshToken());
    }

    @Override
    public String toString() {
        return String.format("AccessToken: %s, RefreshToken: %s", accessToken, refreshToken);
    }
}
