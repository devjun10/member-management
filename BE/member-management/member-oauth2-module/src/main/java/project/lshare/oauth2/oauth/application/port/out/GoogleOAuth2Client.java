package project.lshare.oauth2.oauth.application.port.out;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import project.lshare.membercoremodule.common.mapper.BusinessException;
import project.lshare.oauth2.oauth.application.port.out.request.GoogleAuthorizationRequest;
import project.lshare.oauth2.oauth.application.port.out.response.GoogleMemberProfileResponse;
import project.lshare.oauth2.oauth.application.port.out.response.GoogleOAuth2AuthorizationResponse;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static project.lshare.membercoremodule.common.error.CommonException.API_CALLBACK_EXCEPTION;
import static project.lshare.membercoremodule.common.error.CommonException.TIMEOUT_EXCEPTION;

@Component
public class GoogleOAuth2Client implements OAuth2Client<GoogleMemberProfileResponse> {

    @Value("${oauth2.userprofile}")
    private String baseUrl;

    private final WebClient webClient;

    public GoogleOAuth2Client(WebClient webClient) {
        this.webClient = webClient;
    }

    @Override
    public GoogleMemberProfileResponse callBack(
            String code,
            ClientRegistration clientRegistration
    ) {
        GoogleOAuth2AuthorizationResponse authorization = getAuthorization(code, clientRegistration);
        validateAuthorization(authorization);
        return getProfile(authorization);
    }

    private void validateAuthorization(GoogleOAuth2AuthorizationResponse authorization) {
        if (authorization == null || authorization.getAccessToken() == null) {
            throw BusinessException.of(API_CALLBACK_EXCEPTION);
        }
    }

    private GoogleOAuth2AuthorizationResponse getAuthorization(
            String code,
            ClientRegistration clientRegistration
    ) {
        try {
            return webClient.post()
                    .accept(APPLICATION_JSON)
                    .bodyValue(new GoogleAuthorizationRequest(code, clientRegistration))
                    .retrieve()
                    .bodyToMono(GoogleOAuth2AuthorizationResponse.class)
                    .block();
        } catch (WebClientResponseException | HttpClientErrorException.BadRequest e) {
            throw BusinessException.of(API_CALLBACK_EXCEPTION);
        } catch (Exception e) {
            throw BusinessException.of(TIMEOUT_EXCEPTION);
        }
    }

    private GoogleMemberProfileResponse getProfile(GoogleOAuth2AuthorizationResponse authorization) {
        try {
            return webClient.get()
                    .uri(baseUrl)
                    .accept(APPLICATION_JSON)
                    .header("Authorization", "Bearer " + authorization.getAccessToken())
                    .retrieve()
                    .bodyToMono(GoogleMemberProfileResponse.class)
                    .block();
        } catch (WebClientResponseException | HttpClientErrorException.BadRequest e) {
            throw BusinessException.of(API_CALLBACK_EXCEPTION);
        } catch (Exception e) {
            throw BusinessException.of(TIMEOUT_EXCEPTION);
        }
    }
}
