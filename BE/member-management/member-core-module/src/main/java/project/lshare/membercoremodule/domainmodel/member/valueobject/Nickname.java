package project.lshare.membercoremodule.domainmodel.member.valueobject;

import project.lshare.membercoremodule.common.error.ErrorField;

import java.util.Objects;

public class Nickname {

    private final String nickname;

    public Nickname(String nickname) {
        validateNickname(nickname);
        this.nickname = nickname;
    }

    private void validateNickname(String nickname) {
        if (nickname == null || nickname.isBlank()) {
            throw new IllegalArgumentException("닉네임은 공백일 수 없습니다.", ErrorField.of("NickName", nickname));
        }
        if (nickname.length() > 39) {
            throw new IllegalArgumentException("입력 가능한 닉네임의 최대길이를 초과했습니다.", ErrorField.of("NickName", nickname));
        }

        String blankDeletedNickname = nickname.replaceAll(" ", "");

        if (!blankDeletedNickname.equals(nickname)) {
            throw new IllegalArgumentException("닉네임에 공백이 존재할 수 없습니다.", ErrorField.of("NickName", nickname));
        }
    }

    public String getNickname() {
        return nickname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Nickname nickName)) return false;
        return getNickname().equals(nickName.getNickname());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getNickname());
    }

    @Override
    public String toString() {
        return nickname;
    }
}
