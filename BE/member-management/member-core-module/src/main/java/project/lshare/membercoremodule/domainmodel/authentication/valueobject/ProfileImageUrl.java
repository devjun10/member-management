package project.lshare.membercoremodule.domainmodel.authentication.valueobject;

import project.lshare.membercoremodule.common.error.ErrorField;

import java.util.Objects;

public class ProfileImageUrl {

    private final String profileImageUrl;

    public ProfileImageUrl(String profileImageUrl) {
        validateProfileImageUrl(profileImageUrl);
        this.profileImageUrl = profileImageUrl;
    }

    private void validateProfileImageUrl(String profileImageUrl) {
        if (profileImageUrl == null || profileImageUrl.isBlank()) {
            throw new IllegalArgumentException("프로필 이미지가 존재하지 않습니다.", ErrorField.of("ProfileImageUrl", profileImageUrl));
        }
        if (profileImageUrl.length() > 1000) {
            throw new IllegalArgumentException("프로필 이미지 최대 입력길이를 초과했습니다.", ErrorField.of("ProfileImageUrl", profileImageUrl));
        }
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProfileImageUrl that)) return false;
        return getProfileImageUrl().equals(that.getProfileImageUrl());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getProfileImageUrl());
    }

    @Override
    public String toString() {
        return profileImageUrl;
    }
}
