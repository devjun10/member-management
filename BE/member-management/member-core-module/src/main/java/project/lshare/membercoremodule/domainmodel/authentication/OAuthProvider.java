package project.lshare.membercoremodule.domainmodel.authentication;

public enum OAuthProvider {
    GOOGLE,
    GITHUB
}
