package project.lshare.membercoremodule.persistencemodel.member;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import project.lshare.membercoremodule.common.field.Deleted;
import project.lshare.membercoremodule.domainmodel.member.Role;

import java.util.Optional;

@Repository
public interface SpringDataMemberRepository extends JpaRepository<MemberJpaEntity, Long> {

    @Query(
            "SELECT m FROM member m " +
                    "WHERE m.memberId=:memberId AND m.deleted=:deleted"
    )
    Optional<MemberJpaEntity> findByIdAndDeletedFalse(
            @Param("memberId") Long memberId,
            @Param("deleted") Deleted deleted
    );

    @Query(
            "SELECT m FROM member m " +
                    "WHERE m.memberId=:memberId AND m.role=:role"
    )
    Optional<MemberJpaEntity> findByIdAndRoleAAndDeletedFalse(
            @Param("memberId") Long memberId,
            @Param("role") Role role
    );
}
