package project.lshare.membercoremodule.domainmodel.member.valueobject;

import java.util.Objects;

public class LastLoginIpAddress {

    private final String lastLoginIpAddress;

    public LastLoginIpAddress(String lastLoginIpAddress) {
        if (lastLoginIpAddress == null || lastLoginIpAddress.isBlank()) {
            this.lastLoginIpAddress = "UN_KNOWN";
            return;
        }
        this.lastLoginIpAddress = lastLoginIpAddress;
    }

    public String getLastLoginIpAddress() {
        return lastLoginIpAddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LastLoginIpAddress nickName)) return false;
        return getLastLoginIpAddress().equals(nickName.getLastLoginIpAddress());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getLastLoginIpAddress());
    }

    @Override
    public String toString() {
        return lastLoginIpAddress;
    }
}
