package project.lshare.membercoremodule.domainmodel.member;

import java.util.Arrays;
import java.util.function.Predicate;

public enum District {
    SEOUL,
    BUSAN,
    DAEGU,
    INCHEON,
    DAEJEON,
    ULSAN;

    public static District findDistrictByName(String name) {
        return Arrays.stream(values())
                .filter(equalTo(name))
                .findAny()
                .orElseThrow(() -> new IllegalArgumentException("올바르지 않은 도시 입니다."));
    }

    private static Predicate<District> equalTo(String name) {
        return district -> district.name().equals(name);
    }
}
