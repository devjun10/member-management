package project.lshare.membercoremodule.domainmodel.authentication.valueobject;

import project.lshare.membercoremodule.domainmodel.member.Member;
import project.lshare.membercoremodule.domainmodel.member.Role;

import java.util.Objects;

public class AuthenticatedMember {

    private final Long memberId;
    private final Role role;

    public AuthenticatedMember(Member member) {
        validate(member);
        this.memberId = member.getMemberId();
        this.role = member.getRole();
    }

    private void validate(Member member) {
        if (member == null) {
            throw new IllegalArgumentException();
        }
    }

    public AuthenticatedMember(
            Long memberId,
            Role role
    ) {
        validate(memberId, role);
        this.memberId = memberId;
        this.role = role;
    }

    private void validate(Long memberId, Role role) {
        if (memberId == null || role == null) {
            throw new IllegalArgumentException();
        }
    }

    public Long getMemberId() {
        return memberId;
    }

    public Role getRole() {
        return role;
    }

    public boolean isAdmin() {
        return role.equals(Role.ADMIN);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AuthenticatedMember that)) return false;
        return getMemberId().equals(that.getMemberId()) && getRole() == that.getRole();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMemberId(), getRole());
    }

    @Override
    public String toString() {
        return String.format("MemberId: %s, Role: %s", memberId, role);
    }
}
