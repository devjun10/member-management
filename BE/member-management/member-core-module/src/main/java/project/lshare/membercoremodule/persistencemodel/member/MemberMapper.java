package project.lshare.membercoremodule.persistencemodel.member;

import org.springframework.stereotype.Component;
import project.lshare.membercoremodule.domainmodel.member.Member;

@Component
public class MemberMapper {

    public Member mapToDomainEntity(MemberJpaEntity member) {
        return new Member(
                member.getMemberId(),
                member.getNickname(),
                member.getProfileImageUrl(),
                member.getUserAgent(),
                member.getLastLoginIpaddress(),
                member.getRole(),
                member.getDistrict(),
                member.getDeleted()
        );
    }

    public MemberJpaEntity mapToJpaEntity(Member member) {
        return new MemberJpaEntity(
                member.getMemberId(),
                member.getNickname(),
                member.getProfileImageUrl(),
                member.getUserAgent(),
                member.getLastLoginIpAddress(),
                member.getRole(),
                member.getDistrict(),
                member.getDeleted()
        );
    }
}
