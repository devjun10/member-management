package project.lshare.membercoremodule.domainmodel.member;

import org.springframework.http.HttpStatus;
import project.lshare.membercoremodule.common.mapper.BaseExceptionType;

public enum MemberTypeException implements BaseExceptionType {

    INVALID_ADMIN_ACCESS_EXCEPTION(401, "관리자 권한이 없습니다.", HttpStatus.UNAUTHORIZED),
    MEMBER_NOT_FOUND_EXCEPTION(404, "회원을 찾을 수 없습니다.", HttpStatus.NOT_FOUND);

    private final int code;
    private final String message;
    private final HttpStatus status;

    MemberTypeException(
            int code,
            String message,
            HttpStatus status
    ) {
        this.code = code;
        this.message = message;
        this.status = status;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getMssage() {
        return message;
    }

    @Override
    public HttpStatus getStatus() {
        return status;
    }
}
