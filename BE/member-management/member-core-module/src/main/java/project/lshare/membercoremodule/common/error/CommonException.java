package project.lshare.membercoremodule.common.error;

import org.springframework.http.HttpStatus;
import project.lshare.membercoremodule.common.mapper.BaseExceptionType;

import static org.springframework.http.HttpStatus.BAD_GATEWAY;

public enum CommonException implements BaseExceptionType {
    BAD_REQUEST(400, "올바르지 않은 값입니다.", HttpStatus.BAD_REQUEST),

    API_CALLBACK_EXCEPTION(400, "올바르지 않은 값입니다.", HttpStatus.BAD_REQUEST),
    TIMEOUT_EXCEPTION(502, "외부 서버와의 통신에 실패했습니다.", BAD_GATEWAY);

    private final int code;
    private final String message;
    private final HttpStatus status;

    CommonException(
            int code,
            String message,
            HttpStatus status
    ) {
        this.code = code;
        this.message = message;
        this.status = status;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getMssage() {
        return message;
    }

    @Override
    public HttpStatus getStatus() {
        return status;
    }
}
