package project.lshare.membercoremodule.persistencemodel.authentication;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SpringDataAuthenticationRepository extends JpaRepository<AuthenticationJpaEntity, Long> {

    @Query(value = "SELECT * FROM authentication AS a JOIN member m on m.member_id = a.member_id WHERE a.email=:email", nativeQuery = true)
    Optional<AuthenticationJpaEntity> findAuthenticationJpaEntitiesByEmail(@Param("email") String email);

}
