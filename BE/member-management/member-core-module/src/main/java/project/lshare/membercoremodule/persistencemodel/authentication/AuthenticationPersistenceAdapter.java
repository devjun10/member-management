package project.lshare.membercoremodule.persistencemodel.authentication;

import project.lshare.membercoremodule.common.annotation.helper.PersistenceAdapter;
import project.lshare.membercoremodule.common.field.Deleted;
import project.lshare.membercoremodule.domainmodel.authentication.Authentication;
import project.lshare.membercoremodule.persistencemodel.member.MemberJpaEntity;
import project.lshare.membercoremodule.persistencemodel.member.MemberMapper;
import project.lshare.membercoremodule.persistencemodel.member.SpringDataMemberRepository;

@PersistenceAdapter
public class AuthenticationPersistenceAdapter {

    private final SpringDataAuthenticationRepository authenticationRepository;
    private final SpringDataMemberRepository memberRepository;
    private final AuthenticationMapper authenticationMapper;
    private final MemberMapper memberMapper;

    public AuthenticationPersistenceAdapter(
            SpringDataAuthenticationRepository authenticationRepository,
            SpringDataMemberRepository memberRepository,
            AuthenticationMapper authenticationMapper,
            MemberMapper memberMapper
    ) {
        this.authenticationRepository = authenticationRepository;
        this.memberRepository = memberRepository;
        this.authenticationMapper = authenticationMapper;
        this.memberMapper = memberMapper;
    }

    public Authentication findAuthenticationById(
            Long authenticationId,
            Long memberId
    ) {
        AuthenticationJpaEntity authentication = authenticationRepository.findById(authenticationId)
                .orElseThrow();
        MemberJpaEntity member = memberRepository.findByIdAndDeletedFalse(memberId, Deleted.FALSE)
                .orElseThrow();
        return authenticationMapper.mapToDomainEntity(authentication, memberMapper.mapToDomainEntity(member));
    }
}
