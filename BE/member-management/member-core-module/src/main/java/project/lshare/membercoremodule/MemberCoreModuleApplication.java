package project.lshare.membercoremodule;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MemberCoreModuleApplication {

    public static void main(String[] args) {
        SpringApplication.run(MemberCoreModuleApplication.class, args);
    }

}
