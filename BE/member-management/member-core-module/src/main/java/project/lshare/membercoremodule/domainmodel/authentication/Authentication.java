package project.lshare.membercoremodule.domainmodel.authentication;

import project.lshare.membercoremodule.common.field.Deleted;
import project.lshare.membercoremodule.domainmodel.authentication.valueobject.AuthenticationId;
import project.lshare.membercoremodule.domainmodel.authentication.valueobject.Email;
import project.lshare.membercoremodule.domainmodel.authentication.valueobject.ProfileImageUrl;
import project.lshare.membercoremodule.domainmodel.member.Member;

import java.util.Objects;

public class Authentication {

    private final AuthenticationId authenticationId;
    private final Member member;
    private final OAuthProvider oAuthProvider;
    private final Email email;
    private final ProfileImageUrl profileImageUrl;
    private Deleted deleted;

    public Authentication(
            Long authenticationId,
            Member member,
            OAuthProvider oAuthProvider,
            String email,
            String imageUrl,
            Deleted deleted
    ) {
        this.authenticationId = new AuthenticationId(authenticationId);
        this.member = member;
        this.oAuthProvider = oAuthProvider;
        this.email = new Email(email);
        this.profileImageUrl = new ProfileImageUrl(imageUrl);
        this.deleted = deleted;
    }

    public Long getAuthenticationId() {
        return authenticationId.getAuthenticationId();
    }

    public Member getMember() {
        return member;
    }

    public OAuthProvider getOAuthProvider() {
        return oAuthProvider;
    }

    public String getEmail() {
        return email.getEmail();
    }

    public String getProfileImageUrl() {
        return profileImageUrl.getProfileImageUrl();
    }

    public Deleted getDeleted() {
        return deleted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Authentication that)) return false;
        return getAuthenticationId().equals(that.getAuthenticationId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAuthenticationId());
    }

    @Override
    public String toString() {
        return authenticationId.toString();
    }
}
