package project.lshare.membercoremodule.domainmodel.authentication.valueobject;

import project.lshare.membercoremodule.common.error.ErrorField;

import java.util.Objects;

public class AuthenticationId {

    private final Long authenticationId;

    public AuthenticationId(Long authenticationId) {
        validateAuthenticationId(authenticationId);
        this.authenticationId = authenticationId;
    }

    private void validateAuthenticationId(Long authenticationId) {
        if (authenticationId == null || authenticationId <= 0L) {
            throw new IllegalArgumentException("올바른 인증 아이디가 아닙니다.", ErrorField.of("AuthenticationId", authenticationId));
        }
    }

    public Long getAuthenticationId() {
        return authenticationId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AuthenticationId that)) return false;
        return getAuthenticationId().equals(that.getAuthenticationId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAuthenticationId());
    }

    @Override
    public String toString() {
        return authenticationId.toString();
    }
}
