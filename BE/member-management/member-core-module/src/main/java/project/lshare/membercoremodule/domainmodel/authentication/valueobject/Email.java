package project.lshare.membercoremodule.domainmodel.authentication.valueobject;

import project.lshare.membercoremodule.common.error.ErrorField;

import java.util.Objects;
import java.util.regex.Pattern;

public class Email {

    private static final String regex = "^[_a-zA-Z0-9-]+(.[_a-z0-9-]+)*@(?:\\w+\\.)+\\w+$";
    private static final Pattern pattern = Pattern.compile(regex);

    private String email;

    public Email(String email) {
        validateEmail(email);
        this.email = email;
    }

    private void validateEmail(String email) {
        if (email == null || email.isBlank()) {
            throw new IllegalArgumentException("Email은 공백일 수 없습니다.", ErrorField.of("Email", email));
        }
        if (!isCorrectFormat(email)) {
            throw new IllegalArgumentException("올바른 양식의 이메일을 입력해주세요.", ErrorField.of("Email", email));
        }
    }

    private static boolean isCorrectFormat(String email) {
        return pattern.matcher(email).matches();
    }

    public String getEmail() {
        return email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Email)) return false;
        Email email1 = (Email) o;
        return getEmail().equals(email1.getEmail());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getEmail());
    }

    @Override
    public String toString() {
        return email;
    }
}
