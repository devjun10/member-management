package project.lshare.membercoremodule.common.mapper;

import org.springframework.http.HttpStatus;

public class BusinessException extends RuntimeException {

    private final BaseExceptionType baseExceptionType;

    private BusinessException(BaseExceptionType baseExceptionType) {
        super(baseExceptionType.getMssage());
        this.baseExceptionType = baseExceptionType;
    }

    public static BusinessException of(BaseExceptionType baseExceptionType) {
        return new BusinessException(baseExceptionType);
    }

    public BaseExceptionType getBaseExceptionType() {
        return baseExceptionType;
    }

    public int getCode() {
        return baseExceptionType.getCode();
    }

    public HttpStatus getHttpStatus() {
        return baseExceptionType.getStatus();
    }
}
