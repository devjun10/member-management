package project.lshare.membercoremodule.common.mapper;

import org.springframework.http.HttpStatus;

public interface BaseExceptionType {

    int getCode();

    String getMssage();

    HttpStatus getStatus();

}
