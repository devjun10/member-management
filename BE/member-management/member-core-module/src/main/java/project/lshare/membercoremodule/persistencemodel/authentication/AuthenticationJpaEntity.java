package project.lshare.membercoremodule.persistencemodel.authentication;

import project.lshare.membercoremodule.common.field.Deleted;
import project.lshare.membercoremodule.domainmodel.authentication.OAuthProvider;
import project.lshare.membercoremodule.persistencemodel.member.MemberJpaEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Objects;

@Entity(name = "authentication")
@Table(name = "authentication")
public class AuthenticationJpaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long authenticationId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "member_id")
    private MemberJpaEntity member;

    @Enumerated(EnumType.STRING)
    private OAuthProvider oauthProvider;

    @Column
    private String email;

    @Enumerated(EnumType.STRING)
    private EmailVerified emailVerified;

    @Column
    private String profileImageUrl;

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "ENUM('TRUE', 'FALSE')")
    private Deleted deleted;

    /**
     * @Nullary-Constructor. JPA 기본 생성자로 authentication 외부 패키지에서 호출하지 말 것.
     */
    protected AuthenticationJpaEntity() {
    }

    public AuthenticationJpaEntity(
            MemberJpaEntity member,
            String email,
            String profileImageUrl
    ) {
        this.member = member;
        this.oauthProvider = OAuthProvider.GOOGLE;
        this.email = email;
        this.profileImageUrl = profileImageUrl;
        this.deleted = Deleted.FALSE;
    }

    public AuthenticationJpaEntity(
            MemberJpaEntity member,
            String email,
            boolean emailVerified,
            String profileImageUrl
    ) {
        this.member = member;
        this.oauthProvider = OAuthProvider.GOOGLE;
        this.email = email;
        this.emailVerified = EmailVerified.check(emailVerified);
        this.profileImageUrl = profileImageUrl;
        this.deleted = Deleted.FALSE;
    }

    public AuthenticationJpaEntity(
            Long authenticationId,
            MemberJpaEntity member,
            OAuthProvider oauthProvider,
            String email,
            String profileImageUrl,
            Deleted deleted
    ) {
        this.authenticationId = authenticationId;
        this.member = member;
        this.oauthProvider = oauthProvider;
        this.email = email;
        this.profileImageUrl = profileImageUrl;
        this.deleted = deleted;
    }

    public Long getAuthenticationId() {
        return authenticationId;
    }

    public MemberJpaEntity getMember() {
        return member;
    }

    public OAuthProvider getOauthProvider() {
        return oauthProvider;
    }

    public String getEmail() {
        return email;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public Deleted getDeleted() {
        return deleted;
    }

    public void updateProfile(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AuthenticationJpaEntity that)) return false;
        return getAuthenticationId().equals(that.getAuthenticationId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAuthenticationId());
    }

    @Override
    public String toString() {
        return authenticationId.toString();
    }
}
