package project.lshare.membercoremodule.persistencemodel.authentication;

import org.springframework.stereotype.Component;
import project.lshare.membercoremodule.domainmodel.authentication.Authentication;
import project.lshare.membercoremodule.domainmodel.member.Member;
import project.lshare.membercoremodule.persistencemodel.member.MemberJpaEntity;

@Component
public class AuthenticationMapper {

    public Authentication mapToDomainEntity(
            AuthenticationJpaEntity authenticationJpaEntity,
            Member member
    ) {
        return new Authentication(
                authenticationJpaEntity.getAuthenticationId(),
                member,
                authenticationJpaEntity.getOauthProvider(),
                authenticationJpaEntity.getEmail(),
                authenticationJpaEntity.getProfileImageUrl(),
                authenticationJpaEntity.getDeleted()
        );
    }

    public AuthenticationJpaEntity mapToJpaEntity(
            Authentication authentication,
            MemberJpaEntity memberJpaEntity
    ) {
        return new AuthenticationJpaEntity(
                authentication.getAuthenticationId(),
                memberJpaEntity,
                authentication.getOAuthProvider(),
                authentication.getEmail(),
                authentication.getProfileImageUrl(),
                authentication.getDeleted()
        );
    }
}
