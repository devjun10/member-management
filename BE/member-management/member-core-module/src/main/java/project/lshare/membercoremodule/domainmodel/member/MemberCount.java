package project.lshare.membercoremodule.domainmodel.member;

import java.util.Objects;

public class MemberCount {

    private final Long totalCount;

    public MemberCount(Long totalCount) {
        this.totalCount = totalCount;
    }

    public Long getTotalCount() {
        return totalCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MemberCount that)) return false;
        return getTotalCount().equals(that.getTotalCount());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTotalCount());
    }

    @Override
    public String toString() {
        return totalCount.toString();
    }
}
