package project.lshare.membercoremodule.persistencemodel.member;

import project.lshare.membercoremodule.common.field.Deleted;
import project.lshare.membercoremodule.domainmodel.member.District;
import project.lshare.membercoremodule.domainmodel.member.Role;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity(name = "member")
@Table(name = "member")
public class MemberJpaEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long memberId;

    @Column
    private String nickname;

    @Column
    private String profileImageUrl;

    @Column
    private String userAgent;

    @Column
    private String lastLoginIpaddress;

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "ENUM('NORMAL', 'ADMIN')")
    private Role role;

    @Column(columnDefinition = "ENUM('SEOUL', 'BUSAN', 'DAEGU', 'INCHEON', 'DAEJEON', 'ULSAN')")
    @Enumerated(EnumType.STRING)
    private District district;

    @Enumerated(EnumType.STRING)
    @Column(columnDefinition = "ENUM('TRUE', 'FALSE')")
    private Deleted deleted;

    /**
     * @Nullary-Constructor. JPA 기본 생성자로 member 외부 패키지에서 호출하지 말 것.
     */
    protected MemberJpaEntity() {
    }

    public MemberJpaEntity(
            String nickname,
            String profileImageUrl
    ) {
        this(null, nickname, profileImageUrl, null, null, null);
    }

    public MemberJpaEntity(
            Long memberId,
            String nickname,
            String profileImageUrl,
            String userAgent,
            String lastLoginedIpAddress,
            District district
    ) {
        this.memberId = memberId;
        this.nickname = nickname;
        this.profileImageUrl = profileImageUrl;
        this.userAgent = userAgent;
        this.lastLoginIpaddress = lastLoginedIpAddress;
        this.role = Role.NORMAL;
        this.district = district;
        this.deleted = Deleted.FALSE;
    }


    public MemberJpaEntity(
            Long memberId,
            String nickname,
            String profileImageUrl,
            String userAgent,
            String lastLoginedIpAddress,
            Role role,
            District district,
            Deleted deleted
    ) {
        this.memberId = memberId;
        this.nickname = nickname;
        this.profileImageUrl = profileImageUrl;
        this.userAgent = userAgent;
        this.lastLoginIpaddress = lastLoginedIpAddress;
        this.role = role;
        this.district = district;
        this.deleted = deleted;
    }

    public Long getMemberId() {
        return memberId;
    }

    public String getNickname() {
        return nickname;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public String getLastLoginIpaddress() {
        return lastLoginIpaddress;
    }

    public Role getRole() {
        return role;
    }

    public District getDistrict() {
        return district;
    }

    public Deleted getDeleted() {
        return deleted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MemberJpaEntity that)) return false;
        return getMemberId().equals(that.getMemberId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMemberId());
    }

    @Override
    public String toString() {
        return memberId.toString();
    }
}
