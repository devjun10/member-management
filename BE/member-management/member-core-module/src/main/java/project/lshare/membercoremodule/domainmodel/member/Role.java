package project.lshare.membercoremodule.domainmodel.member;

public enum Role {
    NORMAL,
    ADMIN
}
