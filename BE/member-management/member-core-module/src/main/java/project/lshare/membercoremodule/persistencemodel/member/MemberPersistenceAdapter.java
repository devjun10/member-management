package project.lshare.membercoremodule.persistencemodel.member;

import project.lshare.membercoremodule.common.annotation.helper.PersistenceAdapter;
import project.lshare.membercoremodule.common.field.Deleted;
import project.lshare.membercoremodule.common.mapper.BusinessException;
import project.lshare.membercoremodule.domainmodel.member.Member;

import static project.lshare.membercoremodule.domainmodel.member.MemberTypeException.MEMBER_NOT_FOUND_EXCEPTION;

@PersistenceAdapter
public class MemberPersistenceAdapter {

    private final SpringDataMemberRepository memberRepository;
    private final MemberMapper mapper;

    public MemberPersistenceAdapter(
            SpringDataMemberRepository memberRepository,
            MemberMapper memberMapper
    ) {
        this.memberRepository = memberRepository;
        this.mapper = memberMapper;
    }

    public Member findMemberById(Long memberId) {
        MemberJpaEntity memberJpaEntity = memberRepository.findByIdAndDeletedFalse(memberId, Deleted.FALSE)
                .orElseThrow(()-> BusinessException.of(MEMBER_NOT_FOUND_EXCEPTION));
        return mapper.mapToDomainEntity(memberJpaEntity);
    }

    public Member findAdminById(Long memberId) {
        MemberJpaEntity memberJpaEntity = memberRepository.findByIdAndDeletedFalse(memberId, Deleted.FALSE)
                .orElseThrow(()-> BusinessException.of(MEMBER_NOT_FOUND_EXCEPTION));
        return mapper.mapToDomainEntity(memberJpaEntity);
    }

    public void withdrawlMembership(MemberJpaEntity memberJpaEntity) {
        memberRepository.save(memberJpaEntity);
    }

    public void update(Member member) {
        if (member.getMemberId() != null) {
            memberRepository.save(mapper.mapToJpaEntity(member));
        }
    }
}
