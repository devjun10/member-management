package project.lshare.membercoremodule.persistencemodel.authentication;

public enum EmailVerified {
    TRUE,
    FALSE;

    public static EmailVerified check(boolean emailVerified) {
        return emailVerified ? TRUE : FALSE;
    }
}
