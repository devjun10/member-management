package project.lshare.membercoremodule.domainmodel.member.valueobject;

import project.lshare.membercoremodule.common.error.ErrorField;

import java.util.Objects;

public class MemberId {

    private final Long memberId;

    public MemberId(Long memberId) {
        validateMemberId(memberId);
        this.memberId = memberId;
    }

    private void validateMemberId(Long memberId) {
        if (memberId == null || memberId <= 0L) {
            throw new IllegalArgumentException("올바른 회원 아이디가 아닙니다.", ErrorField.of("MemberId", memberId));
        }
    }

    public Long getMemberId() {
        return memberId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MemberId memberId1)) return false;
        return getMemberId().equals(memberId1.getMemberId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMemberId());
    }

    @Override
    public String toString() {
        return memberId.toString();
    }
}
