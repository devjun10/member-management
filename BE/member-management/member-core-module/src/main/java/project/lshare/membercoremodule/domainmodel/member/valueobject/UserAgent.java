package project.lshare.membercoremodule.domainmodel.member.valueobject;

import java.util.Objects;

public class UserAgent {

    private final String userAgent;

    public UserAgent(String userAgent) {
        if (userAgent == null) {
            this.userAgent = "";
            return;
        }
        this.userAgent = userAgent;
    }

    public String getUserAgent() {
        return userAgent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserAgent nickName)) return false;
        return getUserAgent().equals(nickName.getUserAgent());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUserAgent());
    }

    @Override
    public String toString() {
        return userAgent;
    }
}
