package project.lshare.membercoremodule.domainmodel.member;

import project.lshare.membercoremodule.common.field.Deleted;
import project.lshare.membercoremodule.domainmodel.authentication.valueobject.ProfileImageUrl;
import project.lshare.membercoremodule.domainmodel.member.valueobject.LastLoginIpAddress;
import project.lshare.membercoremodule.domainmodel.member.valueobject.MemberId;
import project.lshare.membercoremodule.domainmodel.member.valueobject.Nickname;
import project.lshare.membercoremodule.domainmodel.member.valueobject.UserAgent;

import java.util.Objects;

public class Member {

    private final MemberId memberId;
    private Nickname nickname;
    private ProfileImageUrl profileImageUrl;
    private final UserAgent userAgent;
    private LastLoginIpAddress lastLoginedIpAddress;
    private Role role;
    private District district;
    private Deleted deleted;

    public Member(
            Long memberId,
            String nickname,
            String profileImageUrl,
            String userAgent,
            String lastLoginedIpAddress,
            Role role,
            District district,
            Deleted deleted
    ) {
        this.memberId = new MemberId(memberId);
        this.nickname = new Nickname(nickname);
        this.profileImageUrl = new ProfileImageUrl(profileImageUrl);
        this.userAgent = new UserAgent(userAgent);
        this.lastLoginedIpAddress = new LastLoginIpAddress(lastLoginedIpAddress);
        this.role = role;
        this.district = district;
        this.deleted = deleted;
    }

    public Long getMemberId() {
        return memberId.getMemberId();
    }

    public String getNickname() {
        return nickname.getNickname();
    }

    public String getProfileImageUrl() {
        return profileImageUrl.getProfileImageUrl();
    }

    public String getUserAgent() {
        return userAgent.getUserAgent();
    }

    public String getLastLoginIpAddress() {
        return lastLoginedIpAddress.getLastLoginIpAddress();
    }

    public Role getRole() {
        return role;
    }

    public District getDistrict() {
        return district;
    }

    public Deleted getDeleted() {
        return deleted;
    }

    public void changeNickname(Nickname nickname) {
        this.nickname = nickname;
    }

    public void withdrawlMembership() {
        this.deleted = Deleted.TRUE;
    }

    public void updateProfileImageUrl(ProfileImageUrl profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public void updateLastLoginIpAddress(LastLoginIpAddress lastLoginIpAddress) {
        this.lastLoginedIpAddress = lastLoginIpAddress;
    }

    public void changeRole(Role role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Member member)) return false;
        return getMemberId().equals(member.getMemberId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getMemberId());
    }

    @Override
    public String toString() {
        return memberId.toString();
    }
}
