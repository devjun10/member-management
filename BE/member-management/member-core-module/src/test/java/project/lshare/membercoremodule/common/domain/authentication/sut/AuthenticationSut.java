package project.lshare.membercoremodule.common.domain.authentication.sut;

import project.lshare.membercoremodule.common.field.Deleted;
import project.lshare.membercoremodule.domainmodel.authentication.Authentication;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class AuthenticationSut {

    private final Authentication authentication;

    public AuthenticationSut(Authentication authentication) {
        this.authentication = authentication;
    }

    public AuthenticationSut shouldExist() {
        assertNotNull(authentication);
        return this;
    }

    public AuthenticationSut withId() {
        assertNotNull(authentication.getAuthenticationId());
        return this;
    }

    public AuthenticationSut withMember() {
        assertNotNull(authentication.getMember());
        return this;
    }

    public AuthenticationSut withOAuthProvider() {
        assertNotNull(authentication.getOAuthProvider());
        return this;
    }

    public AuthenticationSut withEmail() {
        assertNotNull(authentication.getEmail());
        return this;
    }

    public AuthenticationSut withProfileImageUrl() {
        assertNotNull(authentication.getProfileImageUrl());
        return this;
    }

    public AuthenticationSut withNotDeleted() {
        assertEquals(Deleted.FALSE, authentication.getDeleted());
        return this;
    }
}
