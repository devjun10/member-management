package project.lshare.membercoremodule.common.domain.member.fixture;

import project.lshare.membercoremodule.common.field.Deleted;
import project.lshare.membercoremodule.domainmodel.member.District;
import project.lshare.membercoremodule.domainmodel.member.Member;
import project.lshare.membercoremodule.domainmodel.member.Role;
import project.lshare.membercoremodule.persistencemodel.member.MemberJpaEntity;

public class MemberFixture {

    public static Member createMemberDomainWithAllArgs(Long memberId) {
        return new Member(
                memberId,
                "devjun10",
                "https://e1.pngegg.com/pngimages/817/696/png-clipart-lionel-messi.png",
                "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36",
                "192.168.10.23",
                Role.NORMAL,
                District.SEOUL,
                Deleted.FALSE
        );
    }

    public static MemberJpaEntity createNormalMemberJpaEntity() {
        return new MemberJpaEntity(
                "devjun10",
                "https://e1.pngegg.com/pngimages/817/696/png-clipart-lionel-messi.png"
        );
    }

    public static MemberJpaEntity createNormalMemberJpaEntityWithAllArgs() {
        return new MemberJpaEntity(
                null,
                "devjun10",
                "https://e1.pngegg.com/pngimages/817/696/png-clipart-lionel-messi.png",
                "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36",
                "192.168.10.23",
                Role.NORMAL,
                District.SEOUL,
                Deleted.FALSE
        );
    }

    public static MemberJpaEntity createNormalMemberJpaEntityWithAllArgs(Long memberId) {
        return new MemberJpaEntity(
                memberId,
                "devjun10",
                "https://e1.pngegg.com/pngimages/817/696/png-clipart-lionel-messi.png",
                "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36",
                "192.168.10.23",
                Role.NORMAL,
                District.SEOUL,
                Deleted.FALSE
        );
    }
}
