package project.lshare.membercoremodule.test.member.domain.valueobject;

import org.apache.logging.log4j.util.Strings;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import project.lshare.membercoremodule.domainmodel.member.valueobject.Nickname;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("회원 도메인(닉네임) 테스트")
class NicknameTest {

    @DisplayName("회원 닉네임 객체를 생성할때")
    static class NestedTest {

        @Test
        @DisplayName("최대 글자수를 초과하면 IllegalArgumentException이 발생한다.")
        void 닉네임_최대_글자수_테스트() {
            assertThrows(
                    IllegalArgumentException.class,
                    () -> new Nickname(Strings.repeat("A", 40))
            );
        }

        @ParameterizedTest
        @ValueSource(strings = {"devj un10", "devjun1023 ", " devjun23"})
        @DisplayName("글자 중간에 공백이 들어가면 IllegalArgumentException이 발생한다.")
        void 닉네임_중간_공백_테스트(String parameter) {
            assertThrows(
                    IllegalArgumentException.class,
                    () -> new Nickname(parameter)
            );
        }

        @ParameterizedTest
        @NullAndEmptySource
        @DisplayName("값이 Null 또는 비어있다면 IllegalArgumentException이 발생한다.")
        void 닉네임_값이_Null_또는_공백일때_테스트(String parameter) {
            assertThrows(
                    IllegalArgumentException.class,
                    () -> new Nickname(parameter)
            );
        }

        // ------------------------------------------------------------------------

        @ParameterizedTest
        @ValueSource(strings = {"devjun10", "devjun12", "devjun1034", "devjun102321"})
        @DisplayName("equals와 hashCode를 재정의했다면 값으로 객체를 비교한다.")
        void 닉네임_euqlas_hashcode_재정의_테스트(String parameter) {
            assertEquals(
                    new Nickname(parameter),
                    new Nickname(parameter)
            );
        }

        @ParameterizedTest
        @ValueSource(strings = {"devjun10"})
        @DisplayName("equals와 hashCode를 재정의했다면 equals 메서드로 값을 비교할때 같은 객체로 인식한다.")
        void 닉네임_euqlas_재정의_테스트(String parameter) {
            Nickname nickname = new Nickname(parameter);
            assertTrue(
                    nickname.equals(new Nickname(parameter))
            );
        }

        @ParameterizedTest
        @ValueSource(strings = {"devjun10"})
        @DisplayName("같은 객체를 equals로 비교하면 true를 반환한다.")
        void 같은_객체의_equals_테스트(String parameter) {
            Nickname nickname = new Nickname(parameter);
            assertTrue(
                    nickname.equals(nickname)
            );
        }

        @ParameterizedTest
        @ValueSource(strings = {"devjun10"})
        @DisplayName("다른 타입의 객체라면 false가 반환된다.")
        void 닉네임_euqlas_instanceof_테스트(String parameter) {
            assertFalse(
                    new Nickname(parameter).equals(1L)
            );
        }

        @ParameterizedTest
        @ValueSource(strings = {"devjun10"})
        @DisplayName("hashcode를 재정의 했다면 같은 값이 반환된다.")
        void 닉네임_hashcode_재정의_테스트(String parameter) {
            assertEquals(
                    new Nickname(parameter).hashCode(),
                    new Nickname(parameter).hashCode()
            );
        }

        @ParameterizedTest
        @ValueSource(strings = {"devjun10"})
        @DisplayName("toString을 재정의했다면 재정의한 결과에 맞는 값이 반환된다.")
        void 닉네임_toString_재정의_테스트(String parameter) {
            assertEquals(
                    parameter,
                    new Nickname(parameter).toString()
            );
        }
    }
}
