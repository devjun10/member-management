package project.lshare.membercoremodule.test.member.domain;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import project.lshare.membercoremodule.domainmodel.member.District;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@DisplayName("회원 도메인(지역) 테스트")
class DistrictTest {

    @ParameterizedTest
    @ValueSource(strings = {"JAPAN", "CHINA", "AMERICA", "NORTH_KOREA"})
    @DisplayName("올바르지 않은 지역을 입력하면 IllegalArgumentException이 발생한다.")
    void 도시_조회_실패_테스트(String parameter) {
        assertThatThrownBy(() -> District.findDistrictByName(parameter))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("올바르지 않은 도시 입니다.");
    }

    @ParameterizedTest
    @ValueSource(strings = {"SEOUL", "BUSAN", "DAEGU", "INCHEON", "DAEJEON", "ULSAN"})
    @DisplayName("올바르지 않은 지역을 입력하면 IllegalArgumentException이 발생한다.")
    void 도시_조회_성공_테스트(String parameter) {
        assertNotNull(
                District.findDistrictByName(parameter)
        );
    }
}
