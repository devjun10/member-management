package project.lshare.membercoremodule.common.domain.authentication.fixture;

import project.lshare.membercoremodule.common.field.Deleted;
import project.lshare.membercoremodule.domainmodel.authentication.OAuthProvider;
import project.lshare.membercoremodule.persistencemodel.authentication.AuthenticationJpaEntity;
import project.lshare.membercoremodule.persistencemodel.member.MemberJpaEntity;

public class AuthenticationFixture {

    public static AuthenticationJpaEntity createAuthenticationJpaEntity(MemberJpaEntity member) {
        return new AuthenticationJpaEntity(
                member,
                "devjun1023@gmail.com",
                "https://e1.pngegg.com/pngimages/817/696/png-clipart-lionel-messi.png"
        );
    }

    public static AuthenticationJpaEntity createAuthenticationJpaEntityWithAllArgs(MemberJpaEntity member) {
        return new AuthenticationJpaEntity(
                1L,
                member,
                OAuthProvider.GOOGLE,
                "devjun1023@gmail.com",
                "https://e1.pngegg.com/pngimages/817/696/png-clipart-lionel-messi.png",
                Deleted.FALSE
        );
    }
}
