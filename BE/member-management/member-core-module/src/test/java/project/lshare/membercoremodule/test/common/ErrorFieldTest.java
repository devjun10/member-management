package project.lshare.membercoremodule.test.common;

import org.apache.logging.log4j.util.Strings;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.lshare.membercoremodule.common.error.ErrorField;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("도메인 공통 필드(에러) 테스트")
class ErrorFieldTest {

    @DisplayName("에러 필드 객체가 생성될때")
    static class NestedTest {

        @Test
        @DisplayName("값이 Null로 들어오면 문자열 Null이 반환된다.")
        void 필드_값이_Null일때_테스트() {
            ErrorField errorField = ErrorField.of("Title", null);
            assertEquals(
                    "Null",
                    errorField.getValue()
            );
        }

        @Test
        @DisplayName("빈 값, 또는 공백이 들어오면 문자열 ``가 반환된다.")
        void 필드_값이_비어있거나_공백일때_테스트() {
            ErrorField errorFieldA = ErrorField.of("Title", " ");
            ErrorField errorFieldB = ErrorField.of("Title", "");

            assertAll(
                    () -> assertFalse("''".equals(errorFieldA.getValue())),
                    () -> assertTrue("''".equals(errorFieldB.getValue()))
            );
        }

        @Test
        @DisplayName("정상적인 값이 입력되면 그 값이 반환된다.")
        void 필드_값이_정상적으로_들어왔을때_테스트() {
            ErrorField errorField = ErrorField.of("Title", "반갑습니다.");
            assertTrue(
                    "반갑습니다.".equals(errorField.getValue())
            );
        }

        private final ErrorField errorField = ErrorField.of("Title", Strings.repeat("A", 51));

        @Test
        @DisplayName("equals와 hashCode를 재정의했다면 값으로 객체를 비교한다.")
        void euqlas_hashcode_재정의_테스트() {
            assertEquals(
                    (errorField),
                    ErrorField.of("Title", Strings.repeat("A", 51))
            );
        }

        @Test
        @DisplayName("equals와 hashCode를 재정의했다면 equals 메서드로 값을 비교할때 같은 객체로 인식한다.")
        void euqlas_재정의_테스트() {
            assertTrue(
                    (errorField).equals(ErrorField.of("Title", Strings.repeat("A", 51)))
            );
        }

        // & 조건 비교(전)
        @Test
        @DisplayName("equals와 hashCode를 재정의했다면 equals 메서드로 값을 비교할때 같은 객체로 인식한다.")
        void euqlas_재정의_And조건_추가_테스트() {
            assertFalse(
                    (errorField).equals(ErrorField.of("밥", null))
            );
        }

        // & 조건 비교(후)
        @Test
        @DisplayName("equals와 hashCode를 재정의했다면 equals 메서드로 값을 비교할때 같은 객체로 인식한다.")
        void euqlas_재정의_And조건_추가_테스트2() {
            assertFalse(
                    (errorField).equals(ErrorField.of("Title", Strings.repeat("B", 51)))
            );
        }

        @Test
        @DisplayName("같은 객체를 equals로 비교하면 true를 반환한다.")
        void 같은_객체의_equals_테스트() {
            assertTrue(
                    errorField.equals(errorField));
        }

        @Test
        @DisplayName("다른 타입의 객체라면 false가 반환된다.")
        void euqlas_instanceof_테스트() {
            assertFalse(
                    errorField.equals(1L)
            );
        }

        @Test
        @DisplayName("hashcode를 재정의했다면 같은 값이 반환된다.")
        void hashcode_재정의_테스트() {
            assertEquals(
                    errorField.hashCode(),
                    errorField.hashCode()
            );
        }

        @Test
        @DisplayName("toString을 재정의했다면 재정의한 결과에 맞는 값이 반환된다.")
        void toString_재정의_테스트() {
            String result = "field: Title, value: AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
            assertEquals(
                    result,
                    errorField.toString()
            );
        }
    }
}
