package project.lshare.membercoremodule.test.member.domain.valueobject;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import project.lshare.membercoremodule.domainmodel.member.valueobject.LastLoginIpAddress;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("회원 도메인(최근 로그인 아이피) 테스트")
class LastLoginIpAddressTest {

    @DisplayName("최근 로그인 아이피 객체를 생성할때")
    static class NestedTest {

        private String fixedLastLoginIpAddress = "192.168.10.12";

        @ParameterizedTest
        @NullAndEmptySource
        @DisplayName("값이 Null 또는 빈 값이 들어오면 UN_KNOWN이 반환된다.")
        void 최근_로그인_아이피_값이_Null_일때_테스트(String parameter) {
            assertEquals(
                    new LastLoginIpAddress(parameter).getLastLoginIpAddress(),
                    "UN_KNOWN"
            );
        }

        @Test
        @DisplayName("equals와 hashCode를 재정의했다면 값으로 객체를 비교한다.")
        void 최근_로그인_아이피_euqlas_hashcode_재정의_테스트() {
            assertEquals(
                    new LastLoginIpAddress(fixedLastLoginIpAddress),
                    new LastLoginIpAddress(fixedLastLoginIpAddress)
            );
        }

        @Test
        @DisplayName("equals와 hashCode를 재정의했다면 equals 메서드로 값을 비교할때 같은 객체로 인식한다.")
        void 최근_로그인_아이피_euqlas_재정의_테스트() {
            LastLoginIpAddress lastLoginIpAddress = new LastLoginIpAddress(fixedLastLoginIpAddress);
            assertTrue(
                    lastLoginIpAddress.equals(new LastLoginIpAddress(fixedLastLoginIpAddress))
            );
        }

        @Test
        @DisplayName("같은 객체를 equals로 비교하면 true를 반환한다.")
        void 최근_로그인_아이피_같은_객체의_equals_테스트() {
            LastLoginIpAddress lastLoginIpAddress = new LastLoginIpAddress(fixedLastLoginIpAddress);
            assertTrue(
                    lastLoginIpAddress.equals(lastLoginIpAddress)
            );
        }

        @Test
        @DisplayName("다른 타입의 객체라면 false가 반환된다.")
        void 최근_로그인_아이피_euqlas_instanceof_테스트() {
            assertFalse(
                    new LastLoginIpAddress(fixedLastLoginIpAddress).equals(1L)
            );
        }

        @Test
        @DisplayName("hashcode를 재정의 했다면 같은 값이 반환된다.")
        void 최근_로그인_아이피_hashcode_재정의_테스트() {
            assertEquals(
                    new LastLoginIpAddress(fixedLastLoginIpAddress).hashCode(),
                    new LastLoginIpAddress(fixedLastLoginIpAddress).hashCode()
            );
        }

        @Test
        @DisplayName("toString을 재정의했다면 재정의한 결과에 맞는 값이 반환된다.")
        void 최근_로그인_아이피_toString_재정의_테스트() {
            assertEquals(
                    new LastLoginIpAddress(fixedLastLoginIpAddress).toString(),
                    fixedLastLoginIpAddress
            );
        }
    }
}
