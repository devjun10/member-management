package project.lshare.membercoremodule.test.member.domain;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.lshare.membercoremodule.domainmodel.member.Role;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertAll;

@DisplayName("회원 도메인(역할) 테스트")
class RoleTest {

    @Test
    @DisplayName("정적 필드로 역할 칼럼을 생성 할 수 있다.")
    void 역할_생성_테스트() {
        assertAll(
                () -> assertNotNull(Role.NORMAL),
                () -> assertNotNull(Role.ADMIN)
        );
    }
}
