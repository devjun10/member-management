package project.lshare.membercoremodule.test.common;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.lshare.membercoremodule.common.field.Deleted;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertAll;

@DisplayName("도메인 공통 필드(삭제 유무) 테스트")
class DeletedTest {

    @Test
    @DisplayName("정적 필드로 삭제 칼럼을 생성 할 수 있다.")
    void 삭제_칼럼_생성_테스트() {
        assertAll(
                () -> assertNotNull(Deleted.TRUE),
                () -> assertNotNull(Deleted.FALSE)
        );
    }
}
