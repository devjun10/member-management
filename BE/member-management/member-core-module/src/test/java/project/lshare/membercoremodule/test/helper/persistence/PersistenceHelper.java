package project.lshare.membercoremodule.test.helper.persistence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import project.lshare.membercoremodule.persistencemodel.authentication.AuthenticationJpaEntity;
import project.lshare.membercoremodule.persistencemodel.authentication.SpringDataAuthenticationRepository;
import project.lshare.membercoremodule.persistencemodel.member.MemberJpaEntity;
import project.lshare.membercoremodule.persistencemodel.member.SpringDataMemberRepository;

@Component
public class PersistenceHelper {

    @Autowired
    private SpringDataMemberRepository memberRepository;

    @Autowired
    private SpringDataAuthenticationRepository authenticationRepository;

    public MemberJpaEntity persist(MemberJpaEntity memberJpaEntity) {
        return memberRepository.save(memberJpaEntity);
    }

    public AuthenticationJpaEntity persist(AuthenticationJpaEntity authenticationJpaEntity) {
        return authenticationRepository.save(authenticationJpaEntity);
    }

    public MemberJpaEntity findMemberById(Long memberId) {
        return memberRepository.findById(memberId).get();
    }

    public AuthenticationJpaEntity findAuthenticationById(Long authenticationId) {
        return authenticationRepository.findById(authenticationId).get();
    }
}
