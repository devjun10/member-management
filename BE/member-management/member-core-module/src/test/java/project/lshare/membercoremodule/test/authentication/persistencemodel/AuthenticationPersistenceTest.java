package project.lshare.membercoremodule.test.authentication.persistencemodel;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import project.lshare.membercoremodule.common.domain.authentication.sut.AuthenticationSut;
import project.lshare.membercoremodule.common.domain.member.fixture.MemberFixture;
import project.lshare.membercoremodule.domainmodel.authentication.Authentication;
import project.lshare.membercoremodule.persistencemodel.authentication.AuthenticationJpaEntity;
import project.lshare.membercoremodule.persistencemodel.authentication.AuthenticationMapper;
import project.lshare.membercoremodule.persistencemodel.member.MemberJpaEntity;
import project.lshare.membercoremodule.persistencemodel.member.MemberMapper;
import project.lshare.membercoremodule.test.member.persistencemodel.DomainTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static project.lshare.membercoremodule.common.domain.authentication.fixture.AuthenticationFixture.createAuthenticationJpaEntity;
import static project.lshare.membercoremodule.common.domain.authentication.fixture.AuthenticationFixture.createAuthenticationJpaEntityWithAllArgs;
import static project.lshare.membercoremodule.common.domain.member.fixture.MemberFixture.createNormalMemberJpaEntity;

@DisplayName("인증 도메인 영속모델 테스트")
class AuthenticationPersistenceTest extends DomainTest {

    @Autowired
    private AuthenticationMapper authenticationMapper;

    @Autowired
    private MemberMapper memberMapper;

    @Test
    @DisplayName("인증을 영속화하면 데이터가 저장된다.")
    void 인증_영속화_테스트() {
        // given
        MemberJpaEntity memberJpaEntity = persistenceHelper.persist(createNormalMemberJpaEntity());
        AuthenticationJpaEntity authenticationJpaEntity = createAuthenticationJpaEntity(memberJpaEntity);

        // when
        persistenceHelper.persist(authenticationJpaEntity);
        Authentication authentication = authenticationMapper.mapToDomainEntity(
                authenticationJpaEntity,
                memberMapper.mapToDomainEntity(memberJpaEntity)
        );

        AuthenticationJpaEntity convertedToJpaEntity = authenticationMapper.mapToJpaEntity(
                authentication,
                memberJpaEntity
        );

        // then
        AuthenticationSut sut = new AuthenticationSut(authentication);
        sut.shouldExist()
                .withId()
                .withMember()
                .withOAuthProvider()
                .withEmail()
                .withProfileImageUrl()
                .withNotDeleted();
    }

    @ParameterizedTest
    @ValueSource(longs = {1, 3, 5, 9})
    @DisplayName("equals와 hashCode를 재정의했다면 값으로 객체를 비교한다.")
    void 인증_영속_모델_euqlas_hashcode_재정의_테스트(Long parameter) {
        AuthenticationJpaEntity authenticationJpaEntity = createAuthenticationJpaEntityWithAllArgs(
                MemberFixture.createNormalMemberJpaEntityWithAllArgs(parameter)
        );
        AuthenticationJpaEntity sameAuthenticationJpaEntity = createAuthenticationJpaEntityWithAllArgs(
                MemberFixture.createNormalMemberJpaEntityWithAllArgs(parameter)
        );
        assertEquals(
                authenticationJpaEntity,
                sameAuthenticationJpaEntity
        );
    }

    @ParameterizedTest
    @ValueSource(longs = {1, 3, 5, 9})
    @DisplayName("equals와 hashCode를 재정의했다면 equals 메서드로 값을 비교할때 같은 객체로 인식한다.")
    void 인증_영속_모델_euqlas_재정의_테스트(Long parameter) {
        AuthenticationJpaEntity authenticationJpaEntity = createAuthenticationJpaEntityWithAllArgs(
                MemberFixture.createNormalMemberJpaEntityWithAllArgs(parameter)
        );
        AuthenticationJpaEntity sameAuthenticationJpaEntity = createAuthenticationJpaEntityWithAllArgs(
                MemberFixture.createNormalMemberJpaEntityWithAllArgs(parameter)
        );
        assertTrue(
                authenticationJpaEntity.equals(sameAuthenticationJpaEntity)
        );
    }

    @ParameterizedTest
    @ValueSource(longs = {1, 3, 5, 9})
    @DisplayName("같은 객체를 equals로 비교하면 true를 반환한다.")
    void 인증_영속_모델_같은_객체의_equals_테스트(Long parameter) {
        AuthenticationJpaEntity authenticationJpaEntity = createAuthenticationJpaEntityWithAllArgs(
                MemberFixture.createNormalMemberJpaEntityWithAllArgs(parameter)
        );
        assertTrue(
                authenticationJpaEntity.equals(authenticationJpaEntity)
        );
    }

    @ParameterizedTest
    @ValueSource(longs = {1, 3, 5, 9})
    @DisplayName("다른 타입의 객체라면 false가 반환된다.")
    void 인증_영속_모델_euqlas_instanceof_테스트(Long parameter) {
        AuthenticationJpaEntity authenticationJpaEntity = createAuthenticationJpaEntityWithAllArgs(
                MemberFixture.createNormalMemberJpaEntityWithAllArgs(parameter)
        );
        assertFalse(
                authenticationJpaEntity.equals("1L")
        );
    }

    @ParameterizedTest
    @ValueSource(longs = {1, 3, 5, 9})
    @DisplayName("hashcode를 재정의 했다면 같은 값이 반환된다.")
    void 인증_영속_모델_hashcode_재정의_테스트(Long parameter) {
        AuthenticationJpaEntity authenticationJpaEntity = createAuthenticationJpaEntityWithAllArgs(
                MemberFixture.createNormalMemberJpaEntityWithAllArgs(parameter)
        );
        assertEquals(
                authenticationJpaEntity.hashCode(),
                authenticationJpaEntity.hashCode()
        );
    }

    @ParameterizedTest
    @ValueSource(longs = {1})
    @DisplayName("toString을 재정의했다면 재정의한 결과에 맞는 값이 반환된다.")
    void 인증_영속_모델_toString_재정의_테스트(Long parameter) {
        AuthenticationJpaEntity authenticationJpaEntity = createAuthenticationJpaEntityWithAllArgs(
                MemberFixture.createNormalMemberJpaEntityWithAllArgs(parameter)
        );
        assertEquals(
                parameter.toString(),
                authenticationJpaEntity.toString()
        );
    }
}
