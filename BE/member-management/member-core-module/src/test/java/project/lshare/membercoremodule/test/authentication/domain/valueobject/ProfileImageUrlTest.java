package project.lshare.membercoremodule.test.authentication.domain.valueobject;

import org.apache.logging.log4j.util.Strings;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import project.lshare.membercoremodule.domainmodel.authentication.valueobject.ProfileImageUrl;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("인증 도메인(이미지) 테스트")
class ProfileImageUrlTest {

    @DisplayName("회원 이미지 객체를 생성할때")
    static class NestedTest {

        @ParameterizedTest
        @NullAndEmptySource
        @DisplayName("이미지 URL이 들어오지 않거나 비어있으면 IllegalArgumentException이 발생한다.")
        void 회원_이미지_Null값_또는_공백_테스트(String parameter) {
            assertThatThrownBy(() -> new ProfileImageUrl(parameter))
                    .isInstanceOf(IllegalArgumentException.class)
                    .hasMessage("프로필 이미지가 존재하지 않습니다.");
        }

        @Test
        @DisplayName("최대 주소길이를 초과하면 IllegalArgumentException이 발생한다.")
        void 회원_이미지_최대_URL_길이_테스트() {
            assertAll(
                    () -> assertThrows(
                            IllegalArgumentException.class,
                            () -> new ProfileImageUrl(Strings.repeat("A", 1001))
                    ),
                    () -> assertNotNull(
                            new ProfileImageUrl("https://www.naver.com/common/ko/img/program/default_img_640.png")
                    )
            );
        }

        // ------------------------------------------------------------------------

        @ParameterizedTest
        @ValueSource(strings = {"https://www.naver.com/common/ko/img/program/default_img_640.png"})
        @DisplayName("equals와 hashCode를 재정의했다면 값으로 객체를 비교한다.")
        void 회원_이미지_euqlas_hashcode_재정의_테스트(String parameter) {
            assertEquals(
                    new ProfileImageUrl(parameter),
                    new ProfileImageUrl(parameter)
            );
        }

        @ParameterizedTest
        @ValueSource(strings = {"https://www.naver.com/common/ko/img/program/default_img_640.png"})
        @DisplayName("equals와 hashCode를 재정의했다면 equals 메서드로 값을 비교할때 같은 객체로 인식한다.")
        void 회원_이미지_euqlas_재정의_테스트(String parameter) {
            ProfileImageUrl imageUrl = new ProfileImageUrl(parameter);
            assertTrue(
                    imageUrl.equals(new ProfileImageUrl(parameter))
            );
        }

        @ParameterizedTest
        @ValueSource(strings = {"https://www.naver.com/common/ko/img/program/default_img_640.png"})
        @DisplayName("같은 객체를 equals로 비교하면 true를 반환한다.")
        void 회원_이미지_같은_객체의_equals_테스트(String parameter) {
            ProfileImageUrl imageUrl = new ProfileImageUrl(parameter);
            assertTrue(
                    imageUrl.equals(imageUrl)
            );
        }

        @ParameterizedTest
        @ValueSource(strings = {"https://www.naver.com/common/ko/img/program/default_img_640.png"})
        @DisplayName("다른 타입의 객체라면 false가 반환된다.")
        void 회원_이미지_euqlas_instanceof_테스트(String parameter) {
            assertFalse(
                    new ProfileImageUrl(parameter).equals(1L)
            );
        }

        @ParameterizedTest
        @ValueSource(strings = {"https://www.naver.com/common/ko/img/program/default_img_640.png"})
        @DisplayName("hashcode를 재정의 했다면 같은 값이 반환된다.")
        void 회원_이미지_hashcode_재정의_테스트(String parameter) {
            assertEquals(
                    new ProfileImageUrl(parameter).hashCode(),
                    new ProfileImageUrl(parameter).hashCode()
            );
        }

        @ParameterizedTest
        @ValueSource(strings = {"https://www.naver.com/common/ko/img/program/default_img_640.png"})
        @DisplayName("toString을 재정의했다면 재정의한 결과에 맞는 값이 반환된다.")
        void 회원_이미지_toString_재정의_테스트(String parameter) {
            assertEquals(
                    parameter,
                    new ProfileImageUrl(parameter).toString()
            );
        }
    }
}
