package project.lshare.membercoremodule.common.domain.member.sut;

import org.junit.jupiter.api.Assertions;
import project.lshare.membercoremodule.common.field.Deleted;
import project.lshare.membercoremodule.domainmodel.member.Member;
import project.lshare.membercoremodule.domainmodel.member.Role;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class MemberSut {

    private final Member member;

    public MemberSut(Member member) {
        this.member = member;
    }

    public MemberSut shouldExist() {
        Assertions.assertNotNull(member);
        return this;
    }

    public MemberSut withId() {
        Assertions.assertNotNull(member.getMemberId());
        return this;
    }

    public MemberSut withNickname() {
        Assertions.assertNotNull(member.getNickname());
        return this;
    }

    public MemberSut withProfileImageUrl() {
        Assertions.assertNotNull(member.getProfileImageUrl());
        return this;
    }

    public MemberSut withUserAgent() {
        Assertions.assertNotNull(member.getUserAgent());
        return this;
    }

    public MemberSut withLastLoginIpAddress() {
        Assertions.assertNotNull(member.getLastLoginIpAddress());
        return this;
    }

    public MemberSut withNormalRole() {
        assertEquals(Role.NORMAL, member.getRole());
        return this;
    }

    public MemberSut withAdminRole() {
        assertEquals(Role.ADMIN, member.getRole());
        return this;
    }

    public MemberSut withDistrict() {
        assertNotNull(member.getDistrict());
        return this;
    }

    public MemberSut withNotDeleted() {
        assertEquals(Deleted.FALSE, member.getDeleted());
        return this;
    }
}
