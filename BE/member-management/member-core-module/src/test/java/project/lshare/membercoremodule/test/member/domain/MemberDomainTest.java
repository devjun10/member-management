package project.lshare.membercoremodule.test.member.domain;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import project.lshare.membercoremodule.common.domain.member.fixture.MemberFixture;
import project.lshare.membercoremodule.common.field.Deleted;
import project.lshare.membercoremodule.domainmodel.member.Member;
import project.lshare.membercoremodule.domainmodel.member.valueobject.Nickname;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("회원 도메인 테스트")
class MemberDomainTest {

    @Test
    @DisplayName("올바른 값을 입력하면 닉네임이 변경된다.")
    void 회원_도메인_닉네임_변경_테스트() {
        // given
        Member member = MemberFixture.createMemberDomainWithAllArgs(1L);
        Nickname expected = new Nickname("hello-world");

        // when
        member.changeNickname(new Nickname("hello-world"));

        // then
        assertEquals(
                expected.getNickname(),
                member.getNickname()
        );
    }

    @Test
    @DisplayName("회원 탈퇴를 하면 Deleted 값이 TRUE가 된다.")
    void 회원_도메인_회원_탈퇴_테스트() {
        // given
        Member member = MemberFixture.createMemberDomainWithAllArgs(1L);
        Deleted expected = Deleted.TRUE;

        // when
        member.withdrawlMembership();

        // then
        assertEquals(
                expected,
                member.getDeleted()
        );
    }

    // ------------------------------------------------------------------------

    @ParameterizedTest
    @ValueSource(longs = {1, 3, 5, 9})
    @DisplayName("equals와 hashCode를 재정의했다면 값으로 객체를 비교한다.")
    void 회원_도메인_euqlas_hashcode_재정의_테스트(Long parameter) {
        Member member = MemberFixture.createMemberDomainWithAllArgs(parameter);
        Member sameMember = MemberFixture.createMemberDomainWithAllArgs(parameter);
        assertEquals(
                member,
                sameMember
        );
    }

    @ParameterizedTest
    @ValueSource(longs = {1, 3, 5, 9})
    @DisplayName("equals와 hashCode를 재정의했다면 equals 메서드로 값을 비교할때 같은 객체로 인식한다.")
    void 회원_도메인_euqlas_재정의_테스트(Long parameter) {
        Member member = MemberFixture.createMemberDomainWithAllArgs(parameter);
        assertTrue(
                member.equals(MemberFixture.createMemberDomainWithAllArgs(parameter))
        );
    }

    @ParameterizedTest
    @ValueSource(longs = {1, 3, 5, 9})
    @DisplayName("같은 객체를 equals로 비교하면 true를 반환한다.")
    void 회원_도메인_같은_객체의_equals_테스트(Long parameter) {
        Member member = MemberFixture.createMemberDomainWithAllArgs(parameter);
        assertTrue(
                member.equals(member)
        );
    }

    @ParameterizedTest
    @ValueSource(longs = {1, 3, 5, 9})
    @DisplayName("다른 타입의 객체라면 false가 반환된다.")
    void 회원_도메인_euqlas_instanceof_테스트(Long parameter) {
        Member member = MemberFixture.createMemberDomainWithAllArgs(parameter);
        assertFalse(
                member.equals("1L")
        );
    }

    @ParameterizedTest
    @ValueSource(longs = {1, 3, 5, 9})
    @DisplayName("hashcode를 재정의 했다면 같은 값이 반환된다.")
    void 회원_도메인_hashcode_재정의_테스트(Long parameter) {
        Member member = MemberFixture.createMemberDomainWithAllArgs(parameter);
        assertEquals(
                member.hashCode(),
                member.hashCode()
        );
    }

    @ParameterizedTest
    @ValueSource(longs = {1, 3, 5, 9})
    @DisplayName("toString을 재정의했다면 재정의한 결과에 맞는 값이 반환된다.")
    void 회원_도메인_toString_재정의_테스트(Long parameter) {
        Member member = MemberFixture.createMemberDomainWithAllArgs(parameter);
        assertEquals(
                parameter.toString(),
                member.toString()
        );
    }
}
