package project.lshare.membercoremodule.test.authentication.domain.valueobject;

import org.apache.logging.log4j.util.Strings;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import project.lshare.membercoremodule.domainmodel.authentication.valueobject.Email;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("인증 도메인(이메일) 테스트")
class EmailTest {

    @DisplayName("이메일 객체를 생성할때")
    static class NestedTest {

        @ParameterizedTest
        @ValueSource(strings = {"a @gmail.com", "abcer.com", " .com", "@.com"})
        @DisplayName("올바르지 않은 메일 양식을 입력하면 IllegalArgumentException이 발생한다.")
        void 올바르지_않은_양식_이메일_테스트() {
            assertThrows(
                    IllegalArgumentException.class,
                    () -> new Email(Strings.repeat("A", 40))
            );
        }

        @ParameterizedTest
        @NullAndEmptySource
        @DisplayName("값이 Null이거나 비어있다면 IllegalArgumentException이 발생한다.")
        void 이메일_아이디_값이_Null_또는_빈_값_일때_테스트(String parameter) {
            assertThrows(
                    IllegalArgumentException.class,
                    () -> new Email(parameter)
            );
        }

        // ------------------------------------------------------------------------

        @ParameterizedTest
        @ValueSource(strings = {"devjun@gmail.com"})
        @DisplayName("equals와 hashCode를 재정의했다면 값으로 객체를 비교한다.")
        void 이메일_아이디_euqlas_hashcode_재정의_테스트(String parameter) {
            assertEquals(
                    new Email(parameter),
                    new Email(parameter)
            );
        }

        @ParameterizedTest
        @ValueSource(strings = {"devjun@gmail.com"})
        @DisplayName("equals와 hashCode를 재정의했다면 equals 메서드로 값을 비교할때 같은 객체로 인식한다.")
        void 이메일_아이디_euqlas_재정의_테스트(String parameter) {
            Email email = new Email(parameter);
            assertTrue(
                    email.equals(new Email(parameter))
            );
        }

        @ParameterizedTest
        @ValueSource(strings = {"devjun@gmail.com"})
        @DisplayName("같은 객체를 equals로 비교하면 true를 반환한다.")
        void 이메일_아이디_같은_객체의_equals_테스트(String parameter) {
            Email email = new Email(parameter);
            assertTrue(
                    email.equals(email)
            );
        }

        @ParameterizedTest
        @ValueSource(strings = {"devjun@gmail.com"})
        @DisplayName("다른 타입의 객체라면 false가 반환된다.")
        void 이메일_아이디_euqlas_instanceof_테스트(String parameter) {
            assertFalse(
                    new Email(parameter).equals(1L)
            );
        }

        @ParameterizedTest
        @ValueSource(strings = {"devjun@gmail.com"})
        @DisplayName("hashcode를 재정의했다면 같은 값이 반환된다.")
        void 이메일_아이디_hashcode_재정의_테스트(String parameter) {
            assertEquals(
                    new Email(parameter).hashCode(),
                    new Email(parameter).hashCode()
            );
        }

        @ParameterizedTest
        @ValueSource(strings = {"devjun@gmail.com"})
        @DisplayName("toString을 재정의했다면 재정의한 결과에 맞는 값이 반환된다.")
        void 이메일_아이디_toString_재정의_테스트(String parameter) {
            assertEquals(
                    parameter,
                    new Email(parameter).toString()
            );
        }
    }
}
