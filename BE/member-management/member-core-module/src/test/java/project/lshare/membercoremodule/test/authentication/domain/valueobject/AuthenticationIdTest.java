package project.lshare.membercoremodule.test.authentication.domain.valueobject;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import project.lshare.membercoremodule.domainmodel.authentication.valueobject.AuthenticationId;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("인증 도메인(아이디) 테스트")
class AuthenticationIdTest {

    @DisplayName("인증 아이디 객체를 생성할때")
    static class NestedTest {

        @NullSource
        @ParameterizedTest
        @DisplayName("값이 Null 또는 비어있다면 IllegalArgumentException이 발생한다.")
        void 인증_아이디_값이_Null_또는_공백일때_테스트(Long parameter) {
            assertThrows(
                    IllegalArgumentException.class,
                    () -> new AuthenticationId(parameter)
            );
        }

        @ParameterizedTest
        @ValueSource(longs = {0, -1, -2, -3})
        @DisplayName("값이 Null 또는 비어있다면 IllegalArgumentException이 발생한다.")
        void 인증_아이디_값이_0이하_음수_일때_테스트(Long parameter) {
            assertThrows(
                    IllegalArgumentException.class,
                    () -> new AuthenticationId(parameter)
            );
        }

        // ------------------------------------------------------------------------

        @ParameterizedTest
        @ValueSource(longs = {1, 3, 5, 9})
        @DisplayName("equals와 hashCode를 재정의했다면 값으로 객체를 비교한다.")
        void 인증_아이디_euqlas_hashcode_재정의_테스트(Long parameter) {
            assertEquals(
                    new AuthenticationId(parameter),
                    new AuthenticationId(parameter)
            );
        }

        @ParameterizedTest
        @ValueSource(longs = {1, 3, 5, 9})
        @DisplayName("equals와 hashCode를 재정의했다면 equals 메서드로 값을 비교할때 같은 객체로 인식한다.")
        void 인증_아이디_euqlas_재정의_테스트(Long parameter) {
            AuthenticationId authenticationId = new AuthenticationId(parameter);
            assertTrue(
                    authenticationId.equals(new AuthenticationId(parameter))
            );
        }

        @ParameterizedTest
        @ValueSource(longs = {1, 3, 5, 9})
        @DisplayName("같은 객체를 equals로 비교하면 true를 반환한다.")
        void 인증_아이디_같은_객체의_equals_테스트(Long parameter) {
            AuthenticationId authenticationId = new AuthenticationId(parameter);
            assertTrue(
                    authenticationId.equals(authenticationId)
            );
        }

        @ParameterizedTest
        @ValueSource(longs = {1, 3, 5, 9})
        @DisplayName("다른 타입의 객체라면 false가 반환된다.")
        void 인증_아이디_euqlas_instanceof_테스트(Long parameter) {
            assertFalse(
                    new AuthenticationId(parameter).equals("1L")
            );
        }

        @ParameterizedTest
        @ValueSource(longs = {1, 3, 5, 9})
        @DisplayName("hashcode를 재정의 했다면 같은 값이 반환된다.")
        void 인증_아이디_hashcode_재정의_테스트(Long parameter) {
            assertEquals(
                    new AuthenticationId(parameter).hashCode(),
                    new AuthenticationId(parameter).hashCode()
            );
        }

        @ParameterizedTest
        @ValueSource(longs = {1, 3, 5, 9})
        @DisplayName("toString을 재정의했다면 재정의한 결과에 맞는 값이 반환된다.")
        void 인증_아이디_toString_재정의_테스트(Long parameter) {
            assertEquals(
                    parameter.toString(),
                    new AuthenticationId(parameter).toString()
            );
        }
    }
}
