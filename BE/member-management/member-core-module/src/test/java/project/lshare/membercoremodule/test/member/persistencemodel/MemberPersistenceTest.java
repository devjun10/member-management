package project.lshare.membercoremodule.test.member.persistencemodel;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import project.lshare.membercoremodule.common.domain.member.sut.MemberSut;
import project.lshare.membercoremodule.common.mapper.BusinessException;
import project.lshare.membercoremodule.domainmodel.member.Member;
import project.lshare.membercoremodule.domainmodel.member.valueobject.Nickname;
import project.lshare.membercoremodule.persistencemodel.member.MemberJpaEntity;
import project.lshare.membercoremodule.persistencemodel.member.MemberMapper;
import project.lshare.membercoremodule.persistencemodel.member.MemberPersistenceAdapter;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static project.lshare.membercoremodule.common.domain.member.fixture.MemberFixture.createNormalMemberJpaEntityWithAllArgs;

@DisplayName("회원 도메인 영속모델 테스트")
class MemberPersistenceTest extends DomainTest {

    @Autowired
    private MemberPersistenceAdapter adapter;

    @Autowired
    private MemberMapper mapper;

    @Test
    @DisplayName("회원을 영속화하면 데이터가 저장된다.")
    void 회원_영속화_테스트() {
        // given
        MemberJpaEntity newMember = persistenceHelper.persist(createNormalMemberJpaEntityWithAllArgs());

        // when, then
        Member memberJpaEntity = adapter.findMemberById(newMember.getMemberId());
        MemberSut sut = new MemberSut(memberJpaEntity);

        sut.shouldExist()
                .withId()
                .withNickname()
                .withProfileImageUrl()
                .withUserAgent()
                .withLastLoginIpAddress()
                .withNormalRole()
                .withDistrict()
                .withNotDeleted();
    }

    @Test
    @DisplayName("올바른 값을 입력하면 회원 닉네임을 변경할 수 있다.")
    void 회원_닉네임_변경_테스트() {
        // given
        MemberJpaEntity newMember = persistenceHelper.persist(createNormalMemberJpaEntityWithAllArgs());
        Member member = adapter.findMemberById(newMember.getMemberId());
        String expected = "hello-world";

        // when
        member.changeNickname(new Nickname("hello-world"));
        adapter.update(member);

        // then
        Member updatedMember = adapter.findMemberById(member.getMemberId());
        assertEquals(expected, updatedMember.getNickname());
    }

    @Test
    @DisplayName("회원 탈퇴를 하면 정보가 조회되지 않는다..")
    void 회원_탈퇴_테스트() {
        // given
        MemberJpaEntity newMember = persistenceHelper.persist(createNormalMemberJpaEntityWithAllArgs());
        Member member = adapter.findMemberById(newMember.getMemberId());

        // when
        member.withdrawlMembership();
        MemberJpaEntity convertedToJpaEntity = mapper.mapToJpaEntity(member);
        adapter.withdrawlMembership(convertedToJpaEntity);

        // then
        assertThatThrownBy(() -> adapter.findMemberById(member.getMemberId()))
                .isInstanceOf(BusinessException.class)
                .hasMessage("회원을 찾을 수 없습니다.");
    }

    @ParameterizedTest
    @ValueSource(longs = {1, 3, 5, 9})
    @DisplayName("equals와 hashCode를 재정의했다면 값으로 객체를 비교한다.")
    void 회원_영속_모델_euqlas_hashcode_재정의_테스트(Long parameter) {
        MemberJpaEntity member = createNormalMemberJpaEntityWithAllArgs(parameter);
        MemberJpaEntity sameMember = createNormalMemberJpaEntityWithAllArgs(parameter);
        assertEquals(
                member,
                sameMember
        );
    }

    @ParameterizedTest
    @ValueSource(longs = {1, 3, 5, 9})
    @DisplayName("equals와 hashCode를 재정의했다면 equals 메서드로 값을 비교할때 같은 객체로 인식한다.")
    void 회원_영속_모델_euqlas_재정의_테스트(Long parameter) {
        MemberJpaEntity member = createNormalMemberJpaEntityWithAllArgs(parameter);
        assertTrue(
                member.equals(createNormalMemberJpaEntityWithAllArgs(parameter))
        );
    }

    @ParameterizedTest
    @ValueSource(longs = {1, 3, 5, 9})
    @DisplayName("같은 객체를 equals로 비교하면 true를 반환한다.")
    void 회원_영속_모델_같은_객체의_equals_테스트(Long parameter) {
        MemberJpaEntity member = createNormalMemberJpaEntityWithAllArgs(parameter);
        assertTrue(
                member.equals(member)
        );
    }

    @ParameterizedTest
    @ValueSource(longs = {1, 3, 5, 9})
    @DisplayName("다른 타입의 객체라면 false가 반환된다.")
    void 회원_영속_모델_euqlas_instanceof_테스트(Long parameter) {
        MemberJpaEntity member = createNormalMemberJpaEntityWithAllArgs(parameter);
        assertFalse(
                member.equals("1L")
        );
    }

    @ParameterizedTest
    @ValueSource(longs = {1, 3, 5, 9})
    @DisplayName("hashcode를 재정의 했다면 같은 값이 반환된다.")
    void 회원_영속_모델_hashcode_재정의_테스트(Long parameter) {
        MemberJpaEntity member = createNormalMemberJpaEntityWithAllArgs(parameter);
        assertEquals(
                member.hashCode(),
                member.hashCode()
        );
    }

    @ParameterizedTest
    @ValueSource(longs = {1, 3, 5, 9})
    @DisplayName("toString을 재정의했다면 재정의한 결과에 맞는 값이 반환된다.")
    void 회원_영속_모델_toString_재정의_테스트(Long parameter) {
        MemberJpaEntity member = createNormalMemberJpaEntityWithAllArgs(parameter);
        assertEquals(
                parameter.toString(),
                member.toString()
        );
    }
}
