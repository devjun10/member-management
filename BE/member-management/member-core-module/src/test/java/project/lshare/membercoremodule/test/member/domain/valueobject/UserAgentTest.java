package project.lshare.membercoremodule.test.member.domain.valueobject;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import project.lshare.membercoremodule.domainmodel.member.valueobject.UserAgent;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("회원 도메인(User-Agent) 테스트")
class UserAgentTest {

    @DisplayName("User-Agent 객체를 생성할때")
    static class NestedTest {

        private String fixedUserAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/110.0.0.0 Safari/537.36";

        @Test
        @DisplayName("Null 값이 들어오면 빈 칸(\"\")으로 치환된다.")
        void UserAgent_값이_Null_일때_테스트() {
            assertEquals(
                    "",
                    new UserAgent(null).getUserAgent()
            );
        }

        @Test
        @DisplayName("equals와 hashCode를 재정의했다면 값으로 객체를 비교한다.")
        void UserAgent_euqlas_hashcode_재정의_테스트() {
            assertEquals(
                    new UserAgent(fixedUserAgent),
                    new UserAgent(fixedUserAgent)
            );
        }

        @Test
        @DisplayName("equals와 hashCode를 재정의했다면 equals 메서드로 값을 비교할때 같은 객체로 인식한다.")
        void UserAgent_euqlas_재정의_테스트() {
            UserAgent userAgent = new UserAgent(fixedUserAgent);
            assertTrue(
                    userAgent.equals(new UserAgent(fixedUserAgent))
            );
        }

        @Test
        @DisplayName("같은 객체를 equals로 비교하면 true를 반환한다.")
        void UserAgent_같은_객체의_equals_테스트() {
            UserAgent userAgent = new UserAgent(fixedUserAgent);
            assertTrue(
                    userAgent.equals(userAgent)
            );
        }

        @Test
        @DisplayName("다른 타입의 객체라면 false가 반환된다.")
        void UserAgent_euqlas_instanceof_테스트() {
            assertFalse(
                    new UserAgent(fixedUserAgent).equals(1L)
            );
        }

        @Test
        @DisplayName("hashcode를 재정의 했다면 같은 값이 반환된다.")
        void UserAgent_hashcode_재정의_테스트() {
            assertEquals(
                    new UserAgent(fixedUserAgent).hashCode(),
                    new UserAgent(fixedUserAgent).hashCode()
            );
        }

        @Test
        @DisplayName("toString을 재정의했다면 재정의한 결과에 맞는 값이 반환된다.")
        void UserAgent_toString_재정의_테스트() {
            assertEquals(
                    fixedUserAgent,
                    new UserAgent(fixedUserAgent).toString()
            );
        }
    }
}
