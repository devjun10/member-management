![Untitled-5-01](https://gitlab.com/devjun10/member-management/uploads/8043a4940eea1b629042477b66a1ac98/member-management-01.png)
<div align="center">

💁 회원 관리시스템으로 회원을 관리 해보자!<br>
프로젝트에 대한 상세한 내용은 [기술 블로그](https://devjun10.github.io/managements/)에 정리되어 있습니다.

[![Release](https://img.shields.io/badge/-%F0%9F%92%AB%20Tech_Blog%20-blue)](https://devjun10.github.io/managements/)
[![Release](https://img.shields.io/badge/-%F0%9F%93%9A%20API_Document-green)]()

`# 회원` `# 등급` `# 회원관리` `# 효율적 조회` 

`# 관리` `# Command` `# Query`

</div>

<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

## 📚 목차

#### &nbsp;&nbsp;&nbsp;&nbsp; - [💻 프로그래밍 요구사항](#프로그래밍-요구사항) <br/>

#### &nbsp;&nbsp;&nbsp;&nbsp; - [📚 기능 요구사항](#기능-요구사항) <br/>

#### &nbsp;&nbsp;&nbsp;&nbsp; - [✨ 사용된 기술 스택](#사용된-기술-스택) <br/>

#### &nbsp;&nbsp;&nbsp;&nbsp; - [☁ 인프라 아키텍처](#인프라-아키텍처) <br/>

#### &nbsp;&nbsp;&nbsp;&nbsp; - [💭 CICD 아키텍처](#CICD-아키텍처) <br/>

#### &nbsp;&nbsp;&nbsp;&nbsp; - [👬 팀원 소개](#팀원-소개) <br/>

<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

## 프로그래밍 요구사항

&nbsp;&nbsp;&nbsp;&nbsp; - 멀티모듈로 프로젝트를 구성한다. <br/>
&nbsp;&nbsp;&nbsp;&nbsp; - 패키지 간 의존관계가 잘못되면 빌드가 실패하도록 한다. <br/>
&nbsp;&nbsp;&nbsp;&nbsp; - 테스트 커버리지는 70% 이상을 유지한다. <br/>
&nbsp;&nbsp;&nbsp;&nbsp; - 엔티티(Entity)와 도메인 모델(Domain Model)을 구분한다. <br/>
&nbsp;&nbsp;&nbsp;&nbsp; - 가능한 모든 값들은 값 객체로 포장한다.<br/>
&nbsp;&nbsp;&nbsp;&nbsp; - 롬복은 가능한 사용하지 않는다.  <br/>
&nbsp;&nbsp;&nbsp;&nbsp; - 롬복을 사용한다면 @RequiredArgsConstructor정도만 사용한다. <br/>
&nbsp;&nbsp;&nbsp;&nbsp; - 회원가입은 스프링 시큐리티를 사용해 구현한다. <br/>
&nbsp;&nbsp;&nbsp;&nbsp; - 인증/인가는 방식은 쿠키/세션 및 토큰 중 자유롭게 선택한다. <br/>
&nbsp;&nbsp;&nbsp;&nbsp; - OAuth를 사용할 경우 Google, Github 중 하나를 선택한다. <br/>
&nbsp;&nbsp;&nbsp;&nbsp; - 조회를 효율적으로 할 수 있도록 다양한 방법을 고민한다. <br/>
&nbsp;&nbsp;&nbsp;&nbsp; - 운영에 필요한 각종 지표를 수집해 모니터링한다. <br/>

<br/>
<br/>
<br/>
<br/>
<br/>

## 기능 요구사항

&nbsp;&nbsp;&nbsp;&nbsp; - 관리자는 회원의 등급을 조정할 수 있다. <br/>
&nbsp;&nbsp;&nbsp;&nbsp; - 관리자는 회원의 서비스 이용유무를 선택할 수 있다. <br/>
&nbsp;&nbsp;&nbsp;&nbsp; - 해외 아이피는 차단한다. <br/>
&nbsp;&nbsp;&nbsp;&nbsp; - 회원의 로그인 시간, 사용 기기 등과 같은 사용자 정보를 수집한다. <br/>
&nbsp;&nbsp;&nbsp;&nbsp; - 일정량의 사용자 요청만 받을 수 있도록 유량을 제어(Rate Limiter)한다. <br/>
&nbsp;&nbsp;&nbsp;&nbsp; - 일정 기간동안 정해진 횟수를 초과해 요청하면 아이피를 3일간 정지시킨다. <br/>
&nbsp;&nbsp;&nbsp;&nbsp; - 트랜잭션의 시작과 종료 시점을 기록한다. <br/>
&nbsp;&nbsp;&nbsp;&nbsp; - 모든 요청과 응답은 로그를 남겨 확인할 수 있도록 한다. <br/>
&nbsp;&nbsp;&nbsp;&nbsp; - 프로필 이미지를 삭제했을때 사용자가 이전 URL을 알더라도 더 이상 접근할 수 없도록 한다. <br/>

<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

## 사용된 기술 스택

![image](https://gitlab.com/devjun10/member-management/uploads/ad9fd6ec05b9f2d2087f482bdf4a25ce/skill-stack-01.png)

<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

## 인프라 아키텍처

![image](https://gitlab.com/devjun10/member-management/uploads/ded1afc0f695e174cd31e27dffef54f3/infra-arc-01.png)
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

## CICD 아키텍처

![image](https://gitlab.com/devjun10/member-management/uploads/ddaf2f42f39e44a27eac4b4a200c2d44/cicdpng-01.png)

<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

## 팀원 소개

#### [@Jun](https://github.com/devjun10), &nbsp;  [@James](https://github.com/jangh-lee)


![image](https://gitlab.com/devjun10/member-management/uploads/bb8e5960aa62a0af97a2f9d822717efe/Untitled-32-01.png)
<br/>
<br/>
