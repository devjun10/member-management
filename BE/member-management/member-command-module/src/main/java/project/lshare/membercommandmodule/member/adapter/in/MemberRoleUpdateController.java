package project.lshare.membercommandmodule.member.adapter.in;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import project.lshare.membercommandmodule.common.annotation.business.AdminOnly;
import project.lshare.membercommandmodule.common.annotation.business.LoginUser;
import project.lshare.membercommandmodule.common.response.ApiResponse;
import project.lshare.membercommandmodule.member.adapter.in.request.RoleUpdateRequest;
import project.lshare.membercommandmodule.member.application.port.in.AdminCommand;
import project.lshare.membercoremodule.domainmodel.authentication.valueobject.AuthenticatedMember;

@RestController
@RequestMapping("/api/command/admin")
public class MemberRoleUpdateController {

    private final AdminCommand adminCommand;

    public MemberRoleUpdateController(AdminCommand adminCommand) {
        this.adminCommand = adminCommand;
    }

    @AdminOnly
    @PostMapping(path = "/role")
    public ApiResponse<Void> changeMemberRole(
            @LoginUser AuthenticatedMember authenticatedMember,
            @RequestBody RoleUpdateRequest request
    ) {
        adminCommand.changeRole(authenticatedMember.getMemberId(), request.getMemberId(), request.getRole());
        return ApiResponse.ok();
    }
}
