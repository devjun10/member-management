package project.lshare.membercommandmodule.member.adapter.in.request;

import com.sun.istack.NotNull;

public class MemberUpdateRequest {

    @NotNull
    private String nickname;

    private MemberUpdateRequest() {
    }

    public MemberUpdateRequest(String nickname) {
        this.nickname = nickname;
    }

    public String getNickname() {
        return nickname;
    }

    @Override
    public String toString() {
        return nickname;
    }
}
