package project.lshare.membercommandmodule.member.adapter.in.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import project.lshare.membercoremodule.common.mapper.BusinessException;
import project.lshare.membercoremodule.domainmodel.authentication.valueobject.AuthenticatedMember;

import static project.lshare.membercoremodule.domainmodel.member.MemberTypeException.INVALID_ADMIN_ACCESS_EXCEPTION;

@Aspect
@Component
public class RoleCheckAspect {

    @Pointcut("@annotation(project.lshare.membercommandmodule.common.annotation.business.AdminOnly)")
    private void validateRole() {
    }

    @Before("validateRole()")
    public void checkRole(JoinPoint joinPoint) {
        Object[] args = joinPoint.getArgs();
        for (Object arg : args) {
            if (arg != null && arg instanceof AuthenticatedMember) {
                AuthenticatedMember authenticatedMember = (AuthenticatedMember) arg;
                if (authenticatedMember.isAdmin()) {
                    return;
                }
            }
        }
        throw BusinessException.of(INVALID_ADMIN_ACCESS_EXCEPTION);
    }
}
