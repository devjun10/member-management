package project.lshare.membercommandmodule.member.application.port.in;

import project.lshare.membercoremodule.domainmodel.member.valueobject.Nickname;

public interface MemberUpdateCommand {

    void changeNickName(Long memberId, Nickname nickname);

}
