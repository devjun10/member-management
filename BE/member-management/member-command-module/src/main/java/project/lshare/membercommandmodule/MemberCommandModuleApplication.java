package project.lshare.membercommandmodule;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import project.lshare.membercoremodule.MemberCoreModuleApplication;

@SpringBootApplication
@Import(MemberCoreModuleApplication.class)
public class MemberCommandModuleApplication {

    public static void main(String[] args) {
        SpringApplication.run(MemberCommandModuleApplication.class, args);
    }

}
