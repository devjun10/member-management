package project.lshare.membercommandmodule.member.application.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import project.lshare.membercommandmodule.member.application.port.in.MemberUpdateCommand;
import project.lshare.membercoremodule.domainmodel.member.Member;
import project.lshare.membercoremodule.domainmodel.member.valueobject.Nickname;
import project.lshare.membercoremodule.persistencemodel.member.MemberPersistenceAdapter;

@Service
public class MemberInformationUpdateService implements MemberUpdateCommand{

    private final MemberPersistenceAdapter memberPersistenceAdapter;

    public MemberInformationUpdateService(MemberPersistenceAdapter memberPersistenceAdapter) {
        this.memberPersistenceAdapter = memberPersistenceAdapter;
    }

    @Override
    @Transactional
    public void changeNickName(
            Long memberId,
            Nickname nickname
    ) {
        Member member = memberPersistenceAdapter.findMemberById(memberId);
        member.changeNickname(nickname);
        memberPersistenceAdapter.update(member);
    }
}
