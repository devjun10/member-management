package project.lshare.membercommandmodule.common.response;

import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

public final class ApiResponse<T> {

    private static final String SUCCESS = "SUCCESS";

    private final LocalDateTime eventTime;
    private HttpStatus status;
    private final int code;
    private final String message;
    private CommonInformationResponse common;
    private T data;

    private ApiResponse(T data) {
        this.eventTime = LocalDateTime.now();
        this.status = HttpStatus.OK;
        this.message = SUCCESS;
        this.code = HttpStatus.OK.value();
        this.data = data;
    }

    private ApiResponse(HttpStatus status, T data) {
        this.eventTime = LocalDateTime.now();
        this.status = status;
        this.message = SUCCESS;
        this.code = HttpStatus.OK.value();
        this.data = data;
    }

    public static <T> ApiResponse<T> of(T data) {
        return new ApiResponse<>(data);
    }

    public static <T> ApiResponse<T> ok() {
        return new ApiResponse<>(null);
    }

    public static <T> ApiResponse<T> of(HttpStatus httpStatus, T data) {
        return new ApiResponse<>
                (
                        httpStatus,
                        data
                );
    }

    public HttpStatus getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public int getCode() {
        return code;
    }

    public LocalDateTime getEventTime() {
        return eventTime;
    }

    public CommonInformationResponse getCommon() {
        return common;
    }

    public T getData() {
        return data;
    }

    @Override
    public String toString() {
        return String
                .format("EventTime: %s, Status: %s, Code: %s, Message: %s, Language: %s, Platform: %s, Data: %s",
                        eventTime,
                        status,
                        code,
                        message,
                        common.getLang(),
                        common.getPlatForm(),
                        data
                );
    }
}
