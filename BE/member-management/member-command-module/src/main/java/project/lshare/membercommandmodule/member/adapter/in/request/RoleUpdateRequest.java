package project.lshare.membercommandmodule.member.adapter.in.request;

import project.lshare.membercoremodule.domainmodel.member.Role;

public class RoleUpdateRequest {

    private Long memberId;
    private Role role;

    private RoleUpdateRequest() {
    }

    public RoleUpdateRequest(
            Long memberId,
            String role
    ) {
        this.memberId = memberId;
        this.role = Role.valueOf(role);
    }

    public Long getMemberId() {
        return memberId;
    }

    public Role getRole() {
        return role;
    }

    @Override
    public String toString() {
        return String.format("MemberId: %s, Role: %s", memberId, role);
    }
}
