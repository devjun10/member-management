package project.lshare.membercommandmodule.member.adapter.in;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import project.lshare.membercommandmodule.common.annotation.business.LoginUser;
import project.lshare.membercommandmodule.common.response.ApiResponse;
import project.lshare.membercommandmodule.member.adapter.in.request.MemberUpdateRequest;
import project.lshare.membercommandmodule.member.application.port.in.MemberUpdateCommand;
import project.lshare.membercommandmodule.member.application.service.MemberInformationUpdateService;
import project.lshare.membercoremodule.domainmodel.authentication.valueobject.AuthenticatedMember;
import project.lshare.membercoremodule.domainmodel.member.valueobject.Nickname;

@RestController
@RequestMapping("/api/command/members")
public class MemberNicknameUpdateController {

    private final MemberUpdateCommand memberUpdateCommand;

    public MemberNicknameUpdateController(MemberInformationUpdateService memberUpdateCommand) {
        this.memberUpdateCommand = memberUpdateCommand;
    }

    @PostMapping("/information")
    public ApiResponse<Void> updateMemberInformation(
            @LoginUser AuthenticatedMember authenticatedMember,
            @RequestBody MemberUpdateRequest request
    ) {
        memberUpdateCommand.changeNickName(authenticatedMember.getMemberId(), new Nickname(request.getNickname()));
        return ApiResponse.ok();
    }
}
