package project.lshare.membercommandmodule.member.application.port.in;

import project.lshare.membercoremodule.domainmodel.member.Role;

public interface AdminCommand {

    void changeRole(Long adminId, Long memberId, Role role);

}
