package project.lshare.membercommandmodule.member.adapter.in.argumentresolver;

import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;
import project.lshare.membercommandmodule.common.annotation.business.LoginUser;
import project.lshare.membercoremodule.common.mapper.BusinessException;
import project.lshare.membercoremodule.domainmodel.authentication.valueobject.AuthenticatedMember;
import project.lshare.membercoremodule.domainmodel.member.Role;

import javax.servlet.http.HttpServletRequest;

import static project.lshare.membercoremodule.domainmodel.member.MemberTypeException.MEMBER_NOT_FOUND_EXCEPTION;

@Component
public class MemberInformationArgumentResolver implements HandlerMethodArgumentResolver {

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.hasParameterAnnotation(LoginUser.class);
    }

    @Override
    public Object resolveArgument(
            MethodParameter parameter,
            ModelAndViewContainer mavContainer,
            NativeWebRequest webRequest,
            WebDataBinderFactory binderFactory) {
        HttpServletRequest request = (HttpServletRequest) webRequest.getNativeRequest();
        Long memberId = getMemberId(request);
        Role role = getRole(request);
        return new AuthenticatedMember(memberId, role);
    }

    private Long getMemberId(HttpServletRequest request) {
        Object memberId = request.getHeader("memberId");
        if (memberId != null) {
            return Long.parseLong(memberId.toString());
        }
        throw BusinessException.of(MEMBER_NOT_FOUND_EXCEPTION);
    }

    private Role getRole(HttpServletRequest request) {
        Object role = request.getHeader("Role");
        if (role != null) {
            return Role.valueOf(role.toString());
        }
        throw BusinessException.of(MEMBER_NOT_FOUND_EXCEPTION);
    }
}
