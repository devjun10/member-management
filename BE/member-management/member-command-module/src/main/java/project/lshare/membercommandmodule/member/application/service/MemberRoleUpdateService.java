package project.lshare.membercommandmodule.member.application.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import project.lshare.membercommandmodule.member.application.port.in.AdminCommand;
import project.lshare.membercoremodule.domainmodel.member.Member;
import project.lshare.membercoremodule.domainmodel.member.Role;
import project.lshare.membercoremodule.persistencemodel.member.MemberPersistenceAdapter;

@Service
public class MemberRoleUpdateService implements AdminCommand {

    private final MemberPersistenceAdapter memberPersistenceAdapter;

    public MemberRoleUpdateService(MemberPersistenceAdapter memberPersistenceAdapter) {
        this.memberPersistenceAdapter = memberPersistenceAdapter;
    }

    @Override
    @Transactional
    public void changeRole(Long adminId, Long memberId, Role role) {
        Member admin = memberPersistenceAdapter.findAdminById(adminId);
        Member member = memberPersistenceAdapter.findMemberById(memberId);
        member.changeRole(role);
        memberPersistenceAdapter.update(member);
    }
}
