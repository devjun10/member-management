package project.lshare.membercommandmodule.common.configuration.annotation;

import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@SqlGroup({
        @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = InsertData.MEMBER_SQL_SCRIPT_PATH)
})
public @interface InsertData {

    String MEMBER_SQL_SCRIPT_PATH = "classpath:/data/member.sql";

}
