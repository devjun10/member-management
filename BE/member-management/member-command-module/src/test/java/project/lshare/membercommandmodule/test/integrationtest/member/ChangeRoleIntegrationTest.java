package project.lshare.membercommandmodule.test.integrationtest.member;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import project.lshare.membercommandmodule.member.application.port.in.AdminCommand;
import project.lshare.membercommandmodule.test.integrationtest.IntegrationTestBase;
import project.lshare.membercoremodule.domainmodel.member.Role;
import project.lshare.membercoremodule.persistencemodel.member.MemberJpaEntity;

import static org.junit.Assert.assertEquals;
import static project.lshare.membercommandmodule.test.helper.fixture.MemberFixture.createAdminMember;
import static project.lshare.membercommandmodule.test.helper.fixture.MemberFixture.createNormalMember;

@DisplayName("회원 역할 변경 통합 테스트")
class ChangeRoleIntegrationTest extends IntegrationTestBase {

    @Autowired
    private AdminCommand adminCommand;

    @Test
    @DisplayName("관리자가 회원의 권한을 바꾼다면 회원 권한이 수정된다.")
    void when_admin_changes_the_member_role_then_role_should_be_updated() {
        // given
        Role expected = Role.ADMIN;
        MemberJpaEntity admin = persistenceHelper.persist(createAdminMember());
        MemberJpaEntity normalMember = persistenceHelper.persist(createNormalMember(1L, "helloworld"));

        // when
        adminCommand.changeRole(admin.getMemberId(), normalMember.getMemberId(), Role.ADMIN);

        // then
        assertEquals(expected, persistenceHelper.findMemberById(normalMember.getMemberId()).getRole());
    }
}
