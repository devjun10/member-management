package project.lshare.membercommandmodule.test.helper.snippet.restdocsutils;

public enum Option {
    TRUE("true"),
    FALSE("false");

    private final String value;

    Option(String value) {
        this.value = value;
    }

    public String getValue(String value) {
        return Option.valueOf(value).toString();
    }
}
