package project.lshare.membercommandmodule.test.integrationtest.member;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import project.lshare.membercommandmodule.member.application.port.in.MemberUpdateCommand;
import project.lshare.membercommandmodule.test.helper.fixture.MemberFixture;
import project.lshare.membercommandmodule.test.integrationtest.IntegrationTestBase;
import project.lshare.membercoremodule.common.mapper.BusinessException;
import project.lshare.membercoremodule.domainmodel.member.valueobject.Nickname;
import project.lshare.membercoremodule.persistencemodel.member.MemberJpaEntity;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("회원 닉네임 변경 통합 테스트")
class ChangeNicknameIntegrationTest extends IntegrationTestBase {

    @Autowired
    private MemberUpdateCommand memberUpdateCommand;

    @Test
    @DisplayName("회원 닉네임을 변경하면 정보가 업데이트 된다.")
    void when_change_nickname_then_memberprofile_should_be_updated() {
        String expected = "isDevjun10";
        MemberJpaEntity member = persistenceHelper.persist(MemberFixture.createMemberWithId());
        memberUpdateCommand.changeNickName(member.getMemberId(), new Nickname("isDevjun10"));

        assertEquals(expected, persistenceHelper.findMemberById(member.getMemberId()).getNickname());
    }

    @Test
    @DisplayName("존재하지 않는 회원의 정보를 변경하려고 하면 BusinessException이 발생한다.")
    void when_try_to_change_non_members_nickname_then_business_exception_should_be_happened() {
        Long invalidMemberId = Long.MAX_VALUE;
        assertThatThrownBy(() -> memberUpdateCommand.changeNickName(invalidMemberId, new Nickname("isDevjun10")))
                .isInstanceOf(BusinessException.class)
                .hasMessage("회원을 찾을 수 없습니다.");
    }
}
