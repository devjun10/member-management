package project.lshare.membercommandmodule.test.integrationtest;

import org.junit.jupiter.api.AfterEach;
import org.springframework.beans.factory.annotation.Autowired;
import project.lshare.membercommandmodule.common.configuration.annotation.IntegrationTest;
import project.lshare.membercommandmodule.common.configuration.rdb.RDBInitializationConfiguration;
import project.lshare.membercommandmodule.test.helper.persistence.PersistenceHelper;

import javax.persistence.EntityManager;

@IntegrationTest
public abstract class IntegrationTestBase {

    @Autowired
    private RDBInitializationConfiguration rdbInitialization;

    @Autowired
    protected PersistenceHelper persistenceHelper;

    @Autowired
    protected EntityManager entityManager;

    @AfterEach
    void afterEach() {
        rdbInitialization.truncateAllEntity();
    }

}
