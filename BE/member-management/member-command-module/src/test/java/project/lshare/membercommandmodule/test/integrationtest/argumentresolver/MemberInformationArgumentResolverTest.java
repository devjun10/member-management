package project.lshare.membercommandmodule.test.integrationtest.argumentresolver;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.MethodParameter;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.method.support.ModelAndViewContainer;
import project.lshare.membercommandmodule.member.adapter.in.MemberNicknameUpdateController;
import project.lshare.membercommandmodule.member.adapter.in.argumentresolver.MemberInformationArgumentResolver;
import project.lshare.membercommandmodule.member.adapter.in.request.MemberUpdateRequest;
import project.lshare.membercommandmodule.test.helper.persistence.PersistenceHelper;
import project.lshare.membercommandmodule.test.integrationtest.IntegrationTestBase;
import project.lshare.membercoremodule.common.mapper.BusinessException;
import project.lshare.membercoremodule.domainmodel.authentication.valueobject.AuthenticatedMember;
import project.lshare.membercoremodule.domainmodel.member.Role;

import java.lang.reflect.Method;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertAll;
import static project.lshare.membercommandmodule.test.helper.fixture.MemberFixture.createMemberWithoutId;

@DisplayName("사용자 정보 파라미터 변환 테스트")
class MemberInformationArgumentResolverTest extends IntegrationTestBase {

    @Autowired
    private MemberInformationArgumentResolver argumentResolver;

    @MockBean
    private MethodParameter parameter;

    @MockBean
    private ModelAndViewContainer mavContainer;

    @MockBean
    private NativeWebRequest webRequest;

    @MockBean
    private WebDataBinderFactory binderFactory;

    @Autowired
    private PersistenceHelper persistenceHelper;

    private MockHttpServletRequest request;
    private MockHttpServletResponse response;

    @BeforeEach
    void setUP() {
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
        webRequest = new ServletWebRequest(request, response);
    }

    @Test
    @DisplayName("메서드 인자에 존재하는 타입의 정보를 알 수 있다.")
    void when_method_has_parameter_then_argumentresolver_can_identify_method_parameter_type() throws NoSuchMethodException {
        // given
        boolean expected = true;
        Method method = MemberNicknameUpdateController.class.getMethod("updateMemberInformation", AuthenticatedMember.class, MemberUpdateRequest.class);
        MethodParameter methodParameter = new MethodParameter(method, 0);

        // when, then
        assertEquals(expected, argumentResolver.supportsParameter(methodParameter));
    }

    @Test
    @DisplayName("요청(WebRequest)이 올바른 회원 정보를 가지고 있다면 회원정보를 조회할 수 있다.")
    void when_request_has_memberid_then_argumentresolver_can_find_memberinformation() {
        // given
        Long expectedId = 1L;
        Role expectedRole = Role.NORMAL;
        request.addHeader("memberId", 1L);
        request.addHeader("Role", "NORMAL");
        persistenceHelper.persist(createMemberWithoutId());

        // when
        Object argument = argumentResolver.resolveArgument(parameter, mavContainer, webRequest, binderFactory);

        // then
        AuthenticatedMember authenticatedMember = (AuthenticatedMember) argument;
        assertAll(
                () -> assertEquals(expectedId, authenticatedMember.getMemberId()),
                () -> assertEquals(expectedRole, authenticatedMember.getRole())
        );
    }

    @Test
    @DisplayName("요청(WebRequest) 헤더가 회원 아이디를 가지고 있지 않다면 BusinessException이 발생한다.")
    void when_requestheader_has_not_memberid_then_argumentresolver_can_not_find_memberinformation() {
        // given
        request.addHeader("Role", "NORMAL");
        persistenceHelper.persist(createMemberWithoutId());

        // when, then
        assertThatThrownBy(() -> argumentResolver.resolveArgument(parameter, mavContainer, webRequest, binderFactory))
                .isInstanceOf(BusinessException.class)
                .hasMessage("회원을 찾을 수 없습니다.");
    }

    @Test
    @DisplayName("요청(WebRequest) 헤더가 회원 역할을 가지고 있지 않다면 BusinessException이 발생한다.")
    void when_requestheader_has_not_memberrole_then_argumentresolver_can_not_find_memberinformation() {
        // given
        request.addHeader("memberId", 1L);
        persistenceHelper.persist(createMemberWithoutId());

        // when, then
        assertThatThrownBy(() -> argumentResolver.resolveArgument(parameter, mavContainer, webRequest, binderFactory))
                .isInstanceOf(BusinessException.class)
                .hasMessage("회원을 찾을 수 없습니다.");
    }
}
