package project.lshare.membercommandmodule.test.integrationtest.aop;

import org.aspectj.lang.JoinPoint;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.SpyBean;
import project.lshare.membercommandmodule.member.adapter.in.aspect.RoleCheckAspect;
import project.lshare.membercommandmodule.test.integrationtest.IntegrationTestBase;
import project.lshare.membercoremodule.common.mapper.BusinessException;
import project.lshare.membercoremodule.domainmodel.authentication.valueobject.AuthenticatedMember;
import project.lshare.membercoremodule.domainmodel.member.Role;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@DisplayName("AOP 권한 검사 테스트")
class RoleCheckAspectTest extends IntegrationTestBase {

    @SpyBean
    private RoleCheckAspect aspect;

    @Test
    @DisplayName("관리자가 권한을 바꾸려고 하면 메서드가 정상적으로 호출된다.")
    void when_admin_trys_to_change_role_then_method_should_be_called() {
        JoinPoint spy = spy(JoinPoint.class);
        Object[] args = new Object[]{new AuthenticatedMember(1L, Role.ADMIN)};
        when(spy.getArgs()).thenReturn(args);

        aspect.checkRole(spy);

        verify(aspect, times(1)).checkRole(spy);
    }

    @Test
    @DisplayName("일반 사용자가 권한을 바꾸려고 할 경우 BusinessException이 발생한다.")
    void when_not_admin_trys_to_change_role_then_business_exception_should_be_happended() {
        JoinPoint spy = spy(JoinPoint.class);
        Object[] args = new Object[]{new AuthenticatedMember(1L, Role.NORMAL)};
        when(spy.getArgs()).thenReturn(args);

        assertThatThrownBy(() -> aspect.checkRole(spy))
                .isInstanceOf(BusinessException.class)
                .hasMessage("관리자 권한이 없습니다.");
    }

    @Test
    @DisplayName("잘못된 관리자 정보가 들어오면 BusinessException이 발생한다.")
    void when_invalid_admin_information_accessed_then_business_exception_should_be_happended() {
        JoinPoint spy = spy(JoinPoint.class);
        Object[] args = new Object[]{null};
        when(spy.getArgs()).thenReturn(args);

        assertThatThrownBy(() -> aspect.checkRole(spy))
                .isInstanceOf(BusinessException.class)
                .hasMessage("관리자 권한이 없습니다.");
    }
}
