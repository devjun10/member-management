resource "aws_s3_bucket" "example" {
  bucket = "management-backend-tf"

  versioning {
    enabled = true # Prevent from deleting tfstate file
  }
  }

output "bucket_name" {
  value = aws_s3_bucket.example.id
}

# terraform {
#   backend "s3" {
#     bucket = "management-backend-tf"
#     key    = "terraform.tfstate"
#     region = "ap-northeast-2"
#   }
# }