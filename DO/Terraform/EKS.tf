#
# resource "aws_eks_cluster" "example" {
#   name     = "example-eks-cluster"
#   role_arn = aws_iam_role.eks_cluster.arn
#
#   vpc_config {
#     subnet_ids = aws_subnet.private.*.id
#   }
#
#   depends_on = [aws_iam_role_policy_attachment.eks_cluster]
# }
#
# resource "aws_iam_role" "eks_cluster" {
#   name = "eks-cluster"
#
#   assume_role_policy = jsonencode({
#     Version = "2012-10-17"
#     Statement = [
#       {
#         Action = "sts:AssumeRole"
#         Effect = "Allow"
#         Principal = {
#           Service = "eks.amazonaws.com"
#         }
#       }
#     ]
#   })
# }
#
# resource "aws_iam_role_policy_attachment" "eks_cluster" {
#   policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
#   role       = aws_iam_role.eks_cluster.name
# }
#
# # Create a VPC
# resource "aws_vpc" "example" {
#   cidr_block = "10.0.0.0/16"
# }
#
# resource "aws_subnet" "private" {
#   count = 3
#
#   cidr_block = "10.0.${count.index + 1}.0/24"
#
#   tags = {
#     Name = "example-private-subnet-${count.index + 1}"
#   }
# }
#
# output "eks_config" {
#   value = {
#     endpoint        = aws_eks_cluster.example.endpoint
#     certificate_arn = aws_eks_cluster.example.certificate_authority.0.data
#     cluster_name    = aws_eks_cluster.example.name
#   }
# }
