
resource "aws_instance" "example" {
  ami           = "ami-0443a21cb1a8f238e"   # Amazon Linux 2 AMI
  instance_type = "t2.micro"
  key_name      = "my_keypair"


  user_data = <<-EOF
    #!/bin/bash
    # Install Docker
    yum update -y
    amazon-linux-extras install docker -y
    systemctl start docker
    systemctl enable docker

    # Download and run Jenkins Docker image
    docker run -d --name jenkins -p 8080:8080 -p 50000:50000 jenkins/jenkins:lts
    EOF

  tags = {
    Name = "example-instance"
  }
}

output "public_ip" {
  value = aws_instance.example.public_ip
}
