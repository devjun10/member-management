
resource "aws_ecr_repository" "management" {
  name = "management"
}

output "ecr_repository_url" {
  value = aws_ecr_repository.management.repository_url
}
